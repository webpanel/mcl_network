branch='test'; #enter your branch name here
path='/home/texiserver/www/autoverdi/admin-panel-test';  #path to your repo


#back end deployment, below commands will reset hard the server repo with the remote repo and deploy all of you code on to the server. NOTE: all the changes done directly on the server will be lost.

#cd $path && git fetch origin $branch && git reset --hard origin/$branch &&  sudo chmod -R 777 $path && sudo pm2 restart $nodeprocess && sudo pm2 logs $nodeprocess

#front end deployment - similar to Back End commands above.

cd $path && git fetch origin $branch && git reset --hard origin/$branch &&  sudo chmod -R 777 $path && sudo git checkout test;