
App.controller('BkOngoingRidesController', function ($scope, $http,$state, $cookies, $cookieStore, MY_CONSTANT, $timeout,ngDialog,responseCode) {

    'use strict';
    $scope.showloader=true;
    $scope.displaymsg = "";
    if($cookieStore.get('type')==2){
        $scope.showcsvbtn = 0;
    }
    else
    {
        $scope.showcsvbtn = 1;
    }

    var dtInstance;

    var d = new Date((new Date()).getTime());
    var offset = d.getTimezoneOffset();

    $("#ridescsv").attr("href", MY_CONSTANT.url + "/api/bookedOngoingRides?accessToken=" + $cookieStore.get('obj').accesstoken + "&timezone=" + offset + "&csv=" + "1");


    $timeout(function () {
        if (!$.fn.dataTable) return;

        dtInstance = $('#datatable6').dataTable({
            "bProcessing": true,
            'paging': true,  // Table pagination
            'ordering': true,  // Column ordering
            'info': true,  // Bottom left status text
            // Text translation options
            // Note the required keywords between underscores (e.g _MENU_)
            "bServerSide": true,
            sAjaxSource: MY_CONSTANT.url + "/api/bookedOngoingRides?accessToken=" + $cookieStore.get('obj').accesstoken + "&timezone=" + offset,
            oLanguage: {
                sSearch: 'Search all columns:',
                sLengthMenu: '_MENU_ records per page',
                info: 'Showing page _PAGE_ of _PAGES_',
                zeroRecords: 'Nothing found - sorry',
                infoEmpty: 'No records available',
                infoFiltered: '(filtered from _MAX_ total records)'
            },
            "aaSorting": [[0,"desc"]]
            //"columnDefs":[{"targets":7, "data":"name", "render": function(data,type,full,meta)
            //{ return '<a class="resend"  class="text-info" >RESEND</a><br>'+'<a  class="text-success" class="complete_ride">COMPLETE</a><br>'+'<a class="text-danger" class="cancel_ride"">CANCEL</a>'
            //}}]
        });

        $scope.showloader=false;
        var inputSearchClass = 'datatable_input_col_search';
        var columnInputs = $('tfoot .' + inputSearchClass);

        // On input keyup trigger filtering
        columnInputs
            .keyup(function () {
                dtInstance.fnFilter(this.value, columnInputs.index(this));
            });

    });

    // When scope is destroyed we unload all DT instances
    // Also ColVis requires special attention since it attaches
    // elements to body and will not be removed after unload DT
    $scope.$on('$destroy', function () {
        dtInstance.fnDestroy();
        $('[class*=ColVis]').remove();
    });
    //});

    /*--------------------------------------------------------------------------
     * --------- funtion to refresh page ---------------------------------------
     --------------------------------------------------------------------------*/
    $scope.closePopup = function () {
        ngDialog.close({
            template: 'display_msg_modalDialog',
            className: 'ngdialog-theme-default',
            scope: $scope
        });

    };

    $('#datatable6').on('click', '.complete_ride', function (e) {
        console.log("geeee");
        $scope.rideId= (e.currentTarget.id).split('_')[1];
        console.log($scope.rideId);
        ngDialog.open({
            template: 'force_stop_modalDialog',
            className: 'ngdialog-theme-default',
            showClose: false,
            scope: $scope
        });

    });

    $('#datatable6').on('click', '.cancel_ride', function (e) {
        console.log("geeee");
        $scope.rideId= (e.currentTarget.id).split('_')[1];
        $.post(MY_CONSTANT.url_booking + '/api/rides/cancelRideByAdmin', {
            adminAccessToken: $cookieStore.get('obj').accesstoken,
            rideId: $scope.rideId,
            type: '1'
        }, function (response) {
            if(typeof(response)=="string")
                response = JSON.parse(response);

            if (response.flag== responseCode.SUCCESS) {
                $('#'+e.currentTarget.id).parent().parent().remove();
                $scope.displaymsg = "Request has been cancelled successfully.";
            }
            else{
                $scope.displaymsg = response.error;
            }
            ngDialog.open({
                template: 'display_msg_modalDialog',
                className: 'ngdialog-theme-default',
                showClose: false,
                scope: $scope
            });
        });

    });


    $scope.completeRide = function(){
        ngDialog.close({
            template: 'force_stop_modalDialog',
            className: 'ngdialog-theme-default',
            showClose: false,
            scope: $scope
        });

        $.post(MY_CONSTANT.url_booking + '/api/rides/cancelRideByAdmin', {
            adminAccessToken: $cookieStore.get('obj').accesstoken,
            rideId: $scope.rideId,
            type:'0'
        }, function (response) {
            if(typeof(response)=="string")
                response = JSON.parse(response);

            if (response.flag== responseCode.SUCCESS) {
                $('#COMPLETE_'+$scope.rideId).parent().parent().remove();
                $scope.displaymsg = "Ride completed successfully.";
            }
            else if (response.flag == responseCode.SHOW_ERROR_MESSAGE) {
                $scope.displaymsg = response['status'];
            } else if (response.flag == responseCode.ERROR_IN_EXECUTION) {
                $scope.displaymsg = "Something went wrong, please try again later.";
            }
            else{
                $scope.displaymsg = response.error;
            }

            ngDialog.open({
                template: 'display_msg_modalDialog',
                className: 'ngdialog-theme-default',
                showClose: false,
                scope: $scope
            });
        });
    };

    /*--------------------------------------------------------------------------
     * -------------------funtion to modify ONGOING Rides ----------------------
     --------------------------------------------------------------------------*/
    $('#datatable6').on('click', '.resend', function (e) {
        $scope.rideId= (e.currentTarget.id).split('_')[1];

        $('#'+$scope.rideId).text('IN PROGRESS');

        $.post(MY_CONSTANT.url_booking + '/api/rides/reAssignRide', {
            adminAccessToken: $cookieStore.get('obj').accesstoken,
            previousRideId: $scope.rideId,
            adminPanelFlag: 1
        }, function (response) {
            if (typeof(response) == "string")
                response = JSON.parse(response);

            if (response.flag == responseCode.SUCCESS) {
                $scope.displaymsg = "Request has been sent successfully as Ride Id = "+response.data.rideId;
            }
            else {
                $('#'+$scope.rideId).text('MISSED RIDE');
                $scope.displaymsg = response.error;
            }
            ngDialog.open({
                template: 'display_msg_modalDialog',
                className: 'ngdialog-theme-default',
                showClose: false,
                scope: $scope
            });
        });
    });



    /*--------------------------------------------------------------------------
     * -------------------funtion to cancel Rides ----------------------
     --------------------------------------------------------------------------*/
    $('#datatable6').on('click', '.charge_cancel_ride', function (e) {
        console.log("geeee");
        $scope.rideId= (e.currentTarget.id).split('_')[1];
        console.log($scope.rideId);
        ngDialog.open({
            template: 'charge_confirm',
            className: 'ngdialog-theme-default',
            showClose: false,
            scope: $scope
        });

    });
    $scope.chargeCancelledRide = function(flag){
        console.log(flag);
        ngDialog.close({
            template: 'charge_confirm',
            className: 'ngdialog-theme-default',
            showClose: false,
            scope: $scope
        });
        $.post(MY_CONSTANT.url_booking + '/api/rides/chargeCancellationToCustomer', {
            adminAccessToken: $cookieStore.get('obj').accesstoken,
            rideId: $scope.rideId,
            flag: flag
        }, function (response) {
            if (typeof(response) == "string")
                response = JSON.parse(response);

            if (response.flag == responseCode.SUCCESS) {
                $('#CHARGE_'+$scope.rideId).parent().parent().remove();
                $scope.displaymsg = "Request has been  successfully removed accordingly.";
            }
            else {
                $scope.displaymsg = response.error;
            }
            ngDialog.open({
                template: 'display_msg_modalDialog',
                className: 'ngdialog-theme-default',
                showClose: false,
                scope: $scope
            });
        });
    };

    // socket connection
    var socket = io.connect(MY_CONSTANT.url_booking);
    socket.on("admin:rideStatus",function(data){
        var newData = data.data.rideData;
        var carType ='Taxi';
        console.log(data);
        switch(data.flag){
            case 0:  //Request in progress
                //var newStr='<p id="in_progress_'+data.rideId+'">#'+data.rideId+' request is in progress</p>';
                //$('#request_progress').append(newStr);
                break;
            case 1:   //WHEN DRIVER HAS ACCEPTED
                // $('#'+data.rideId).parent().parent().remove();
                //$('#in_progress_'+data.rideId).remove();

                if($('#'+data.rideId).length == 0) {
                    //it doesn't exist
                    if (newData.carType == 1)
                        carType = 'Town Car';

                    var trStr = '<tr>' +
                        '<td>' + data.rideId + '</td>' +
                        '<td> <span> car# : ' + newData.carNo + '</span><br><span> carType : '+carType+'</span></td>' +
                        '<td> <span> Pickup : ' + newData.pickupLocationAddress + '</span><br><span> DropOff : '+newData.destinationLocationAddress+'</span></td>' +
                        '<td> <span> Name : ' + newData.userName + '</span><br><span> Phone : '+newData.phoneNo+'</span></td>' +
                        '<td> <span> Name : ' + newData.driverName + '</span><br><span> Phone : '+newData.driverPhoneNo+'</span></td>' +
                        '<td><p id="' + data.rideId + '" style=background-color:lightblue;padding:5px;color:#fff>ACCEPTED</p></td>' +
                        '<td>' + getRideTime(newData, offset) + '</td>' +
                        '<td><a class=\"text-success complete_ride\" id ="COMPLETE_' + data.rideId + '">COMPLETE</a><br><a class=\"text-danger cancel_ride\" id ="CANCEL_' + data.rideId + '" >CANCEL</a></td>' +
                        '</tr>';
                    $('#datatable6 > tbody').prepend($(trStr)[0]);
                }else{
                    $('#'+data.rideId).css('background-color','lightblue');
                    $('#'+data.rideId).text('ACCEPTED');
                    $('#'+data.rideId).css('color','#fff');
                    $('#RESEND_'+data.rideId).remove();

                    $('#carNo_'+data.rideId).text(newData.carNo);
                    var tdStr='<td> <span> Name : ' + newData.driverName + '</span><br><span> Phone : '+newData.driverPhoneNo+'</span></td>';
                    $('#driverTd_'+data.rideId).html(tdStr);
                }

                break;
            case 2:   //WHEN DRIVER HAS ARRIVED
                console.log("arrived");
                $('#'+data.rideId).css('background-color','BLUE');
                $('#'+data.rideId).css('padding','5px');
                $('#'+data.rideId).css('color','#fff');
                $('#'+data.rideId).text('DRIVER ARRIVED');
                break;
            case 3:   //WHEN RIDE IS STARTED
                console.log("STARTED");
                $('#'+data.rideId).css('background-color','green');
                $('#'+data.rideId).css('padding','5px');
                $('#'+data.rideId).css('color','#fff');
                $('#'+data.rideId).text('RIDE STARTED');
                break;
            case 4:   //WHEN RIDE IS ENDED
                $('#'+data.rideId).parent().parent().remove();
                break;
            case 5:   //WHEN RIDE CANCELED BY DRIVER
                $('#'+data.rideId).css('background-color','red');
                $('#'+data.rideId).text(data.status);
                var newstr2 = '<a class=\"text-info charge_cancel_ride\" id ="CHARGE_' + data.rideId + '">CHARGE</a>';
                $('#COMPLETE_'+data.rideId).parent().html(newstr2);
                break;
            case 6:   //WHEN RIDE CANCELED BY CUSTOMER
                $('#'+data.rideId).parent().parent().remove();
                break;
            case 7:   //WHEN RIDE CANCELED BY ADMIN
                $('#'+data.rideId).parent().parent().remove();
                break;
            case 10:   //WHEN NO DRIVER ACCEPTED OR AVAILABLE
                console.log("NO DRIVER");
                //  $('#in_progress_'+data.rideId).remove();
                if($('#'+data.rideId).length == 0) {
                    if (newData.carType == 1)
                        carType = 'Town Car';

                    var trStr = '<tr>' +
                        '<td>' + data.rideId + '</td>' +
                        '<td> <span> car# : <span id="carNo_' + data.rideId + '" >N.A</span></span><br><span> carType : '+carType+'</span></td>' +
                        '<td> <span> Pickup : ' + newData.pickupLocationAddress + '</span><br><span> DropOff : '+newData.destinationLocationAddress+'</span></td>' +
                        '<td> <span> Name : ' + newData.userName + '</span><br><span> Phone : '+newData.phoneNo+'</span></td>' +
                        '<td> <p id="driverTd_' + data.rideId + '"> <span> Name : N.A </span><br><span> Phone : N.A </span></p></td>' +
                        '<td><p id="' + data.rideId + '" style=background-color:#1fb5ad;padding:5px;color:#fff>'+data.status+'</p></td>' +
                        '<td>' + getRideTime(newData, offset) + '</td>' +
                        '<td><a class=\"resend text-info\" id ="RESEND_' + data.rideId + '">RESEND</a><br><a class=\"text-success complete_ride\" id ="COMPLETE_' + data.rideId + '">COMPLETE</a><br><a class=\"text-danger cancel_ride\" id ="CANCEL_' + data.rideId + '" >CANCEL</a></td>' +
                        '</tr>';
                    $('#datatable6 > tbody').prepend($(trStr)[0]);
                }else{
                    if($('#RESEND_'+data.rideId).length == 0)
                        $('<a class=\"resend text-info\" id ="RESEND_' + data.rideId + '">RESEND</a><br>').insertBefore('#COMPLETE_'+data.rideId);
                    $('#'+data.rideId).text(data.status);
                }
                break;
            case 12:   //WHEN RIDE CANCELED BY ADMIN
                $('#'+data.rideId).parent().parent().remove();
                break;
            case 13:   //WHEN RIDE CANCELED BY ADMIN
                $('#'+data.rideId).parent().parent().remove();
                break;
        }
    });

    function getRideTime(rideData , timezone) {

        return '<span> E: ' + getTime(rideData.requestTime, timezone) + ' | A: ' + getTime(rideData.acceptTime, timezone) + ' <br> D: ' + getTime(rideData.arrivingTime, timezone) + ' | C: ' + getTime(rideData.dropTime, timezone) + ' | ' + getDate(rideData.requestTime, timezone) + ' </span>'; // resultAllRides[i].arrivingTime;

    }


    function getTime(date) {

        if (date == "0000-00-00 00:00:00" || date == null) {
            return 'N.A.';
        }

        var dtOb = new DateObject(date);


        return dtOb.time;

    }

    function getDate(date, timezone) {
        if (date == "0000-00-00 00:00:00") {
            return 'N.A.';
        }

        var dtOb = new DateObject(date, timezone);

        return dtOb.date;

    }

    function DateObject(date, timezone) {

        this.date = new Date(date);

        if (typeof timezone != "undefined")

        {
            // time zone in minutes

            this.date.setTime(this.date.getTime() - (timezone * 60 * 1000));

        }

        this.dateString = this.date.toString();
        this.dateStringArr = this.dateString.split(" ");
        this.hour = appendZero(this.date.getHours() % 12);
        this.hour = this.hour == '00' ? '12' : this.hour;
        this.second = appendZero(this.date.getSeconds());
        this.minute = appendZero(this.date.getMinutes());
        this.amPm = this.hour > 12 ? 'PM' : 'AM';
        this.dayName = this.dateStringArr[0];
        this.monthName = this.dateStringArr[1];

        this.time = this.hour + ":" + this.minute + " " + this.amPm;

        this.date = this.date.getDate() + "/" + (this.date.getMonth() + 1) + "/" + this.date.getFullYear();

    }

    function appendZero(num) {
        return num < 10 ? '0' + num : num;
    }

});

