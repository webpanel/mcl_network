/**
 * Created by salma on 22/8/15.
 */

App.controller('ViewDriverController', function ($scope, $http, $cookies, $cookieStore, MY_CONSTANT, $state,ngDialog,responseCode,$stateParams) {
    /*--------------------------------------------------------------------------
     * ---------------- get driver's data by using driver id -------------
     --------------------------------------------------------------------------*/
    $.post(MY_CONSTANT.url + '/api/drivers/driver', {
            accessToken: $cookieStore.get('obj').accesstoken,
            userId: $stateParams.driver_id
        },
        function (data) {
              console.log(data);
            if(typeof data == "string")
            {
                data = JSON.parse(data);
            }
            if (data.flag == responseCode.SUCCESS) {
                var driverlist = data.data.drivers[0];
                console.log(driverlist);
                $scope.user_name = driverlist.firstName + " " +driverlist.lastName ;
                $scope.phone_no = driverlist.phoneNo;
                $scope.email = driverlist.email;
                $scope.address = driverlist.streetAddress + "," + driverlist.city + ","+ driverlist.state + ", Zipcode:" + driverlist.zipcode;
                $scope.editdriver_url = "/app/edit-driver/" +  $stateParams.driver_id;
                $scope.isBlocked = driverlist.isBlocked;
                console.log("$scope.is_blocked"+$scope.isBlocked);
            }
            else if (data.flag == responseCode.INVALID_ACCESS_TOKEN){
                $state.go('page.login');
            }
            $scope.$apply();
        });


    /*--------------------------------------------------------------------------
     * --------- funtion to open dialog for block or unblock driver ------------
     --------------------------------------------------------------------------*/
    $scope.blockunblockdriver_popup = function (is_blocked) {

        $scope.blockunblockmsg = '';
        $scope.is_blocked = is_blocked;

        if (is_blocked == 1) {
            $scope.blockunblockmsg = "Are you sure you want to block this driver?";
        } else {
            $scope.blockunblockmsg = "Are you sure you want to unblock this driver?";
        }

        ngDialog.open({
            template: 'block_unblock_driver_modalDialog',
            className: 'ngdialog-theme-default',
            showClose: false,
            scope: $scope
        });

    };

    /*--------------------------------------------------------------------------
     * ------------------ funtion to block or unblock driver -------------------
     --------------------------------------------------------------------------*/
    $scope.do_block_unblock_driver = function (is_blocked) {
        $.post(MY_CONSTANT.url + '/api/drivers/blockUnblock',
            {
                accessToken: $cookieStore.get('obj').accesstoken,
                "userId": $stateParams.driver_id,
                "blockStatus": is_blocked
            }, function (data) {
                // data = JSON.parse(data);

                if (data.flag == responseCode.SUCCESS) {
                    if (is_blocked == 1) {
                        $scope.displaymsg = "Driver is blocked successfully.";
                    } else {
                        $scope.displaymsg = "Driver is unblocked successfully.";
                    }
                }
                else {
                    $scope.displaymsg = data.error;
                }
                $scope.$apply();
                ngDialog.close({
                    template: 'block_unblock_driver_modalDialog',
                    className: 'ngdialog-theme-default',
                    scope: $scope
                });

                ngDialog.open({
                    template: 'display_msg_modalDialog',
                    className: 'ngdialog-theme-default',
                    showClose: false,
                    scope: $scope
                });
            });
    };

    /*--------------------------------------------------------------------------
     * ---------------- funtion to open modal for delete driver ----------------
     --------------------------------------------------------------------------*/
    $scope.deletedriver = function () {
        ngDialog.open({
            template: 'delete_driver_modalDialog',
            className: 'ngdialog-theme-default',
            showClose: false,
            scope: $scope
        });
    };

    /*--------------------------------------------------------------------------
     * -------------------------funtion to delete driver --------------------------
     --------------------------------------------------------------------------*/
    $scope.delete_driver = function (delete_driver_id) {

        ngDialog.close({
            template: 'delete_driver_modalDialog',
            className: 'ngdialog-theme-default',
            scope: $scope
        });


        $.post(MY_CONSTANT.url + '/api/drivers/delete',
            {
                accessToken: $cookieStore.get('obj').accesstoken,
                userId: $stateParams.driver_id
            }, function (data) {
                if(typeof data == "string")
                {
                    data = JSON.parse(data);
                };
                console.log(data);

                if (data.flag == responseCode.SUCCESS) {
                    $scope.displaymsg2 = "Driver deleted successfully";
                }
                else if (data.status == responseCode.INVALID_ACCESS_TOKEN){
                    $state.go('page.login');
                }
                else {
                    $scope.displaymsg2 = data.error;
                }
                console.log($scope.list);
                $scope.$apply();
                ngDialog.open({
                    template: 'display_msg_modalDialog2',
                    className: 'ngdialog-theme-default',
                    showClose: false,
                    scope: $scope
                });
            });


    };

    /*--------------------------------------------------------------------------
     * --------- funtion to refresh page ---------------------------------------
     --------------------------------------------------------------------------*/
    $scope.refreshPage = function () {
        $state.reload();
        ngDialog.close({
            template: 'display_msg_modalDialog',
            className: 'ngdialog-theme-default',
            scope: $scope
        });

    };
});



