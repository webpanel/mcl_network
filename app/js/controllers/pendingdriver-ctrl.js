
App.controller('PendingDriverController', function ($scope, $http, $cookies,$state, $cookieStore, MY_CONSTANT, $timeout,ngDialog,responseCode) {

    'use strict';
    $scope.showloader=true;

    $scope.delete_driver_id = '';

    $scope.exportData = function () {
        alasql('SELECT * INTO CSV("pendingdrivers.csv",{headers:true}) FROM ?',[$scope.excelList]);
    };

    $.post(MY_CONSTANT.url + '/api/drivers/pendingDriversList', {
        accessToken: $cookieStore.get('obj').accesstoken

    }, function (data) {
        //console.log(data);
        $scope.showloader=false;
        var dataArray = [];
        var excelArray = [];
        if(typeof data == "string")
        {
            data = JSON.parse(data);
        }
        console.log(data);

      if (data.flag == responseCode.INVALID_ACCESS_TOKEN){
            $state.go('page.login');
        }
       else if (data.flag == responseCode.SUCCESS) {
          var driverList = data.data.drivers;
          driverList.forEach(function (column) {
 //==========================================================================================================================
//============================================================ data for excel =============================================
//==========================================================================================================================
              var e={}
              e.NAME = column.firstName + " " + column.lastName;
              e.EMAIL = column.email;
              e.CONTACT = column.phoneNo;
              e.CAR_TYPE = column.carType;
              e.CAR_MODEL = column.carModel;
              excelArray.push(e);


//==========================================================================================================================
//============================================================  end data for excel =============================================
//==========================================================================================================================
              var d = {

              };

              d.driver_id = column.userId;
              d.user_name = column.firstName + " " + column.lastName;
              d.user_email = column.email;
              d.phone_no = column.phoneNo;
              d.car_type = column.carType;
              d.braintreeStatus = column.braintreeStatus;
              d.braintreeMessage = column.braintreeMessage;
              d.car_model = column.carModel;
              d.editdriver_url = "/app/edit-driver/" +  column.userId;
              dataArray.push(d);
          });


            $scope.$apply(function () {
                $scope.list = dataArray;
                $scope.excelList = excelArray;

                // Define global instance we'll use to destroy later
                var dtInstance;

                $timeout(function () {
                    if (!$.fn.dataTable) return;
                    dtInstance = $('#datatable2').dataTable({
                        'paging': true,  // Table pagination
                        'ordering': true,  // Column ordering
                        'info': true,  // Bottom left status text
                        "bDestroy": true,
                        // Text translation options
                        // Note the required keywords between underscores (e.g _MENU_)
                        oLanguage: {
                            sSearch: 'Search all columns:',
                            sLengthMenu: '_MENU_ records per page',
                            info: 'Showing page _PAGE_ of _PAGES_',
                            zeroRecords: 'Nothing found - sorry',
                            infoEmpty: 'No records available',
                            infoFiltered: '(filtered from _MAX_ total records)'
                        },
                        "aoColumnDefs": [
                            { 'bSortable': false, 'aTargets': [5,6] }
                        ]
                    });
                    var inputSearchClass = 'datatable_input_col_search';
                    var columnInputs = $('tfoot .' + inputSearchClass);

                    // On input keyup trigger filtering
                    columnInputs
                        .keyup(function () {
                            dtInstance.fnFilter(this.value, columnInputs.index(this));
                        });
                });

                // When scope is destroyed we unload all DT instances
                // Also ColVis requires special attention since it attaches
                // elements to body and will not be removed after unload DT
                $scope.$on('$destroy', function () {
                    dtInstance.fnDestroy();
                    $('[class*=ColVis]').remove();
                });
            });
        }
    });


    /*--------------------------------------------------------------------------
     * -------------------------funtion to re apply card approval --------------------------
     --------------------------------------------------------------------------*/

    $scope.reApply = function (usedId){

        console.log(usedId);
        $.post(MY_CONSTANT.url + '/api/drivers/createSubMerchant',
            {
                accessToken: $cookieStore.get('obj').accesstoken,
                userId: usedId
            }, function (data) {
                if(typeof data == "string")
                {
                    data = JSON.parse(data);
                }

                if (data.flag == responseCode.SUCCESS) {
                    $scope.displaymsg = "Re-apply for card approval done successfully";
                }
                else if (data.status == responseCode.INVALID_ACCESS_TOKEN){
                    $state.go('page.login');
                }
                else {
                    $scope.displaymsg = data.error;
                }
                $scope.$apply();
                ngDialog.open({
                    template: 'display_msg_modalDialog',
                    className: 'ngdialog-theme-default',
                    showClose: false,
                    scope: $scope
                });
            });

    };


    /*--------------------------------------------------------------------------
     * ---------------- funtion to open modal for delete driver ----------------
     --------------------------------------------------------------------------*/
    $scope.deletedriver = function (delete_driver_id) {

        console.log('delete function');
        console.log(delete_driver_id);

        $scope.delete_driver_id = delete_driver_id;

        ngDialog.open({
            template: 'delete_driver_modalDialog',
            className: 'ngdialog-theme-default',
            showClose: false,
            scope: $scope
        });
    };

    /*--------------------------------------------------------------------------
     * -------------------------funtion to delete driver --------------------------
     --------------------------------------------------------------------------*/
    $scope.delete_driver = function (delete_driver_id) {

        ngDialog.close({
            template: 'delete_driver_modalDialog',
            className: 'ngdialog-theme-default',
            scope: $scope
        });


        $.post(MY_CONSTANT.url + '/api/drivers/delete',
            {
                accessToken: $cookieStore.get('obj').accesstoken,
                userId: delete_driver_id
            }, function (data) {
                if(typeof data == "string")
                {
                    data = JSON.parse(data);
                }
                console.log(data);

                if (data.flag == responseCode.SUCCESS) {
                    $scope.displaymsg = "Driver deleted successfully";
                }
                else if (data.status == responseCode.INVALID_ACCESS_TOKEN){
                    $state.go('page.login');
                }
                else {
                    $scope.displaymsg = data.error;
                }
                $scope.list = $scope.list.filter(function (el) {
                    return el.user_id !== delete_driver_id;
                });
                console.log($scope.list);
                $scope.$apply();
                ngDialog.open({
                    template: 'display_msg_modalDialog',
                    className: 'ngdialog-theme-default',
                    showClose: false,
                    scope: $scope
                });
            });


    };


    /*--------------------------------------------------------------------------
     * ---------------- funtion to open dialog for approve driver --------------
     --------------------------------------------------------------------------*/
    $scope.openApproveDialog = function (driverId,phone_no,driver_paypal_id) {
       console.log(phone_no);
        $scope.value = true;
        $scope.approveData={};
        $scope.approveData.driver_id=driverId;
        $scope.approveData.paypal_id = driver_paypal_id;
        var res = phone_no.substring(0, 1);
        if (res == '+')
            phone_no=phone_no.substring(1);
        $scope.approveData.phn_no=phone_no;

        ngDialog.open({
            template: 'approveDriverDialog',
            className: 'ngdialog-theme-default',
            showClose: false,
            scope: $scope
        });
    };

    /*--------------------------------------------------------------------------
     * ---------------- funtion to upload image ------------------------------
     --------------------------------------------------------------------------*/
    $scope.file_to_upload = function (files) {
        console.log(files);
        $scope.vehicle_image=files[0];
        var file = files[0];
        var imageType = /image.*/;
        if (!file.type.match(imageType)) {

        }
        var img = document.getElementById("car_image");
        img.file = file;
        var reader = new FileReader();
        reader.onload = (function (aImg) {
            return function (e) {
                aImg.src = e.target.result;
            };
        })(img);
        reader.readAsDataURL(file);
    }

    /*--------------------------------------------------------------------------
     * ---------------- funtion to approve driver ------------------------------
     --------------------------------------------------------------------------*/
    $scope.approveDriver = function (id) {
        $.post(MY_CONSTANT.url + '/api/drivers/verifyDriver',
            {
                accessToken: $cookieStore.get('obj').accesstoken,
                userId: id
            }, function (data) {
                if(typeof data == "string")
                {
                    data = JSON.parse(data);
                };
                console.log(data);

                if (data.flag == responseCode.SUCCESS) {
                    $scope.displaymsg = "Driver Approved Successfully.";
                }
                else if (data.flag == responseCode.INVALID_ACCESS_TOKEN){
                    $state.go('page.login');
                }
                else {
                    $scope.displaymsg = data.error;
                }
                $scope.$apply();
                ngDialog.open({
                    template: 'display_msg_modalDialog',
                    className: 'ngdialog-theme-default',
                    showClose: false,
                    scope: $scope
                });
            });

    };

    /*--------------------------------------------------------------------------
     * ---------------- funtion to open modal for delete driver ----------------
     --------------------------------------------------------------------------*/
    $scope.delete_pending_driver = function (delete_driver_id) {

        $scope.delete_driver_id = delete_driver_id;

        ngDialog.open({
            template: 'delete_driver_modalDialog',
            className: 'ngdialog-theme-default',
            showClose: false,
            scope: $scope
        });
    };

    /*--------------------------------------------------------------------------
     * -------------------------funtion to delete driver --------------------------
     --------------------------------------------------------------------------*/
    $scope.delete_driver = function (delete_driver_id) {

        ngDialog.close({
            template: 'delete_driver_modalDialog',
            className: 'ngdialog-theme-default',
            scope: $scope
        });

        $.post(MY_CONSTANT.url + '/api/drivers/delete',
            {
                accessToken: $cookieStore.get('obj').accesstoken,
                userId: delete_driver_id
            }, function (data) {
                if(typeof data == "string")
                {
                    data = JSON.parse(data);
                };
                console.log(data);

                if (data.flag == responseCode.SUCCESS) {
                    $scope.displaymsg = "Driver deleted successfully";
                }
                else if (data.flag == responseCode.INVALID_ACCESS_TOKEN){
                    $state.go('page.login');
                }
                else {
                    $scope.displaymsg = data.error;
                }
                $scope.$apply();
                ngDialog.open({
                    template: 'display_msg_modalDialog',
                    className: 'ngdialog-theme-default',
                    showClose: false,
                    scope: $scope
                });
            });


    };

    /*--------------------------------------------------------------------------
     * --------- funtion to refresh page ---------------------------------------
     --------------------------------------------------------------------------*/
    $scope.refreshPage = function () {
        $state.reload();
        ngDialog.close({
            template: 'display_msg_modalDialog',
            className: 'ngdialog-theme-default',
            scope: $scope
        });
    };

});