App.controller('DashboardReportController', ['$scope', '$http', '$cookies', '$cookieStore', 'MY_CONSTANT', '$state','responseCode'
    , function ($scope, $http, $cookies, $cookieStore, MY_CONSTANT, $state, responseCode) {

        //var socket = io.connect(MY_CONSTANT.url_booking);

        //socket.on('admin:LocationUpdated', function (data) {
        //
        //    console.log(data);
        //
        //});

        //$scope.$on('$destroy',function(){
        //    socket.close();
        //});

        $scope.data = null;

        var now = new Date();
        var yesterday = new Date();

        var yesterday = moment(yesterday).format("YYYY-MM-DD");
        var now = moment(now).format("YYYY-MM-DD");
        yesterday = startDateToUTC(yesterday);
        now = endDateToUTC(now);

        $.post(MY_CONSTANT.url + '/api/dashboard/dashboard_report', {
            accessToken: $cookieStore.get('obj').accesstoken,
            start_time: yesterday,
            end_time: now,
            offset: (new Date()).getTimezoneOffset()
        }, function (response) {
            response = JSON.parse(response);
            if (response.flag == responseCode.SUCCESS) {
                $scope.data = response.data;
                $scope.$apply();
            }
        })

    }]);