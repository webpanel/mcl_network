/**
 * Created by sanjay on 3/28/15.
 */
App.controller('MyDriverController', function ($scope, $http, $route, $state, $cookies, $cookieStore, MY_CONSTANT, $timeout, ngDialog, responseCode) {

    'use strict';
    $scope.addDriver = {};
    $scope.addDriver.email = '';
    $scope.delete_driver_id = '';
    $scope.uploaded_vehicle_img='';
    $scope.showloader=true;
    $scope.driverpass ={}


    $scope.exportData = function () {

        console.log($scope.excelList);
        alasql('SELECT * INTO CSV("drivers.csv",{headers:true}) FROM ?', [ $scope.excelList]);
    };



    /*--------------------------------------------------------------------------
     * -------------------funtion to open dialog for add driver ----------------
     --------------------------------------------------------------------------*/
    $scope.AddDriverDialog = function () {
        $scope.addDriver.email = '';
        ngDialog.open({
            template: 'modalDialogId',
            className: 'ngdialog-theme-default',
            showClose: false,
            scope: $scope
        });
    };

    /*--------------------------------------------------------------------------
     * -------------------------funtion to add driver --------------------------
     --------------------------------------------------------------------------*/
    $scope.submit = function (addDriver) {
        $.post(MY_CONSTANT.url + '/add_driver', {
            access_token: $cookieStore.get('obj').accesstoken,
            email: addDriver.email
        }, function (data) {
            console.log(data);
            data = JSON.parse(data);

            if (data.status == responseCode.SUCCESS) {
                $scope.addDriver.successMsg = "Mail sent to driver-id successfully.";
                $scope.$apply();
                setTimeout(function () {
                    $scope.addDriver.successMsg = "";
                    $scope.$apply();
                    ngDialog.close({
                        template: 'modalDialogId',
                        className: 'ngdialog-theme-default',
                        showClose: false,
                        scope: $scope
                    });
                }, 3000);
            }
            else if (data.status == responseCode.INVALID_ACCESS_TOKEN){
                $state.go('page.login');
            }
            else {
                $scope.addDriver.errorMsg = data.error;
                $scope.$apply();
                setTimeout(function () {
                    $scope.addDriver.errorMsg = "";
                    $scope.$apply();
                    ngDialog.close({
                        template: 'modalDialogId',
                        className: 'ngdialog-theme-default',
                        showClose: false,
                        scope: $scope
                    });
                }, 3000);
            }
        })
    };

    //var driver_details = function () {
    $.post(MY_CONSTANT.url + '/api/drivers/listDrivers', {
        accessToken: $cookieStore.get('obj').accesstoken
    }, function (data) {
        $scope.showloader=false;
        console.log(data);
        var dataArray = [];
        var excelArray=[];


        if(typeof data == "string")
        {
            data = JSON.parse(data);
        }

        if (data.flag == responseCode.SUCCESS) {
            console.log("drivers");
            console.log(data);
            var driverList = data.data.drivers;
            console.log(driverList)
            driverList.forEach(function (column) {

 //==========================================================================================================================
//============================================================ data for excel =============================================
//==========================================================================================================================
                var e ={}
                e.USER_ID = column.userId;
                e.USER_NAME = column.firstName+" "+column.lastName;
                e.EMAIL = column.email;
                e.CONTACT = column.phoneNo;
                e.ISLOGIN  = (column.isLogin ==1)? 'YES':'NO';
                e.TOTALRATING = column.totalRating;
                e.braintree_status =column.braintreeStatus;
                e.totalPaymentRemaining = column.totalPaymentRemaining;
                e.TotalRequests = column.totalRequests;
                e.TotalAcceptedRides = column.totalAcceptedRides;
                e.TotalCancelledRides = column.totalCancelledRides;
                e.IsBlocked = (column.isBlocked == 1)?'BLOCKED':'UNBLOCKED';
                e.IsDeleted = (column.isDeleted == 1)?'DELETED':'NO';
                excelArray.push(e);


//==========================================================================================================================
//============================================================  end data for excel ========================================
//==========================================================================================================================
                var d = {};
                if(column.isDeleted!=1){
                    d.user_id = column.userId;
                    d.user_name = column.firstName+" "+column.lastName;
                    d.user_email = column.email;
                    d.phone_no = column.phoneNo;
                    d.isLogin  = column.isLogin;
                    d.totalRating = column.totalRating;
                    d.totalRequests = column.totalRequests;
                    d.totalAcceptedRides = column.totalAcceptedRides;
                    d.totalCancelledRides = column.totalCancelledRides;
                    d.totalMissedRides = column.totalMissedRides;
                    d.gotTotalCash = column.gotTotalCash;
                    d.moneyTransferedToAccount = column.moneyTransferedToAccount;
                    d.moneyEarned = column.moneyEarned;
                    d.tipEarned = column.tipEarned;
                    d.totalMoneyEarned = column.totalMoneyEarned;
                    d.totalPaymentRemaining = column.totalPaymentRemaining;

                    d.braintree_status = column.braintreeStatus;
                    d.is_blocked = column.isBlocked;
                    d.is_deleted = column.isDeleted;
                    d.is_available = column.is_available;

                    d.last_login = moment(column.last_login).format("DD-MM-YYYY HH:mm:ss") ;
                    if(d.last_login == 'Invalid date'){
                        d.last_login = 'N/A';
                    }
                    d.last_logout = moment(column.last_logout).format("DD-MM-YYYY HH:mm:ss");
                    if(d.last_logout == 'Invalid date'){
                        d.last_logout = 'N/A';
                    }
                    d.driver_car_no = column.carNo;
                    d.car_type = column.carType;
                    d.editdriver_url = "/app/edit-driver/" +  column.userId;
                    d.viewdriver_url = "/app/view-driver/" +  column.userId;
                    d.region = column.region;

                    dataArray.push(d);
                }

            });

            $scope.$apply(function () {
                $scope.list = dataArray;
                $scope.excelList = excelArray;


                // Define global instance we'll use to destroy later
                var dtInstance;

                $timeout(function () {
                    if (!$.fn.dataTable)
                        return;
                    dtInstance = $('#datatable2').dataTable({
                        'paging': true, // Table pagination
                        'ordering': true, // Column ordering
                        'info': true, // Bottom left status text
                        // Text translation options
                        // Note the required keywords between underscores (e.g _MENU_)
                        oLanguage: {
                            sSearch: 'Search all columns:',
                            sLengthMenu: '_MENU_ records per page',
                            info: 'Showing page _PAGE_ of _PAGES_',
                            zeroRecords: 'Nothing found - sorry',
                            infoEmpty: 'No records available',
                            infoFiltered: '(filtered from _MAX_ total records)'
                        },
                        "aoColumnDefs": [
                            { 'bSortable': false, 'aTargets': [ 8,10] }
                        ],
                        "aaSorting": [[0,"desc"]]
                    });
                    var inputSearchClass = 'datatable_input_col_search';
                    var columnInputs = $('tfoot .' + inputSearchClass);

                    // On input keyup trigger filtering
                    columnInputs
                        .keyup(function () {
                            dtInstance.fnFilter(this.value, columnInputs.index(this));
                        });
                });
                // When scope is destroyed we unload all DT instances
                // Also ColVis requires special attention since it attaches
                // elements to body and will not be removed after unload DT
                $scope.$on('$destroy', function () {
                    dtInstance.fnDestroy();
                    $('[class*=ColVis]').remove();
                });
            });
        }
        else if (data.flag == responseCode.INVALID_ACCESS_TOKEN){
            $state.go('page.login');
        }
    });

    /*--------------------------------------------------------------------------
     * --------- funtion to refresh page ---------------------------------------
     --------------------------------------------------------------------------*/
    $scope.refreshPage = function () {
        $state.reload();
        ngDialog.close({
            template: 'display_msg_modalDialog',
            className: 'ngdialog-theme-default',
            scope: $scope
        });

    };

    /*--------------------------------------------------------------------------
     * --------- funtion to open dialog for block or unblock driver ------------
     --------------------------------------------------------------------------*/
    $scope.blockunblockdriver_popup = function (is_blocked,user_id) {

        $scope.blockunblockmsg = '';
        $scope.blockunblockid = user_id;
        $scope.is_blocked = is_blocked;

        if (is_blocked == 1) {
            $scope.blockunblockmsg = "Are you sure you want to block this driver?";
        } else {
            $scope.blockunblockmsg = "Are you sure you want to unblock this driver?";
        }

        ngDialog.open({
            template: 'block_unblock_driver_modalDialog',
            className: 'ngdialog-theme-default',
            showClose: false,
            scope: $scope
        });

    };

    /*--------------------------------------------------------------------------
     * ------------------ funtion to block or unblock driver -------------------
     --------------------------------------------------------------------------*/
    $scope.do_block_unblock_driver = function (is_blocked,blockunblockid) {
        console.log('is_blocked '+is_blocked);
        console.log('blockunblockid '+blockunblockid);

        $.post(MY_CONSTANT.url + '/api/drivers/blockUnblock',
            {
                accessToken: $cookieStore.get('obj').accesstoken,
                "userId": blockunblockid,
                "blockStatus": is_blocked
            }, function (data) {
               // data = JSON.parse(data);

                if (data.flag == responseCode.SUCCESS) {
                    if (is_blocked == 1) {
                        $scope.displaymsg = "Driver is blocked successfully.";
                    } else {
                        $scope.displaymsg = "Driver is unblocked successfully.";
                    }
                }
                else {
                    $scope.displaymsg = data.error;
                }
                $scope.$apply();
                ngDialog.close({
                    template: 'block_unblock_driver_modalDialog',
                    className: 'ngdialog-theme-default',
                    scope: $scope
                });

                ngDialog.open({
                    template: 'display_msg_modalDialog',
                    className: 'ngdialog-theme-default',
                    showClose: false,
                    scope: $scope
                });
            });
    };

    /*--------------------------------------------------------------------------
     * ---------------- funtion to open modal for delete driver ----------------
     --------------------------------------------------------------------------*/
    $scope.deletedriver = function (delete_driver_id) {

        console.log('delete function');
        console.log(delete_driver_id);

        $scope.delete_driver_id = delete_driver_id;

        ngDialog.open({
            template: 'delete_driver_modalDialog',
            className: 'ngdialog-theme-default',
            showClose: false,
            scope: $scope
        });
    };

    /*--------------------------------------------------------------------------
     * -------------------------funtion to delete driver --------------------------
     --------------------------------------------------------------------------*/
    $scope.delete_driver = function (delete_driver_id) {

        ngDialog.close({
            template: 'delete_driver_modalDialog',
            className: 'ngdialog-theme-default',
            scope: $scope
        });


        $.post(MY_CONSTANT.url + '/api/drivers/delete',
            {
                accessToken: $cookieStore.get('obj').accesstoken,
                userId: delete_driver_id
            }, function (data) {
                if(typeof data == "string")
                {
                    data = JSON.parse(data);
                };
                console.log(data);

                if (data.flag == responseCode.SUCCESS) {
                    $scope.displaymsg = "Driver deleted successfully";
                }
                else if (data.status == responseCode.INVALID_ACCESS_TOKEN){
                    $state.go('page.login');
                }
                else {
                    $scope.displaymsg = data.error;
                }
                $scope.list = $scope.list.filter(function (el) {
                    return el.user_id !== delete_driver_id;
                });
                console.log($scope.list);
                $scope.$apply();
                ngDialog.open({
                    template: 'display_msg_modalDialog',
                    className: 'ngdialog-theme-default',
                    showClose: false,
                    scope: $scope
                });
            });


    };

    /*--------------------------------------------------------------------------
     * ---------------- funtion to open dialog for edit driver --------------
     --------------------------------------------------------------------------*/
    $scope.editdriver = function (driverId,driver_car_no,driver_license_number,driver_social_security_number,car_type,phone_no,driver_paypal_id,driver_car_image,vehicle_emissions,co2_produced) {
        console.log("car_type"+car_type);
        var res = phone_no.substring(0, 1);
        if (res == '+')
        phone_no=phone_no.substring(1);


        if(driver_car_image==''){
           $scope.uploaded_vehicle_img='app/img/noimg.png';
        }
        else{
            $scope.uploaded_vehicle_img=driver_car_image;
        }
        $scope.value = true;
        $scope.updateDriverData={};
        $scope.updateDriverData.driver_id=driverId;
        $scope.updateDriverData.driver_car_no=driver_car_no;
        $scope.updateDriverData.driver_license_number=driver_license_number;
        $scope.updateDriverData.driver_social_security_number=driver_social_security_number;
        $scope.updateDriverData.car_type=car_type;
        $scope.updateDriverData.phone_no=phone_no;
        $scope.updateDriverData.driver_paypal_id='xyz@paypal.com';
        $scope.updateDriverData.vehicle_emissions=vehicle_emissions;
        $scope.updateDriverData.co2_produced=co2_produced;
        $scope.file = '';

        console.log("car_type " + car_type);

        ngDialog.open({
            template: 'editDriverDialog',
            className: 'ngdialog-theme-default',
            showClose: false,
            scope: $scope
        });
    };

    /*--------------------------------------------------------------------------
     * ---------------- funtion to upload image ------------------------------
     --------------------------------------------------------------------------*/
    $scope.file_to_upload = function (files) {
        console.log(files);
        $scope.vehicle_image=files[0];
       // console.log("new image"+vehicle_image);

        var file = files[0];
        var imageType = /image.*/;
        if (!file.type.match(imageType)) {

        }
        var img = document.getElementById("car_image");
        img.file = file;
        var reader = new FileReader();
        reader.onload = (function (aImg) {
            return function (e) {
                aImg.src = e.target.result;
            };
        })(img);
        reader.readAsDataURL(file);


    }

    /*--------------------------------------------------------------------------
     * ---------------- funtion to update driver -------------------------------
     --------------------------------------------------------------------------*/
    $scope.updateDriver = function (updateDriverData) {

        console.log($scope.updateDriverData.phone_no);
        $scope.updateDriverData.contact_no="+"+updateDriverData.phone_no;

        console.log($scope.vehicle_image);
        console.log($scope.updateDriverData.car_type);
        //var a = $scope.vehicle_image;
        //console.log("a "+a);
        //
        //if(a == undefined){
        //    console.log("a "+a);
        //}

        var formData = new FormData();
        formData.append('access_token', $cookieStore.get('obj').accesstoken);
        formData.append('driver_id', $scope.updateDriverData.driver_id);
        formData.append('driver_vehicle_number', $scope.updateDriverData.driver_car_no);
        formData.append('driver_license_number', $scope.updateDriverData.driver_license_number);
        formData.append('social_security_number', $scope.updateDriverData.driver_social_security_number);
        formData.append('driver_car_type', $scope.updateDriverData.car_type);
        formData.append('phone_no', $scope.updateDriverData.contact_no);
        formData.append('driver_paypal_id', 'xyz@paypal.com');
        formData.append('driver_vehicle_image', $scope.vehicle_image);
        formData.append('vehicle_emissions', updateDriverData.vehicle_emissions);
        formData.append('co2_produced_per_km', updateDriverData.co2_produced);

        $.ajax({
            type: 'POST',
            url: MY_CONSTANT.url + '/approve_driver',
            dataType: "json",
            data: formData,
            async: false,
            processData: false,
            contentType: false,
            success: function (data) {
                if (data.status == responseCode.SUCCESS) {
                    $scope.displaymsg = "Driver Updated Successfully";
                }
                else if (data.status == responseCode.INVALID_ACCESS_TOKEN) {
                    $state.go('page.login');
                }
                else {
                    $scope.displaymsg = data.message;
                }
                //$scope.$apply();
                ngDialog.close({
                    template: 'editDriverDialog',
                    className: 'ngdialog-theme-default',
                    scope: $scope
                });
                ngDialog.open({
                    template: 'display_msg_modalDialog',
                    className: 'ngdialog-theme-default',
                    showClose: false,
                    scope: $scope
                });

            }
        });


        return false;

    };
//==============================================================================================================================
//==========================================================password dialog-====================================================
//==============================================================================================================================
    $scope.openPasswordDialog = function(id){

        $scope.driver_pass_id = id
        ngDialog.open({
            template: 'password_dialog',
            className: 'ngdialog-theme-default',
            showClose: false,
            scope: $scope
        });

    }
//==============================================================================================================================
//==========================================================password dialog-====================================================
//==============================================================================================================================
    $scope.changeDriverPass = function(driver){

        $.post(MY_CONSTANT.url + '/api/drivers/changePassword',
            {
                accessToken: $cookieStore.get('obj').accesstoken,
                userId: $scope.driver_pass_id,
                password:driver.password

            }, function (data) {
                //data = JSON.parse(data);
                console.log(data);
                if (data.flag == responseCode.SUCCESS) {
                    $scope.driverpass.successMsg = "Password Changed Successfully.";
                    $scope.$apply();
                    setTimeout(function () {
                        $scope.driverpass.successMsg = "";
                        $scope.$apply();
                        ngDialog.close({
                            template: 'password_dialog',
                            className: 'ngdialog-theme-default',
                            showClose: false,
                            scope: $scope
                        });
                    }, 3000);
                }
                else if (data.status == responseCode.INVALID_ACCESS_TOKEN){
                    $state.go('page.login');
                }
                else {
                    $scope.driverpass.errorMsg = data.error;
                    $scope.$apply();
                    setTimeout(function () {
                        $scope.driverpass.errorMsg = "";
                        $scope.$apply();
                        ngDialog.close({
                            template: 'password_dialog',
                            className: 'ngdialog-theme-default',
                            showClose: false,
                            scope: $scope
                        });
                    }, 3000);
                }
            });

    }

});