/**
 * Created by sanjay on 3/28/15.
 */
App.controller('PromoCodeController', function ($scope, $http, $route, $state, $cookies, $cookieStore, MY_CONSTANT, $timeout, ngDialog, responseCode,promoCode,promoService) {

    'use strict';
    $scope.promo_code_id = '';
    $scope.delete_driver_id = '';
    $scope.editReferral = {};
    $scope.showloader=true;
    $scope.exportData = function () {
        alasql('SELECT * INTO CSV("promocode.csv",{headers:true}) FROM ?',[$scope.excelList]);
    };

    /*--------------------------------------------------------------------------
     * ------------------- API HIT ON PAGE REFRESH -----------------------------
     --------------------------------------------------------------------------*/
    //var driver_details = function () {
    $.post(MY_CONSTANT.url + '/list_promo_details', {
            access_token: $cookieStore.get('obj').accesstoken
        },
        function (data) {
            $scope.showloader=false;

            var dataArray = [];
            var excelArray = [];
            data = JSON.parse(data);
            console.log(data);

            if(data.status == responseCode.SUCCESS) {

                var promoCodeList = data.data;

                promoCodeList.forEach(function (column) {
//==========================================================================================================================
//============================================================ data for excel =============================================
//==========================================================================================================================
                    var e ={};
                    e.ID = column.id;
                    e.Promotion_Code = column.promotion_code;
                    e.Description = column.description;
                    e.Type = column.type;
                    e.Days_Validity = column.days_validity;
                    e.Num_Coupons = column.num_coupons;
                    e.Num_Coupons_Per_User =column.no_coupons_per_user;
                    e.Max_Discount = column.maximum;
                    e.Perc_Discount = column.discount;

                    //******************** date conversion ******************************************
                    e.Start_Date = column.start_date;
                    e.End_Date = column.end_date;

                    if (e.Start_Date == "0000-00-00 00:00:00") {
                        e.Start_Date = "N/A";
                    }
                    else if (e.Start_Date != "") {
                        e.Start_Date = moment(e.Start_Date).format("YYYY/MM/DD") ;
                        if(e.Start_Date == 'Invalid date'){
                            e.Start_Date = '';
                        }


                    } else {
                        e.Start_Date = "N/A";
                    }
                    if (e.End_Date == "0000-00-00 00:00:00") {
                        e.End_Date = "N/A";
                    }
                    else if (e.End_Date != "") {
                        e.End_Date = moment(e.End_Date).format("YYYY/MM/DD") ;
                        if(e.End_Date == 'Invalid date'){
                            e.End_Date = '';
                        }

                    } else {
                        e.End_Date = "N/A";
                    }


                    //******************** type of promo code ******************************************
                    if (e.type == 1) {
                        e.perc_discount = "--"
                    }
                    else if(e.type == 2){
                        e.max_discount = "--"
                    }
                    excelArray.push(e);


//==========================================================================================================================
//============================================================  end data for excel =============================================
//==========================================================================================================================
                    var d = {   };

                    d.id = column.id;
                    d.promotion_code = column.promotion_code;
                    d.description = column.description;
                    d.type = column.type;
                    d.days_validity = column.days_validity;
                    d.num_coupons = column.num_coupons;
                    d.promo_image = column.promo_image;
                    d.max_discount = column.maximum;
                    d.perc_discount = column.discount;
                    d.no_coupons_per_user =column.no_coupons_per_user;
                    d.editpromo_url = "/app/edit-promo-code/" +  column.id;
                    d.engagement_url = "/app/engagement-list/" +  column.promotion_code;
                    d.is_deleted = column.is_deleted;


                    //******************** date conversion ******************************************
                    d.start_date = column.start_date;
                    d.end_date = column.end_date;

                    if (d.start_date == "0000-00-00 00:00:00") {
                        d.start_date = "N/A";
                    }
                    else if (d.start_date != "") {
                        d.start_date = moment(d.start_date).format("YYYY/MM/DD") ;
                        if(d.start_date == 'Invalid date'){
                            d.start_date = '';
                        }

                       
                    } else {
                        d.start_date = "N/A";
                    }
                    if (d.end_date == "0000-00-00 00:00:00") {
                        d.end_date = "N/A";
                    }
                    else if (d.end_date != "") {
                        d.end_date = moment(d.end_date).format("YYYY/MM/DD") ;
                        if(d.end_date == 'Invalid date'){
                            d.end_date = '';
                        }

                    } else {
                        d.end_date = "N/A";
                    }


                    //******************** type of promo code ******************************************
                    if (d.type == 1) {
                        d.perc_discount = "--"
                    }
                    else if(d.type==2){
                        d.max_discount = "--"
                    }

                    dataArray.push(d);
                });

                $scope.$apply(function () {
                    $scope.list = dataArray;
                    $scope.excelList = excelArray;

                    // Define global instance we'll use to destroy later
                    var dtInstance;

                    $timeout(function () {
                        if (!$.fn.dataTable)
                            return;
                        dtInstance = $('#datatable2').dataTable({
                            'paging': true, // Table pagination
                            'ordering': true, // Column ordering
                            'info': true, // Bottom left status text
                            // Text translation options
                            // Note the required keywords between underscores (e.g _MENU_)
                            oLanguage: {
                                sSearch: 'Search all columns:',
                                sLengthMenu: '_MENU_ records per page',
                                info: 'Showing page _PAGE_ of _PAGES_',
                                zeroRecords: 'Nothing found - sorry',
                                infoEmpty: 'No records available',
                                infoFiltered: '(filtered from _MAX_ total records)'
                            },
                            "aoColumnDefs": [
                                { 'bSortable': false, 'aTargets': [10] }
                            ]
                        });
                        var inputSearchClass = 'datatable_input_col_search';
                        var columnInputs = $('tfoot .' + inputSearchClass);

                        // On input keyup trigger filtering
                        columnInputs
                            .keyup(function () {
                                dtInstance.fnFilter(this.value, columnInputs.index(this));
                            });
                    });
                    // When scope is destroyed we unload all DT instances
                    // Also ColVis requires special attention since it attaches
                    // elements to body and will not be removed after unload DT
                    $scope.$on('$destroy', function () {
                        dtInstance.fnDestroy();
                        $('[class*=ColVis]').remove();
                    });
                });
            }

            else if (data.status == responseCode.INVALID_ACCESS_TOKEN){
                $state.go('page.login');
            }
        });


    /*--------------------------------------------------------------------------
     * --------- funtion to refresh page ---------------------------------------
     --------------------------------------------------------------------------*/
    $scope.refreshPage = function () {
        $state.reload();
        ngDialog.close({
            template: 'display_msg_modalDialog',
            className: 'ngdialog-theme-default',
            scope: $scope
        });

    };

    /*--------------------------------------------------------------------------
     * ---------------- funtion to open modal for delete promo code ------------
     --------------------------------------------------------------------------*/
    $scope.deletePromocode_popup = function (promo_code_id) {

        $scope.promo_code_id = promo_code_id;

        ngDialog.open({
            template: 'delete_promocode_modalDialog',
            className: 'ngdialog-theme-default',
            showClose: false,
            scope: $scope
        });
    };

    /*--------------------------------------------------------------------------
     * -------------------------funtion to delete promo code -------------------
     --------------------------------------------------------------------------*/
    $scope.delete_promo = function (promo_code_id) {

        ngDialog.close({
            template: 'delete_promocode_modalDialog',
            className: 'ngdialog-theme-default',
            scope: $scope
        });

        $.post(MY_CONSTANT.url + '/delete_promo_details',
            {
                access_token: $cookieStore.get('obj').accesstoken,
                id: promo_code_id
            }, function (data) {
                data = JSON.parse(data);

                if (data.status == responseCode.SUCCESS) {
                    $scope.displaymsg = "Promo Code blocked successfully.";
                }

                else if (data.status == responseCode.INVALID_ACCESS_TOKEN){
                    $state.go('page.login');
                }
                else {
                    $scope.displaymsg = data.message;
                }
                $scope.$apply();
                ngDialog.open({
                    template: 'display_msg_modalDialog',
                    className: 'ngdialog-theme-default',
                    showClose: false,
                    scope: $scope
                });
            });
    };

    /*--------------------------------------------------------------------------
     * ---------------- funtion to edit promo code ------------------------------
     --------------------------------------------------------------------------*/
    $scope.editPromocode_popup = function (id) {
        promoService.add_promo_id(id);
        $state.go('app.editpromocode');
    };


    /*--------------------------------------------------------------------------
     * ---------------- funtion to add promo code ------------------------------
     --------------------------------------------------------------------------*/
    $scope.AddPromoCodeDialog = function () {
        $state.go('app.addpromocode');
    };

    /*--------------------------------------------------------------------------
     * ---------------- funtion to show referral codes -------------------------
     --------------------------------------------------------------------------*/
    $scope.showReferralCode = function () {
        $.post(MY_CONSTANT.url + '/list_refferal_details',
            {
                access_token: $cookieStore.get('obj').accesstoken
            }, function (data) {
                data = JSON.parse(data);
                console.log(data);

                if (data.status == responseCode.SUCCESS) {

                    var referral_code = data.data[0];
                    $scope.referral = {
                        perc_off :referral_code.discount,
                        max_off:referral_code.maximum,
                        coupon_id:referral_code.coupon_id
                    }

                    ngDialog.open({
                        template: 'referral_code_popup',
                        className: 'ngdialog-theme-default',
                        showClose: false,
                        scope: $scope
                    });


                }

                else if (data.status == responseCode.INVALID_ACCESS_TOKEN){
                    $state.go('page.login');
                }


            });

    };


    //====================================================================================
    //==================================edit referral code ===============================
    //====================================================================================

    $scope.submitReferral = function(){

        $.post(MY_CONSTANT.url + '/update_refferal_details',
            {
                access_token: $cookieStore.get('obj').accesstoken,
                coupon_id:$scope.referral.coupon_id,
                discount:$scope.referral.perc_off,
                maximum:$scope.referral.max_off



            }, function (data) {
                data = JSON.parse(data);
                console.log(data);

                if (data.status == responseCode.SUCCESS) {
                    $scope.editReferral.successMsg = "Referral Code Updated Successfully.";

                }

                else if (data.status == responseCode.INVALID_ACCESS_TOKEN){
                    $state.go('page.login');
                }
                else{
                    $scope.editReferral.errorMsg = data.message;

                }
                $scope.$apply();
                setTimeout(function () {
                    $scope.editReferral.successMsg = "";
                    $scope.editReferral.errorMsg = "";
                    $scope.$apply();
                    ngDialog.close({
                        template: 'referral_code_popup',
                        className: 'ngdialog-theme-default',
                        showClose: false,
                        scope: $scope
                    });
                }, 3000);


            });


    }

    /*--------------------------------------------------------------------------
     * ---------------- date conversion function -------------------------------
     --------------------------------------------------------------------------*/
    function dateConversion(dte) {
        var dteSplit1 = dte.split("T");
        var date = dteSplit1[0];
        return date;

    };

});