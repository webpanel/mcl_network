App.controller('ChanePasswordController', function ($scope, $http,$state, $cookies,$location , $cookieStore, MY_CONSTANT, $state,responseCode) {

    $scope.changePass = {};

    $scope.changePassword = function () {
        $scope.errorMsg = '';


        if($scope.changePass.new_password != $scope.changePass.confirmpassword){
            $scope.errorMsg = "Both Passwords do not match.";

            setTimeout(function () {
                $scope.errorMsg = "";
                $scope.$apply();
            }, 3000);

        }
        else if($scope.changePass.new_password == $scope.changePass.old_password){
            $scope.errorMsg = "New password must be different from old password.";

            setTimeout(function () {
                $scope.errorMsg = "";
                $scope.$apply();
            }, 3000);

        }
        else{
            $.post(MY_CONSTANT.url + '/api/admin/updateAdminPassword',
                {
                    accessToken: $cookieStore.get('obj').accesstoken,
                    oldPassword: $scope.changePass.old_password,
                    newPassword: $scope.changePass.new_password
                }).then(
                function (data) {

                    if(typeof data == "string")
                    {
                        data = JSON.parse(data);
                    }

                    if (data.flag == responseCode.SHOW_ERROR_MESSAGE) {
                        $scope.errorMsg = "Incorrect Password";
                        $scope.$apply();
                        setTimeout(function () {
                            $scope.errorMsg = "";
                            $scope.$apply();
                        }, 3000);
                    }
                    else if(data.flag == responseCode.SUCCESS) {
                        $scope.successMsg = "Password changed successfully.";
                        $scope.$apply();
                        setTimeout(function () {
                            $scope.successMsg = "";
                            // $cookieStore.remove('obj');
                            $state.go('app.dashboard');
                            $scope.$apply();
                        }, 3000);

                    }
                    else {
                        $scope.errorMsg = data.error;
                        $scope.$apply();
                        setTimeout(function () {
                            $scope.errorMsg = "";
                            $scope.$apply();
                        }, 3000);
                    }
                });
        }

    };


});

