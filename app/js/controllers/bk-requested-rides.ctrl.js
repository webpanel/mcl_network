
App.controller('BkRequestedRidesController', function ($scope, $http,$state, $cookies, $cookieStore, MY_CONSTANT, $timeout,ngDialog,responseCode) {

    'use strict';
    $scope.showloader=true;
    $scope.displaymsg = "";
    if($cookieStore.get('type')==2){
        $scope.showcsvbtn = 0;
    }
    else
    {
        $scope.showcsvbtn = 1;
    }

    var dtInstance;

    var d = new Date((new Date()).getTime());
    var offset = d.getTimezoneOffset();

    $("#ridescsv").attr("href", MY_CONSTANT.url + "/api/bookedRides?accessToken=" + $cookieStore.get('obj').accesstoken + "&timezone=" + offset + "&csv=" + "1");


    $timeout(function () {
        if (!$.fn.dataTable) return;

        dtInstance = $('#requested_datatable').dataTable({
            "bProcessing": true,
            'paging': true,  // Table pagination
            'ordering': true,  // Column ordering
            'info': true,  // Bottom left status text
            // Text translation options
            // Note the required keywords between underscores (e.g _MENU_)
            "bServerSide": true,
            sAjaxSource: MY_CONSTANT.url + "/api/bookedRides?accessToken=" + $cookieStore.get('obj').accesstoken + "&timezone=" + offset,
            oLanguage: {
                sSearch: 'Search all columns:',
                sLengthMenu: '_MENU_ records per page',
                info: 'Showing page _PAGE_ of _PAGES_',
                zeroRecords: 'Nothing found - sorry',
                infoEmpty: 'No records available',
                infoFiltered: '(filtered from _MAX_ total records)'
            },
            "aaSorting": [[0,"desc"]]
            //"columnDefs":[{"targets":7, "data":"name", "render": function(data,type,full,meta)
            //{ return '<a class="resend"  class="text-info" >RESEND</a><br>'+'<a  class="text-success" class="complete_ride">COMPLETE</a><br>'+'<a class="text-danger" class="cancel_ride"">CANCEL</a>'
            //}}]
        });

        $scope.showloader=false;
        var inputSearchClass = 'datatable_input_col_search';
        var columnInputs = $('tfoot .' + inputSearchClass);

        // On input keyup trigger filtering
        columnInputs
            .keyup(function () {
                dtInstance.fnFilter(this.value, columnInputs.index(this));
            });

    });

    // When scope is destroyed we unload all DT instances
    // Also ColVis requires special attention since it attaches
    // elements to body and will not be removed after unload DT
    $scope.$on('$destroy', function () {
        dtInstance.fnDestroy();
        $('[class*=ColVis]').remove();
    });
    //});

    /*--------------------------------------------------------------------------
     * --------- funtion to refresh page ---------------------------------------
     --------------------------------------------------------------------------*/
    $scope.closePopup = function () {
        ngDialog.close({
            template: 'display_msg_modalDialog',
            className: 'ngdialog-theme-default',
            scope: $scope
        });

    };

    // socket connection
    var socket = io.connect(MY_CONSTANT.url_booking);
    socket.on("admin:rideStatus",function(data){
        var newData = data.data.rideData;
        console.log(data);
        switch(data.flag){
            case 7:   //WHEN RIDE CANCELED BY Customer
                $('#bookingStatus_'+newData.bookingId).css('background-color','red');
                $('#bookingStatus_'+newData.bookingId).css('padding','5px');
                $('#bookingStatus_'+newData.bookingId).css('color','white');
                $('#bookingStatus_'+newData.bookingId).text(data.status);
                break;
            case 100:
                var trStr = '<tr>' +
                    '<td>' + newData.bookingId + '</td>' +
                    '<td>0</td>' +
                    '<td id="bookingStatus_'+newData.bookingId+'">' + data.status + '</td>' +
                    '<td> <span> Car Type : ' + newData.carName + '</span></td>' +
                    '<td>' + getBookingTime(newData, offset) + '</td>' +
                    '<td>' + getRideTime(newData, offset) + '</td>' +
                    '<td> <span> Distance(miles): ' + newData.distance + '</span><br> <span> Bags Count : '+newData.bagsCount+' </span>  <br> ' +
                    '<span> Passanger Count : '+newData.passengerCount+' </span>  Hours Count : '+newData.hoursCount+' </span> <br> <br> <span>Is Hourly : '+newData.isHourly+'</span></td>' +
                    '<td> <span> Pickup : ' + newData.pickupLocationAddress + '</span><br><span> DropOff : '+newData.destinationLocationAddress+'</span></td>' +
                    '<td> <span> Name : ' + newData.userData.firstName + ' ' + newData.userData.lastName + '</span><br><span> Phone : '+newData.userData.phoneNo+'</span></td>' +
                    '<td> <span> Fare : ' + newData.totalFare + '</span><br><span> Toll : '+newData.toll+'</span> <br>' +
                    '<span> Tax : ' + newData.tax + '</span><br><span> Cancellation Charges : '+newData.cancellationCharges+'</span></td>' +
                    '<td>'+newData.comments+'</td>' +
                    '</tr>';
                $('#requested_datatable > tbody').prepend($(trStr)[0]);
                break;
        }
    });

    function getBookingTime(rideData , timezone) {

        return '<span> E: ' + getTime(rideData.bookingTime, timezone) + ' | ' + getDate(rideData.bookingTime, timezone) + ' </span>'; // resultAllRides[i].arrivingTime;

    }

    function getRideTime(rideData , timezone) {

        return '<span> E: ' + getTime(rideData.requestTime, timezone) + ' | ' + getDate(rideData.requestTime, timezone) + ' </span>';

    }

    function getTime(date) {

        if (date == "0000-00-00 00:00:00" || date == null) {
            return 'N.A.';
        }

        var dtOb = new DateObject(date);


        return dtOb.time;

    }

    function getDate(date, timezone) {
        if (date == "0000-00-00 00:00:00") {
            return 'N.A.';
        }

        var dtOb = new DateObject(date, timezone);

        return dtOb.date;

    }

    function DateObject(date, timezone) {

        this.date = new Date(date);

        if (typeof timezone != "undefined")

        {
            // time zone in minutes

            this.date.setTime(this.date.getTime() - (timezone * 60 * 1000));

        }

        this.dateString = this.date.toString();
        this.dateStringArr = this.dateString.split(" ");
        this.hour = appendZero(this.date.getHours() % 12);
        this.hour = this.hour == '00' ? '12' : this.hour;
        this.second = appendZero(this.date.getSeconds());
        this.minute = appendZero(this.date.getMinutes());
        this.amPm = this.hour > 12 ? 'PM' : 'AM';
        this.dayName = this.dateStringArr[0];
        this.monthName = this.dateStringArr[1];

        this.time = this.hour + ":" + this.minute + " " + this.amPm;

        this.date = this.date.getDate() + "/" + (this.date.getMonth() + 1) + "/" + this.date.getFullYear();

    }

    function appendZero(num) {
        return num < 10 ? '0' + num : num;
    }

});

