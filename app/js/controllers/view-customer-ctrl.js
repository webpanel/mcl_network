/**
 * Created by salma on 22/8/15.
 */
/**
 * Created by salma on 22/8/15.
 */

App.controller('ViewCustomerController', function ($scope, $http, $cookies, $cookieStore, MY_CONSTANT, $state,ngDialog,responseCode,$stateParams) {
    /*--------------------------------------------------------------------------
     * ---------------- get driver's data by using driver id -------------
     --------------------------------------------------------------------------*/
    $.post(MY_CONSTANT.url + '/api/customers/customer', {
            accessToken: $cookieStore.get('obj').accesstoken,
            userId: $stateParams.customer_id
        },
        function (data) {
            console.log(data);
            if(typeof data == "string")
            {
                data = JSON.parse(data);
            }
            if (data.flag == responseCode.SUCCESS) {
                var customerList = data.data.customer[0];
                console.log(customerList);
                $scope.user_name = customerList.firstName + " " +customerList.lastName ;
                $scope.phone_no = customerList.phoneNo;
                $scope.email = customerList.email;
                $scope.rating = customerList.totalRating;
                $scope.editcustomer_url = "/app/edit-customer/" +  $stateParams.customer_id;
                $scope.isBlocked = customerList.isBlocked;
                console.log("$scope.is_blocked"+$scope.isBlocked);
            }
            else if (data.flag == responseCode.INVALID_ACCESS_TOKEN){
                $state.go('page.login');
            }
            $scope.$apply();
        });

    /*--------------------------------------------------------------------------
     * ------------- funtion to open modal for delete passenger ----------------
     --------------------------------------------------------------------------*/
    $scope.deletepassenger_popup = function () {
        ngDialog.open({
            template: 'delete_passenger_modalDialog',
            className: 'ngdialog-theme-default',
            scope: $scope
        });
    };


    /*--------------------------------------------------------------------------
     * -------------------------funtion to delete passenger --------------------------
     --------------------------------------------------------------------------*/
    $scope.delete_passenger = function () {
        ngDialog.close({
            template: 'delete_passenger_modalDialog',
            className: 'ngdialog-theme-default',
            scope: $scope
        });

        $.post(MY_CONSTANT.url + '/api/customers/delete',{
            accessToken: $cookieStore.get('obj').accesstoken,
            userId:  $stateParams.customer_id
        }, function (data) {

            if(typeof data == "string")
            {
                data = JSON.parse(data);
            }
            console.log(data);

            $scope.displaymsg = '';

            if (data.flag == responseCode.SUCCESS) {
                $scope.displaymsg2 = "Passenger is deleted successfully.";
            }
            else if (data.status == responseCode.INVALID_ACCESS_TOKEN){
                $state.go('page.login');
            }
            else {
                $scope.displaymsg2 = data.error;
            }
            $scope.$apply();
            ngDialog.open({
                template: 'display_msg_modalDialog2',
                className: 'ngdialog-theme-default',
                scope: $scope
            });
        });
    };


    /*--------------------------------------------------------------------------
     * --------- funtion to open dialog for block or unblock passenger ------------
     --------------------------------------------------------------------------*/
    $scope.blockunblockpassenger_popup = function (is_blocked) {
        $scope.blockunblockmsg = '';
        $scope.is_blocked = is_blocked;

        if (is_blocked == 1) {
            $scope.blockunblockmsg = "Are you sure you want to block this passenger?";
        } else {
            $scope.blockunblockmsg = "Are you sure you want to unblock this passenger?";
        }

        ngDialog.open({
            template: 'block_unblock_passenger_modalDialog',
            className: 'ngdialog-theme-default',
            showClose: false,
            scope: $scope
        });

    };

    /*--------------------------------------------------------------------------
     * ------------------ funtion to block or unblock passenger -------------------
     --------------------------------------------------------------------------*/
    $scope.do_block_unblock_passenger = function (is_blocked) {
        $.post(MY_CONSTANT.url + '/api/customers/blockUnblock',
            {
                accessToken: $cookieStore.get('obj').accesstoken,
                "userId": $stateParams.customer_id,
                "blockStatus": is_blocked
            }, function (data) {
                if(typeof data == "string")
                {
                    data = JSON.parse(data);
                }
                console.log(data);

                if (data.flag == responseCode.SUCCESS) {
                    if (is_blocked == 1) {
                        $scope.displaymsg = "Passenger is blocked successfully.";
                    } else {
                        $scope.displaymsg = "Passenger is unblocked successfully.";
                    }
                }
                else if (data.flag == responseCode.INVALID_ACCESS_TOKEN){
                    $state.go('page.login');
                }
                else {
                    $scope.displaymsg = data.error;
                }
                $scope.$apply();
                ngDialog.close({
                    template: 'block_unblock_passenger_modalDialog',
                    className: 'ngdialog-theme-default',
                    scope: $scope
                });

                ngDialog.open({
                    template: 'display_msg_modalDialog',
                    className: 'ngdialog-theme-default',
                    showClose: false,
                    scope: $scope
                });
            });
    };

    /*--------------------------------------------------------------------------
     * --------- funtion to refresh page ---------------------------------------
     --------------------------------------------------------------------------*/
    $scope.refreshPage = function () {
        $state.reload();
        ngDialog.close({
            template: 'display_msg_modalDialog',
            className: 'ngdialog-theme-default',
            scope: $scope
        });

    };
});




