/**
 * Created by salma on 28/6/16.
 */

App.controller('blogController', function ($scope, $http, $cookies, $cookieStore, MY_CONSTANT, $state,ngDialog,responseCode,$timeout,$stateParams) {


    $scope.showList = function(){
        $scope.showloader = true;
        $.post(MY_CONSTANT.url_booking + '/api/blogs/listBlogs', function (data) {
            $scope.showloader=false;

            //$scope.blogs = data.blogs;
            if(data.flag == responseCode.SUCCESS){
                $scope.blogs = data.data.blogs;
                $scope.$apply(function () {
                    // Define global instance we'll use to destroy later
                    var dtInstance;

                    $timeout(function () {
                        if (!$.fn.dataTable)
                            return;
                        dtInstance = $('#datatable2').dataTable({
                            'paging': true, // Table pagination
                            'ordering': true, // Column ordering
                            'info': true, // Bottom left status text
                            // Text translation options
                            // Note the required keywords between underscores (e.g _MENU_)
                            oLanguage: {
                                sSearch: 'Search all columns:',
                                sLengthMenu: '_MENU_ records per page',
                                info: 'Showing page _PAGE_ of _PAGES_',
                                zeroRecords: 'Nothing found - sorry',
                                infoEmpty: 'No records available',
                                infoFiltered: '(filtered from _MAX_ total records)'
                            },
                            "aoColumnDefs": [
                                { 'bSortable': false, 'aTargets': [] }
                            ]
                        });
                        var inputSearchClass = 'datatable_input_col_search';
                        var columnInputs = $('tfoot .' + inputSearchClass);

                        // On input keyup trigger filtering
                        columnInputs
                            .keyup(function () {
                                dtInstance.fnFilter(this.value, columnInputs.index(this));
                            });
                    });
                    // When scope is destroyed we unload all DT instances
                    // Also ColVis requires special attention since it attaches
                    // elements to body and will not be removed after unload DT
                    $scope.$on('$destroy', function () {
                        dtInstance.fnDestroy();
                        $('[class*=ColVis]').remove();
                    });
                });
            }
        });
    }


    /*================================================
     *      INIT FUNCTION OF ADD/UPDATE
     *================================================*/
    $scope.initBlog = function(){
        $scope.blog={};
        $scope.blog.blogDesc = [];
       if($stateParams.blogId){
           $scope.getBlogDetails($stateParams.blogId);
       }

    }
    $scope.getBlogDetails = function(id){
        $.post(MY_CONSTANT.url_booking + '/api/blogs/getABlog', {
                blogId: id
            }).then(function(data){
                console.log(data);
                $scope.blog = data.data;
                $scope.blog.blogDesc  = data.data.blogDesc[0];
                $scope.$apply();

            },
            function (error) {
                alert(JSON.parse(error.responseText).error)
            });

    };

    /*=================================================
     * Submit Add or Updated data
     *================================================*/
    $scope.addUpdateBlog = function(){
        var formData = new FormData();
        var api;
        if($stateParams.blogId){
            api = "updateABlog" ;  // in case of update
            formData.append("blogId",$scope.blog.blogId);
        }
        else{
            api = "addNewBlog";
            $scope.blog.blogDesc.descId = 5;
            $scope.blog.blogDesc.image = "image url related to this description";
            $scope.blog.blogDesc.imageAlignment ="top/bottom/left/right";
            $scope.blog.blogDesc.imgWidth = 200;
            $scope.blog.blogDesc.imgHeight = 200;

        }
        //formData.append('accessToken',$cookieStore.get('obj').accesstoken);
        //formData.append('title',$scope.blog.title);
        //formData.append('mainImg',$scope.blog.mainImg);
        //formData.append('blogDesc',$scope.blog.blogDesc);
        for(var key in $scope.blog){
            formData.append(key,$scope.blog[key]);
        }
        $.ajax({
            type: 'POST',
            url: MY_CONSTANT.url + '/api/blogs/'+api,
            dataType: "json",
            data: formData,
            async: false,
            processData: false,
            contentType: false,
            success: function (data) {
                console.log(data);
                if(data.flag == responseCode.SUCCESS){
                    $scope.successMsg = "Blog Updated Successfully";
                    $scope.TimeOutError();
                    $scope.$apply();
                }
            },
            error:function(error){
                console.log(error);
                return false;
                $scope.errorMsg = JSON.parse(error.responseText).error;
                $scope.TimeOutError();
                $scope.$apply();
            }
        });

    };
    /*==========================================================
             confirm deletion
     ===========================================================*/
    $scope.confirmDelete = function(id){
        $scope.blogId = id;
        ngDialog.open({
            template: 'confirmDelete',
            className: 'ngdialog-theme-default',
            showClose: false,
            scope: $scope
        });
    };
    $scope.deleteBlog = function(){
        ngDialog.close({
            template: 'confirmDelete',
            className: 'ngdialog-theme-default',
            showClose: false,
            scope: $scope
        });
        $.post(MY_CONSTANT.url + '/api/blogs/deleteABlog', {
            accessToken:$cookieStore.get('obj').accesstoken,
            blogId: $scope.blogId
        }).then(function(data){
                console.log(data);
            if(data.flag == responseCode.SUCCESS){
                $scope.displaymsg = "Blog deleted successfully.";
                ngDialog.open({
                    template: 'display_msg_modalDialog',
                    className: 'ngdialog-theme-default',
                    showClose: false,
                    scope: $scope
                });
                $scope.$apply();
            }

            },
            function (error) {
                alert(JSON.parse(error.responseText).error)
            });
    }

      /*==========================================================
             function on upload of image
     ===========================================================*/
    $scope.file_to_upload = function (files) {
        $scope.blog.mainImg = files[0];
        var file = files[0];
        var imageType = /image.*/;
        if (!file.type.match(imageType)) {

        }
        var img = document.getElementById("blogImage");
        img.file = file;
        var reader = new FileReader();
        reader.onload = (function (aImg) {
            return function (e) {
                aImg.src = e.target.result;
            };
        })(img);
        reader.readAsDataURL(file);
    }
    /*=================================================
     *              Timeout function
     ================================================*/
    $scope.TimeOutError = function () {
        setTimeout(function () {
            $scope.errorMsg = "";
            $scope.successMsg = "";
            $scope.$apply();
        }, 3000);

    }

});


