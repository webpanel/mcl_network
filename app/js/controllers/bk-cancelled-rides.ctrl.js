/**
 * Created by salma on 15/11/15.
 */
/**
 * Created by salma on 15/11/15.
 */

App.controller('BkCancelledRidesController', function ($scope, $http,$state, $cookies, $cookieStore, MY_CONSTANT, $timeout,ngDialog,responseCode) {

    'use strict';
    $scope.is_driver = 0;
    $scope.is_customer = 0;
    $scope.start_date = "0/0/0";
    $scope.end_date = "0/0/0";
    $scope.is_apply_filter = 0;
    $scope.driver_name = "";
    $scope.customer_name = "";
    $scope.rides = "";
    //type of USER
    $scope.select_user=[{
        id:0,
        name: 'Driver'
    },
        {
            id:1,
            name: 'Customer'
        }
    ]
    $scope.minDate = new Date();
    $scope.maxDate = new Date();

    $scope.today = function() {
        $scope.rides.start_date = new Date();
    };

    $scope.clear = function () {
        $scope.rides.start_date = null;
    };


    $scope.toggleMin = function() {
        $scope.minDate = $scope.minDate ? null : new Date();
    };
    $scope.toggleMin();

    $scope.open = function($event) {
        console.log("jdfhjr");
        $event.preventDefault();
        $event.stopPropagation();

        $scope.opened = true;
        $scope.opened1=false;
    };

    $scope.dateOptions = {
        formatYear: 'yy',
        startingDay: 1
    };

    $scope.initDate = new Date();
    $scope.format = 'yyyy-MM-dd';



    $scope.today = function() {
        $scope.ride.end_date = new Date();
    };
    //$scope.today();

    $scope.clear = function () {
        $scope.ride.end_date = null;
    };

    //// Disable weekend selection
    //$scope.disabled = function(date, mode) {
    //    return ( mode === 'day' && ( date.getDay() === 0 || date.getDay() === 6 ) );
    //};

    $scope.toggleMin = function() {
        $scope.minDate = $scope.minDate ? null : new Date();
    };
    $scope.toggleMin();

    $scope.open1 = function($event) {
        $event.preventDefault();
        $event.stopPropagation();

        $scope.opened1 = true;
        $scope.opened = false;

    };

    $scope.dateOptions = {
        formatYear: 'yy',
        startingDay: 1
    };

    $scope.initDate = new Date();
    $scope.format = 'yyyy-MM-dd';
    $scope.showloader=true;
    $scope.displaymsg = "";
    if($cookieStore.get('type')==2){
        $scope.showcsvbtn = 0;
    }
    else
    {
        $scope.showcsvbtn = 1;
    }

    var dtInstance;

    var d = new Date((new Date()).getTime());
    var offset = d.getTimezoneOffset();

    $("#ridescsv").attr("href", MY_CONSTANT.url + "/api/bookedCancelledRides?accessToken=" + $cookieStore.get('obj').accesstoken + "&timezone=" + offset + "&csv=" + "1");

    $timeout(function () {
        if (!$.fn.dataTable) return;
        dtInstance = $('#datatable4').dataTable({
            'paging': true,  // Table pagination
            'ordering': true,  // Column ordering
            'info': true,  // Bottom left status text
            // Text translation options
            // Note the required keywords between underscores (e.g _MENU_)
            "bServerSide": true,
            sAjaxSource: MY_CONSTANT.url + "/api/bookedCancelledRides?accessToken=" + $cookieStore.get('obj').accesstoken + "&timezone=" + offset,
            oLanguage: {
                sSearch: 'Search all columns:',
                sLengthMenu: '_MENU_ records per page',
                info: 'Showing page _PAGE_ of _PAGES_',
                zeroRecords: 'Nothing found - sorry',
                infoEmpty: 'No records available',
                infoFiltered: '(filtered from _MAX_ total records)'
            },
            "aaSorting": [[0,"desc"]]
            //"columnDefs":[{"targets":8, "data":"name", "render": function(data,type,full,meta)
            //{ return '<a class="charge_cancel_ride text-danger">CANCEL</a>'
            //}}]
        });
        $scope.showloader=false;
        var inputSearchClass = 'datatable_input_col_search';
        var columnInputs = $('tfoot .' + inputSearchClass);

        // On input keyup trigger filtering
        columnInputs
            .keyup(function () {
                dtInstance.fnFilter(this.value, columnInputs.index(this));
            });
    });

    // When scope is destroyed we unload all DT instances
    // Also ColVis requires special attention since it attaches
    // elements to body and will not be removed after unload DT
    $scope.$on('$destroy', function () {
        dtInstance.fnDestroy();
        $('[class*=ColVis]').remove();
    });
    //});


    //===========================================================
    //                 APPLY FILTER FUNCTION
    //============================================================
    $scope.addfilter = function(){
        //$scope.is_apply_filter = 1;
        var start_date = $scope.rides.start_date;
        var end_date = $scope.rides.end_date;
        start_date = new Date(start_date);
        end_date = new Date(end_date);

        if(end_date)
            end_date.setDate(end_date.getDate() + 1);
        var days = end_date - start_date;

        if($scope.rides.start_date == '' || $scope.rides.start_date == undefined || $scope.rides.start_date == null){
            $scope.errorMsg = "Please select start date";
            $scope.TimeOutError();
            return false;
        }
        if($scope.rides.end_date == '' || $scope.rides.end_date == undefined || $scope.rides.end_date == null){
            $scope.errorMsg = "Please select End Date";
            $scope.TimeOutError();
            return false;
        }
        if (days <= 0) {
            $scope.errorMsg = "Start Date must be less than End Date";
            $scope.TimeOutError();
            return false;
        }


        var start_date = $("#start_date").val();
        var end_date = $("#end_date").val();
        start_date = start_date + " 00:00:00";
        var start_date = new Date(start_date);
        start_date = start_date.toUTCString();

        console.log(start_date);

        end_date = end_date + " 23:59:00";
        end_date = new Date(end_date);
        end_date = end_date.toUTCString();
        console.log(end_date);
        console.log($scope.rides.user);
        if((!angular.isUndefined($scope.rides.user)) || $scope.rides.user !=null){
            if($scope.rides.user==0){
                if(!angular.isUndefined($scope.rides.user_name)){
                    $scope.driver_name = $scope.rides.user_name;
                    console.log($scope.driver_name)
                    $scope.customer_name = "";
                }

            }
            else if($scope.rides.user==1){
                if(!angular.isUndefined($scope.rides.user_name)){
                    $scope.driver_name = "";
                    $scope.customer_name = $scope.rides.user_name;
                }
            }
        }
        else{
            $scope.driver_name = "";      //in case of nothing is selected
            $scope.customer_name = "";
            console.log($scope.driver_name);
        }


        if ($.fn.DataTable.isDataTable("#datatable4")) {
            $('#datatable4').DataTable().clear().destroy();
        }


        $scope.showloader=true;

        if (!$.fn.dataTable) return;
        dtInstance = $('#datatable4').dataTable({
            'paging': true,  // Table pagination
            'ordering': true,  // Column ordering
            'info': true,  // Bottom left status text
            // Text translation options
            // Note the required keywords between underscores (e.g _MENU_)
            "bServerSide": true,
            sAjaxSource: MY_CONSTANT.url + "/api/bookedCancelledRides?accessToken=" + $cookieStore.get('obj').accesstoken +"&driver_name="+$scope.driver_name+ "&customer_name="+$scope.customer_name+"&is_apply_filter=1&start_date="+start_date+"&end_date="+end_date + "&type=4&timezone=" + offset,
            oLanguage: {
                sSearch: 'Search all columns:',
                sLengthMenu: '_MENU_ records per page',
                info: 'Showing page _PAGE_ of _PAGES_',
                zeroRecords: 'Nothing found - sorry',
                infoEmpty: 'No records available',
                infoFiltered: '(filtered from _MAX_ total records)'
            },
            "aaSorting": [[0,"desc"]]

        });
        $scope.showloader=false;
        var inputSearchClass = 'datatable_input_col_search';
        var columnInputs = $('tfoot .' + inputSearchClass);

        // On input keyup trigger filtering
        columnInputs
            .keyup(function () {
                dtInstance.fnFilter(this.value, columnInputs.index(this));
            });

        var oTable = document.getElementById('datatable4');

//gets rows of table
        var rowLength = oTable.rows.length;

//loops through rows
        for (var i = 0; i < rowLength-1; i++){

            //gets cells of current row
            var oCells = oTable.rows.item(i).cells;

            //gets amount of cells of current row
            var cellLength = oCells.length;

            //loops through each cell in current row
            for(var j = 0; j < cellLength; j++){
                /* get your cell info here */
                var cellVal = oCells.item(j).innerHTML;
                console.log(cellVal);
            }
        }



    }

    /*--------------------------------------------------------------------------
     * -------------------funtion to cancel Rides ----------------------
     --------------------------------------------------------------------------*/
    $('#datatable4').on('click', '.charge_cancel_ride', function (e) {
        console.log("geeee");
        $scope.rideId = e.currentTarget.id;
        console.log($scope.rideId);
        ngDialog.open({
            template: 'charge_confirm',
            className: 'ngdialog-theme-default',
            showClose: false,
            scope: $scope
        });

    });
    $scope.chargeCancelledRide = function(flag){
        console.log(flag);
        ngDialog.close({
            template: 'charge_confirm',
            className: 'ngdialog-theme-default',
            showClose: false,
            scope: $scope
        });
        $.post(MY_CONSTANT.url_booking + '/api/rides/chargeCancellationToCustomer', {
            adminAccessToken: $cookieStore.get('obj').accesstoken,
            rideId: $scope.rideId,
            flag: flag
        }, function (response) {
            if (typeof(response) == "string")
                response = JSON.parse(response);

            if (response.flag == responseCode.SUCCESS) {
                $scope.displaymsg = "Request has been  successfully removed accordingly.";
            }
            else {
                $scope.displaymsg = response.error;
            }
            ngDialog.open({
                template: 'display_msg_modalDialog',
                className: 'ngdialog-theme-default',
                showClose: false,
                scope: $scope
            });
        });
    };
    //================================================================
    //                    remove filter
    //================================================================
    $scope.removeFilter = function(){
        console.log("here");
        $state.reload();
    };


    /*--------------------------------------------------------------------------
     * --------- funtion to refresh page ---------------------------------------
     --------------------------------------------------------------------------*/
    $scope.refreshPage = function () {
        console.log("Refresh page");
        $state.reload();
        ngDialog.close({
            template: 'display_msg_modalDialog',
            className: 'ngdialog-theme-default',
            scope: $scope
        });

    };

    // socket connection
    var socket = io.connect(MY_CONSTANT.url_booking);
    socket.on("admin:rideStatus",function(data){
        var newData = data.data.rideData;
        var carType ='Taxi';
        console.log(data);

        if(data.flag == 6 || data.flag == 7 || data.flag == 12 || data.flag == 13){

            if(data.data.rideData.carType)
                carType='Town Car';

            var paymentBy= 'CARD';
            if (parseInt(newData.requestFrom))
                paymentBy= 'CASH';

            var carNo = newData.carNo;
            if (angular.isUndefined(carNo))
                carNo = 'N.A';

            var dName = newData.driverName;
            var dPhone = newData.driverPhoneNo;
            if (angular.isUndefined(dName)){
                dName = 'N.A';
                dPhone = 'N.A';
            }

            var fare = newData.fare;
            if (angular.isUndefined(carNo))
                fare = 'N.A';

            var trStr = '<tr>'+
                '<td>'+data.rideId+'</td>'+
                '<td> <span> car# : ' + carNo + '</span><br><span> carType : '+carType+'</span></td>' +
                '<td> <span> Pickup : ' + newData.pickupLocationAddress + '</span><br><span> DropOff : '+newData.destinationLocationAddress+'</span></td>' +
                '<td> <span> Name : ' + newData.userName + '</span><br><span> Phone : '+newData.phoneNo+'</span></td>' +
                '<td> <span> Name : ' + dName + '</span><br><span> Phone : '+dPhone+'</span></td>' +
                '<td> <span> Fare : '+ fare +'</span> <br> <span> Tip: 0</span> <br> <span> Payment By: '+paymentBy+'</span></td>'+
                '<td><p id="'+data.rideId+'" style=background-color:red;padding:5px;color:#fff>'+data.status+'</p></td>'+
                '<td>'+getRideTime(newData,offset)+'</td>'+
                '<td>'+data.chargeStatus+'</td>'+
                '</tr>';
            $('#datatable4 > tbody').prepend( $(trStr)[0]);

        }
    });

    function getRideTime(rideData , timezone) {

        return '<span> E: ' + getTime(rideData.requestTime, timezone) + ' | A: ' + getTime(rideData.acceptTime, timezone) + ' <br> D: ' + getTime(rideData.arrivingTime, timezone) + ' | C: ' + getTime(rideData.dropTime, timezone) + ' | ' + getDate(rideData.requestTime, timezone) + ' </span>'; // resultAllRides[i].arrivingTime;

    }

    function getTime(date) {

        if (date == "0000-00-00 00:00:00" || date == null) {
            return 'N.A.';
        }

        var dtOb = new DateObject(date);


        return dtOb.time;

    }

    function getDate(date, timezone) {
        if (date == "0000-00-00 00:00:00") {
            return 'N.A.';
        }

        var dtOb = new DateObject(date, timezone);

        return dtOb.date;

    }

    function DateObject(date, timezone) {

        this.date = new Date(date);

        if (typeof timezone != "undefined")

        {
            // time zone in minutes

            this.date.setTime(this.date.getTime() - (timezone * 60 * 1000));

        }

        this.dateString = this.date.toString();
        this.dateStringArr = this.dateString.split(" ");
        this.hour = appendZero(this.date.getHours() % 12);
        this.hour = this.hour == '00' ? '12' : this.hour;
        this.second = appendZero(this.date.getSeconds());
        this.minute = appendZero(this.date.getMinutes());
        this.amPm = this.hour > 12 ? 'PM' : 'AM';
        this.dayName = this.dateStringArr[0];
        this.monthName = this.dateStringArr[1];

        this.time = this.hour + ":" + this.minute + " " + this.amPm;

        this.date = this.date.getDate() + "/" + (this.date.getMonth() + 1) + "/" + this.date.getFullYear();

    }

    function appendZero(num) {
        return num < 10 ? '0' + num : num;
    }

});

