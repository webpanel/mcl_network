/**
 * Created by sanjay on 3/28/15.
 */
App.controller('activeDriverController', function ($scope, $http, $route, $state, $cookies, $cookieStore, MY_CONSTANT, $timeout, ngDialog, responseCode) {

    'use strict';
    $scope.addAdmin = {};
    $scope.addAdmin.email = '';
    $scope.delete_driver_id = '';
    $scope.exportData = function () {
        alasql('SELECT * INTO CSV("activedrivers.csv",{headers:true}) FROM ?',[$scope.excelList]);
    };
    //type of users
    $scope.type_of_admin=[{
        id:0,
        name: 'Admin'
    },
        {
            id:1,
            name: 'Manager'
        }
    ];


    /*--------------------------------------------------------------------------
     * -------------------funtion to open dialog for add driver ----------------
     --------------------------------------------------------------------------*/
    $scope.AddAdminDialog = function () {
        $scope.addAdmin.email = '';
        ngDialog.open({
            template: 'modalToAddAdmin',
            className: 'ngdialog-theme-default',
            showClose: false,
            scope: $scope,
            preCloseCallback: function () {
                $scope.addAdmin.email='';
                $scope.addAdmin.name = '';
                return true;
            }
        });
    };

    /*--------------------------------------------------------------------------
     * -------------------------funtion to add driver --------------------------
     --------------------------------------------------------------------------*/
    //var driver_details = function () {
    $.post(MY_CONSTANT.url + '/api/dashboard/dashboard_map', {
        accessToken: $cookieStore.get('obj').accesstoken
    }, function (data) {

        var dataArray = [];
        var excelArray = [];
        if(typeof data == "string")
        {
            data = JSON.parse(data);
        };

        if (data.flag == responseCode.SUCCESS) {
            console.log(data);
            var driverList = data.data.drivers;
            driverList.forEach(function (column) {
 //==========================================================================================================================
//============================================================ data for excel =============================================
//==========================================================================================================================
                var e ={};
                e.NAME = column.firstName + " " + column.lastName;
                e.CONTACT = column.phoneNo;
                e.CAR_TYPE = column.carType;
                e.CAR_MODEL = column.carModel;
                e.VEHICLE_NUMBER = column.carNo;
                e.STATUS = (column.status  = '1')?'FREE':'BUSY';
            
              
                excelArray.push(e);


//==========================================================================================================================
//============================================================  end data for excel =============================================
//==========================================================================================================================
                var d = {};
                d.name = column.firstName + " " + column.lastName;
                d.contact = column.phoneNo;
                d.car_type = column.carType;
                d.car_model = column.carModel;
                d.vehicle_num = column.carNo;
                d.status = column.status;
                d.status_show = (column.status  = '1')?'FREE':'BUSY';
                dataArray.push(d);
            });

            $scope.$apply(function () {
                $scope.list = dataArray;
                $scope.excelList = excelArray;
                console.log($scope.list);

                // Define global instance we'll use to destroy later
                var dtInstance;

                $timeout(function () {
                    if (!$.fn.dataTable)
                        return;
                    dtInstance = $('#datatable2').dataTable({
                        'paging': true, // Table pagination
                        'ordering': true, // Column ordering
                        'info': true, // Bottom left status text
                        // Text translation options
                        // Note the required keywords between underscores (e.g _MENU_)
                        oLanguage: {
                            sSearch: 'Search all columns:',
                            sLengthMenu: '_MENU_ records per page',
                            info: 'Showing page _PAGE_ of _PAGES_',
                            zeroRecords: 'Nothing found - sorry',
                            infoEmpty: 'No records available',
                            infoFiltered: '(filtered from _MAX_ total records)'
                        },
                        "aaSorting": [[0,"desc"]],
                        "aoColumnDefs": [
                            { 'bSortable': false, 'aTargets': [5] }
                        ]
                    });
                    var inputSearchClass = 'datatable_input_col_search';
                    var columnInputs = $('tfoot .' + inputSearchClass);

                    // On input keyup trigger filtering
                    columnInputs
                        .keyup(function () {
                            dtInstance.fnFilter(this.value, columnInputs.index(this));
                        });
                });
                // When scope is destroyed we unload all DT instances
                // Also ColVis requires special attention since it attaches
                // elements to body and will not be removed after unload DT
                $scope.$on('$destroy', function () {
                    dtInstance.fnDestroy();
                    $('[class*=ColVis]').remove();
                });
            });
        }
        else if (data.flag == responseCode.INVALID_ACCESS_TOKEN){
            $state.go('page.login');
        }
    });

    /*--------------------------------------------------------------------------
     * --------- funtion to refresh page ---------------------------------------
     --------------------------------------------------------------------------*/
    $scope.refreshPage = function () {
        $state.reload();
        ngDialog.close({
            template: 'display_msg_modalDialog',
            className: 'ngdialog-theme-default',
            scope: $scope
        });

    };


    /*--------------------------------------------------------------------------
     * ---------------- funtion to open modal for delete admin ----------------
     --------------------------------------------------------------------------*/
    $scope.deleteadmin_popup = function (id) {




        $scope.admin_id  = id;

        ngDialog.open({
            template: 'delete_admin_modalDialog',
            className: 'ngdialog-theme-default',
            showClose: false,
            scope: $scope
        });
    };

    /*--------------------------------------------------------------------------
     * -------------------------function to delete admin --------------------------
     --------------------------------------------------------------------------*/
    $scope.delete_admin = function () {

        ngDialog.close({
            template: 'delete_admin_modalDialog',
            className: 'ngdialog-theme-default',
            scope: $scope
        });


        $.post(MY_CONSTANT.url + '/api/admin/deleteAnAdmin',
            {
                accessToken: $cookieStore.get('obj').accesstoken,
                "userId":$scope.admin_id
            }, function (data) {
                if(typeof data == "string")
                {
                    data = JSON.parse(data);
                }
                console.log(data);

                if (data.flag == responseCode.SUCCESS) {
                    $scope.displaymsg = "Admin deleted successfully";
                }
                else if (data.flag == responseCode.INVALID_ACCESS_TOKEN){
                    $state.go('page.login');
                }
                else {
                    $scope.displaymsg = data.error;
                }
                $scope.$apply();
                ngDialog.open({
                    template: 'display_msg_modalDialog',
                    className: 'ngdialog-theme-default',
                    showClose: false,
                    scope: $scope
                });
            });


}
});
