/**
 * Created by Salma on 8/18/15.
 */
App.controller('EditDriverController', function ($scope, $http, $cookies, $cookieStore, MY_CONSTANT, $state,ngDialog,responseCode,$stateParams) {
    //type of users
    $scope.add_driver_img_sent= 0;
    //getting car types in list
    $.post(MY_CONSTANT.url + '/api/vehicles/viewType', {accessToken: $cookieStore.get('obj').accesstoken},
        function (data) {
            var carArray = [];
            if(typeof(data)=='string'){
                data = JSON.parse(data);
            }
            console.log("viewtype");
            console.log(data);

            if (data.flag == responseCode.SUCCESS) {
                var carList = data.data.vehicles;
                carList.forEach(function (column) {
                    var d = {};
                    d.name = column.name;
                    d.id = column.id;
                    d.type = column.type;
                    carArray.push(d);
                });
                $scope.$apply(function () {
                    $scope.carlist = carArray;
                });
            }
            else if (data.flag == responseCode.INVALID_ACCESS_TOKEN) {
                $state.go('page.login');
            }
        });
    /*==========================================================
     function on upload of image
     ===========================================================*/
    $scope.file_to_upload = function (files) {
        console.log(files);

        $scope.edit_driver_image = files[0];
        $scope.edit_driver_img_sent = 1;
        var file = files[0];
        var imageType = /image.*/;
        if (!file.type.match(imageType)) {

        }
        var img = document.getElementById("driver_image");
        img.file = file;
        var reader = new FileReader();
        reader.onload = (function (aImg) {
            return function (e) {
                aImg.src = e.target.result;
            };
        })(img);
        reader.readAsDataURL(file);
    }
    /*--------------------------------------------------------------------------
     * ---------------- get driver's data by using driver id -------------
     --------------------------------------------------------------------------*/
    $.post(MY_CONSTANT.url + '/api/drivers/driver', {
            accessToken: $cookieStore.get('obj').accesstoken,
            userId: $stateParams.driver_id
        },
        function (data) {
            console.log(data);
            if(typeof data == "string")
            {
                data = JSON.parse(data);
            }
            if (data.flag == responseCode.SUCCESS) {
                var driverlist = data.data.drivers[0];
                console.log(driverlist);
                //var dob = driverlist.dob.split("/");
                $scope.editdriver = {
                    email_id:driverlist.email,
                    first_name:driverlist.firstName,
                    last_name:driverlist.lastName,
                    acc_num:driverlist.accountNo,
                    routing_num:driverlist.routingNo,
                    country_code:driverlist.countryCode,
                    mobile_num:driverlist.phoneNo,
                    street_address:driverlist.streetAddress,
                    city:driverlist.city,
                    state:driverlist.state,
                    postal_code:driverlist.zipcode,
                    type_of_vehicle:parseInt(driverlist.carType),
                    dob:driverlist.dob
                    //dob:dob[0]+dob[1]+dob[2]
                }
                console.log($scope.editdriver.dob);
            }
            else if (data.flag == responseCode.INVALID_ACCESS_TOKEN){
                $state.go('page.login');
            }
            $scope.$apply();

        });
    /*==========================================================
     api hit for add admin
     ===========================================================*/

    $scope.updateDriver = function(editdriver){
        //if($scope.edit_driver_img_sent==0){
        //    $scope.errorMsg = "Upload Image";
        //    $scope.TimeOutError();
        //    return false;
        //}
        var formData = new FormData();
        formData.append('accessToken', $cookieStore.get('obj').accesstoken);
        formData.append('userId', $stateParams.driver_id);
        formData.append('email', editdriver.email_id);
        //formData.append('access_token', editdriver.password);
        formData.append('firstName', editdriver.first_name);
        formData.append('lastName', editdriver.last_name);
        formData.append('accountNo', editdriver.acc_num);
        formData.append('routingNo', editdriver.routing_num);
        formData.append('dob', editdriver.dob);
        formData.append('countryCode', "+"+ editdriver.country_code);
        formData.append('phoneNo', editdriver.mobile_num);
        formData.append('streetAddress', editdriver.street_address);
        formData.append('city', " ");
        formData.append('state', " ");
        formData.append('userImage', $scope.edit_driver_image);
        formData.append('carType', editdriver.type_of_vehicle);

        //api hit here
        $.ajax({
            type: 'POST',
            url: MY_CONSTANT.url + '/api/drivers/update',
            dataType: "json",
            data: formData,
            async: false,
            processData: false,
            contentType: false,
            success: function (data) {
                if(typeof(data)=="string"){
                    data = JSON.parse(data);
                }
                console.log(data);
                if (data.flag == responseCode.SUCCESS) {
                    $scope.displaymsg = "Driver Details Updated Successfully.";
                    ngDialog.open({
                        template: 'display_msg_modalDialog',
                        className: 'ngdialog-theme-default',
                        showClose: false,
                        scope: $scope
                    });
                }
                else if (data.flag == responseCode.INVALID_ACCESS_TOKEN) {
                    $state.go('page.login');
                }
                else {
                    $scope.errorMsg = data.error;
                    $scope.TimeOutError();
                    return false;
                }


            },
            error:function(data){
                if(typeof(data)=="string"){
                    data = JSON.parse(data)
                }
                var response = JSON.parse(data.responseText);
                $scope.errorMsg = response.error;
                $scope.TimeOutError();
                return false;
            }
        });
    }

    /*=================================================
     *              Timeout function
     ================================================*/
    $scope.TimeOutError = function () {
        setTimeout(function () {
            $scope.errorMsg = "";
            $scope.$apply();
        }, 3000);

    }
    /*--------------------------------------------------------------------------
     * --------- funtion to refresh page ---------------------------------------
     --------------------------------------------------------------------------*/
    $scope.refreshPage = function () {
        $state.reload();
        ngDialog.close({
            template: 'display_msg_modalDialog',
            className: 'ngdialog-theme-default',
            scope: $scope
        });

    };

});


