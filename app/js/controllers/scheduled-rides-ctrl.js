App.controller('ScheduledridesController', function ($scope, $http, $cookies, $cookieStore, MY_CONSTANT, $timeout,ngDialog,responseCode) {

    'use strict';

    $scope.showloader=true;
    if($cookieStore.get('type')==2){
        $scope.showcsvbtn = 0;
    }
    else
    {
        $scope.showcsvbtn = 1;
    }
    var dtInstance;
    var d = new Date((new Date()).getTime());
    var offset = d.getTimezoneOffset();
    $("#ridescsv").attr("href", MY_CONSTANT.url + "/scheduled_rides?access_token=" + $cookieStore.get('obj').accesstoken + "&timezone=" + offset + "&csv=" + "1");


    $timeout(function () {
        if (!$.fn.dataTable) return;
        dtInstance = $('#datatable2').dataTable({
            'paging': true,  // Table pagination
            'ordering': true,  // Column ordering
            'info': true,  // Bottom left status text
            // Text translation options
            // Note the required keywords between underscores (e.g _MENU_)
            "bServerSide": true,
            sAjaxSource: MY_CONSTANT.url + "/scheduled_rides?access_token=" + $cookieStore.get('obj').accesstoken + "&timezone=" + offset,
            oLanguage: {
                sSearch: 'Search all columns:',
                sLengthMenu: '_MENU_ records per page',
                info: 'Showing page _PAGE_ of _PAGES_',
                zeroRecords: 'Nothing found - sorry',
                infoEmpty: 'No records available',
                infoFiltered: '(filtered from _MAX_ total records)'
            }
        });
        $scope.showloader=false;
        var inputSearchClass = 'datatable_input_col_search';
        var columnInputs = $('tfoot .' + inputSearchClass);

        // On input keyup trigger filtering
        columnInputs
            .keyup(function () {
                dtInstance.fnFilter(this.value, columnInputs.index(this));
            });
    });

    // When scope is destroyed we unload all DT instances
    // Also ColVis requires special attention since it attaches
    // elements to body and will not be removed after unload DT
    $scope.$on('$destroy', function () {
        dtInstance.fnDestroy();
        $('[class*=ColVis]').remove();
    });


    /*--------------------------------------------------------------------------
     * -------------------funtion to ask cancel Schedule Rides ---------------
     --------------------------------------------------------------------------*/
    $('#datatable2').on('click', '.completeride', function(e) {
        $scope.scheduled_ride_id= e.currentTarget.id;

        ngDialog.open({
            template: 'cancel_ride_modalDialog',
            className: 'ngdialog-theme-default',
            showClose: false,
            scope: $scope
        });


    });
    $('#datatable2').on('keypress', '.completeride', function(e) {

        var code = e.keyCode || e.which;
        if( code === 13 ) {
            e.preventDefault();
            return false;
        }
    });


    /*--------------------------------------------------------------------------
     * -------------------funtion to cancel Schedule Ride -------------------
     --------------------------------------------------------------------------*/
    $scope.cancel_schedule_ride = function () {

        ngDialog.close({
            template: 'cancel_ride_modalDialog',
            className: 'ngdialog-theme-default',
            showClose: false,
            scope: $scope
        });

        $.post(MY_CONSTANT.url + '/remove_schedule_ride', {
            access_token: $cookieStore.get('obj').accesstoken,
            pickup_id: $scope.scheduled_ride_id
        }, function (response) {

            //response = JSON.parse(response);

            if (response.status== responseCode.SUCCESS) {
                $scope.displaymsg = "Ride Cancelled successfully.";
            }
            else if (response.status == responseCode.SHOW_ERROR_MESSAGE) {
                $scope.displaymsg = response['status'];
            } else if (response.status == responseCode.ERROR_IN_EXECUTION) {
                $scope.displaymsg = "Something went wrong, please try again later.";
            }

            ngDialog.open({
                template: 'display_msg_modalDialog',
                className: 'ngdialog-theme-default',
                showClose: false,
                scope: $scope
            });
        });
    };

});

