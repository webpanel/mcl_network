App.controller('HelpFaqController', function ($scope, $http, $cookies, $cookieStore, MY_CONSTANT, $state,ngDialog,responseCode) {

    $scope.show_tab1 = 1;
    $scope.show_tab2 = 0;
    $scope.show_tab3 = 0;
    $scope.show_tab4 = 0;
    $scope.show_tab5 = 0;

    $scope.hides_how_about_btn = 1;
    $scope.hide_show_faq_btn = 1;
    $scope.hide_show_terms_btn = 1;
    $scope.hide_show_policy_btn = 1;
    $scope.hide_show_fare_btn = 1;

    $scope.about_us_data = '';
    $scope.help_data = '';
    $scope.terms_data = '';
    $scope.policy_data = '';
    $scope.fare_data = '';

    getAboutData();


    /*--------------------------------------------------------------------------
     *----------------- function to change tab ----------------------------------
     --------------------------------------------------------------------------*/
    $scope.changeTabValue = function (tab) {

        if(tab == 1){
            getAboutData();
            $scope.show_tab1 = 1;
            $scope.show_tab2 = 0;
            $scope.show_tab3 = 0;
            $scope.show_tab4 = 0;
            $scope.show_tab5 = 0;
        }
        else if(tab == 2){
            $scope.getHelpData();
            $scope.show_tab1 = 0;
            $scope.show_tab2 = 1;
            $scope.show_tab3 = 0;
            $scope.show_tab4 = 0;
            $scope.show_tab5 = 0;
        }
        else if(tab == 3){
            $scope.getTermsData();
            $scope.show_tab1 = 0;
            $scope.show_tab2 = 0;
            $scope.show_tab3 = 1;
            $scope.show_tab4 = 0;
            $scope.show_tab5 = 0;
        }
        else if(tab == 4){
            $scope.getPolicyData();
            $scope.show_tab1 = 0;
            $scope.show_tab2 = 0;
            $scope.show_tab3 = 0;
            $scope.show_tab4 = 1;
            $scope.show_tab5 = 0;
        }
        else{
            $scope.getFareData();
            $scope.show_tab1 = 0;
            $scope.show_tab2 = 0;
            $scope.show_tab3 = 0;
            $scope.show_tab4 = 0;
            $scope.show_tab5 = 1;
        }


    }

    /*--------------------------------------------------------------------------
     *----------------- function to get about us data --------------------------
     --------------------------------------------------------------------------*/
    //$scope.getAboutData = function () {
    function getAboutData() {
        $.post(MY_CONSTANT.url + '/read_about_us', {access_token: $cookieStore.get('obj').accesstoken},
            function (response) {
                var response = JSON.parse(response);

                if(response.status == responseCode.SUCCESS) {
                    $scope.about_us_data = response.data.aboutus;
                    $scope.$apply();
                }
                else{
                    $state.go('page.login');
                }

            });
    }

    /*--------------------------------------------------------------------------
     *----------------- function to get help data ------------------------------
     --------------------------------------------------------------------------*/
    $scope.getHelpData = function () {
        $.post(MY_CONSTANT.url + '/read_faq', {access_token: $cookieStore.get('obj').accesstoken},
            function (response) {
                var response = JSON.parse(response);
                if(response.status == responseCode.SUCCESS) {
                    $scope.help_data = response.data.aboutus;
                    $scope.$apply();
                }
                else{
                    $state.go('page.login');
                }
            });
    }

    /*--------------------------------------------------------------------------
     *----------------- function to get terms and rules data -------------------
     --------------------------------------------------------------------------*/
    $scope.getTermsData = function () {
        $.post(MY_CONSTANT.url + '/read_tnc', {
                access_token: $cookieStore.get('obj').accesstoken
            },
            function (response) {
                var response = JSON.parse(response);

                if(response.status == responseCode.SUCCESS) {
                    $scope.terms_data = response.data.aboutus;
                    $scope.$apply();
                }
                else{
                    $state.go('page.login');
                }
            });
    }

    /*--------------------------------------------------------------------------
     *----------------- function to get policy data ----------------------------
     --------------------------------------------------------------------------*/
    $scope.getPolicyData = function () {
        $.post(MY_CONSTANT.url + '/read_privacy_policy', {
                access_token: $cookieStore.get('obj').accesstoken
            },
            function (response) {
                var response = JSON.parse(response);
                if (response.status == responseCode.SUCCESS) {
                    $scope.policy_data = response.data.aboutus;
                    $scope.$apply();
                }
                else {
                    $state.go('page.login');
                }
            });
    }

    /*--------------------------------------------------------------------------
     *----------------- function to get fare data ----------------------------
     --------------------------------------------------------------------------*/
    $scope.getFareData = function () {
        $.post(MY_CONSTANT.url + '/read_fare_details', {
                access_token: $cookieStore.get('obj').accesstoken
            },
            function (response) {
                var response = JSON.parse(response);
                if (response.status == responseCode.SUCCESS) {
                    $scope.fare_data = response.data.aboutus;
                    $scope.$apply();
                }
                else {
                    $state.go('page.login');
                }
            });
    }

    /*--------------------------------------------------------------------------
     *----------------- funtion to edit about us data --------------------------
     --------------------------------------------------------------------------*/
    $scope.editAboutUs = function () {
        $scope.hides_how_about_btn = 0;
    }

    /*--------------------------------------------------------------------------
     *----------------- funtion to edit help data ------------------------------
     --------------------------------------------------------------------------*/
    $scope.editHelp = function () {
        $scope.hide_show_faq_btn = 0;
    }

    /*--------------------------------------------------------------------------
     *----------------- funtion to edit terms & rules data ---------------------
     --------------------------------------------------------------------------*/
    $scope.editTerms = function () {
        $scope.hide_show_terms_btn = 0;
    }

    /*--------------------------------------------------------------------------
     *----------------- funtion to edit privacy policy data --------------------
     --------------------------------------------------------------------------*/
    $scope.editPolicy = function () {
        $scope.hide_show_policy_btn = 0;
    }

    /*--------------------------------------------------------------------------
     *----------------- funtion to edit fare data --------------------
     --------------------------------------------------------------------------*/
    $scope.editFare = function () {
        $scope.hide_show_fare_btn = 0;
    }

    /*--------------------------------------------------------------------------
     *----------------- funtion to submit about us data --------------------------
     --------------------------------------------------------------------------*/
    $scope.submitAboutUs = function () {

        $.post(MY_CONSTANT.url + '/write_about_us',{
                access_token: $cookieStore.get('obj').accesstoken,
                data : $scope.about_us_data
            },
            function (response) {
                var response = JSON.parse(response);

                if (response['status'] == responseCode.SUCCESS) {
                    $scope.displaymsg = "Document updated successfully";
                    ngDialog.open({
                        template: 'display_msg_modalDialog',
                        className: 'ngdialog-theme-default',
                        showClose: false,
                        scope: $scope
                    });

                }
                else if (response['status'] == responseCode.SHOW_ERROR_MESSAGE) {
                    $scope.displaymsg = response['message'];
                    ngDialog.open({
                        template: 'display_msg_modalDialog',
                        className: 'ngdialog-theme-default',
                        showClose: false,
                        scope: $scope
                    });
                } else if (response['status'] == responseCode.ERROR_IN_EXECUTION) {
                    $scope.displaymsg = "Something went wrong, please try again later.";
                    ngDialog.open({
                        template: 'display_msg_modalDialog',
                        className: 'ngdialog-theme-default',
                        showClose: false,
                        scope: $scope
                    });
                }

                $scope.$apply();

            });
    }

    /*--------------------------------------------------------------------------
     *----------------- funtion to submit help data ----------------------------
     --------------------------------------------------------------------------*/
    $scope.submitHelp = function () {

        $.post(MY_CONSTANT.url + '/write_faq',{
                access_token: $cookieStore.get('obj').accesstoken,
                data : $scope.help_data
            },
            function (response) {
                var response = JSON.parse(response);

                if (response['status'] == responseCode.SUCCESS) {
                    $scope.displaymsg = "Document updated successfully";
                    ngDialog.open({
                        template: 'display_msg_modalDialog',
                        className: 'ngdialog-theme-default',
                        showClose: false,
                        scope: $scope
                    });

                }
                else if (response['status'] == responseCode.SHOW_ERROR_MESSAGE) {
                    $scope.displaymsg = response['message'];
                    ngDialog.open({
                        template: 'display_msg_modalDialog',
                        className: 'ngdialog-theme-default',
                        showClose: false,
                        scope: $scope
                    });
                } else if (response['status'] == responseCode.ERROR_IN_EXECUTION) {
                    $scope.displaymsg = "Something went wrong, please try again later.";
                    ngDialog.open({
                        template: 'display_msg_modalDialog',
                        className: 'ngdialog-theme-default',
                        showClose: false,
                        scope: $scope
                    });
                }

                $scope.$apply();

            });


    }

    /*--------------------------------------------------------------------------
     *----------------- funtion to submit terms & rules data -------------------
     --------------------------------------------------------------------------*/
    $scope.submitTerms = function () {

        $.post(MY_CONSTANT.url + '/write_tnc ',{
                access_token: $cookieStore.get('obj').accesstoken,
                data : $scope.terms_data
            },
            function (response) {
                var response = JSON.parse(response);

                if (response['status'] == responseCode.SUCCESS) {
                    $scope.displaymsg = "Document updated successfully";
                    ngDialog.open({
                        template: 'display_msg_modalDialog',
                        className: 'ngdialog-theme-default',
                        showClose: false,
                        scope: $scope
                    });

                }
                else if (response['status'] == responseCode.SHOW_ERROR_MESSAGE) {
                    $scope.displaymsg = response['message'];
                    ngDialog.open({
                        template: 'display_msg_modalDialog',
                        className: 'ngdialog-theme-default',
                        showClose: false,
                        scope: $scope
                    });
                } else if (response['status'] == responseCode.ERROR_IN_EXECUTION) {
                    $scope.displaymsg = "Something went wrong, please try again later.";
                    ngDialog.open({
                        template: 'display_msg_modalDialog',
                        className: 'ngdialog-theme-default',
                        showClose: false,
                        scope: $scope
                    });
                }

                $scope.$apply();

            });


    }

    /*--------------------------------------------------------------------------
     *----------------- funtion to submit privacy policy data ------------------
     --------------------------------------------------------------------------*/
    $scope.submitPolicy = function () {

        $.post(MY_CONSTANT.url + '/write_privacy_policy',{
                access_token: $cookieStore.get('obj').accesstoken,
                data : $scope.policy_data
            },
            function (response) {
                var response = JSON.parse(response);

                if (response['status'] == responseCode.SUCCESS) {
                    $scope.displaymsg = "Document updated successfully";
                    ngDialog.open({
                        template: 'display_msg_modalDialog',
                        className: 'ngdialog-theme-default',
                        showClose: false,
                        scope: $scope
                    });

                }
                else if (response['status'] == responseCode.SHOW_ERROR_MESSAGE) {
                    $scope.displaymsg = response['message'];
                    ngDialog.open({
                        template: 'display_msg_modalDialog',
                        className: 'ngdialog-theme-default',
                        showClose: false,
                        scope: $scope
                    });
                } else if (response['status'] == responseCode.ERROR_IN_EXECUTION) {
                    $scope.displaymsg = "Something went wrong, please try again later.";
                    ngDialog.open({
                        template: 'display_msg_modalDialog',
                        className: 'ngdialog-theme-default',
                        showClose: false,
                        scope: $scope
                    });
                }

                $scope.$apply();

            });


    }

    /*--------------------------------------------------------------------------
     *----------------- funtion to submit fare data ------------------
     --------------------------------------------------------------------------*/
    $scope.submitFare = function () {

        $.post(MY_CONSTANT.url + '/write_fare_details',{
                access_token: $cookieStore.get('obj').accesstoken,
                data : $scope.fare_data
            },
            function (response) {
                var response = JSON.parse(response);

                if (response['status'] == responseCode.SUCCESS) {
                    $scope.displaymsg = "Document updated successfully";
                    ngDialog.open({
                        template: 'display_msg_modalDialog',
                        className: 'ngdialog-theme-default',
                        showClose: false,
                        scope: $scope
                    });

                }
                else if (response['status'] == responseCode.SHOW_ERROR_MESSAGE) {
                    $scope.displaymsg = response['message'];
                    ngDialog.open({
                        template: 'display_msg_modalDialog',
                        className: 'ngdialog-theme-default',
                        showClose: false,
                        scope: $scope
                    });
                } else if (response['status'] == responseCode.ERROR_IN_EXECUTION) {
                    $scope.displaymsg = "Something went wrong, please try again later.";
                    ngDialog.open({
                        template: 'display_msg_modalDialog',
                        className: 'ngdialog-theme-default',
                        showClose: false,
                        scope: $scope
                    });
                }

                $scope.$apply();

            });


    }

    /*--------------------------------------------------------------------------
     * ---------------- funtion to display msg ---------------------------------
     --------------------------------------------------------------------------*/
    $scope.updateDocument = function () {

        $scope.hides_how_about_btn = 1;
        $scope.hide_show_faq_btn = 1;
        $scope.hide_show_terms_btn = 1;
        $scope.hide_show_policy_btn = 1;
        $scope.hide_show_fare_btn = 1;

        ngDialog.close({
            template: 'display_msg_modalDialog',
            className: 'ngdialog-theme-default',
            scope: $scope
        });
    };

});