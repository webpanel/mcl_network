App.controller('CarController', function ($scope, $http, $cookies, $cookieStore, MY_CONSTANT, $state,responseCode) {

    $scope.select_car = '';

    $.post(MY_CONSTANT.url + '/list_all_cars', {access_token: $cookieStore.get('obj').accesstoken},
        function (data) {
            var dataArray = [];
            data = JSON.parse(data);
            if (data.status== responseCode.SUCCESS) {
                var carList = data.data.car_list;
                carList.forEach(function (column) {
                    var d = {
                        car_type: "",
                        car_name: "",
                        car_id: "",
                        fare_fixed: "",
                        fare_per_km: "",
                        fare_per_min: ""
                    };
                    d.car_type = column.car_type;
                    d.car_name = column.car_name;
                    d.car_id = column.car_id;
                    d.fare_fixed = column.fare_fixed;
                    d.fare_per_km = column.fare_per_km;
                    d.fare_per_min = column.fare_per_min;
                    dataArray.push(d);
                });
                $scope.$apply(function () {
                    $scope.list = dataArray;
                    scrollTo(0, 0);
                });
            }
            else{
                $state.go('page.login');
            }
        });

    /*--------------------------------------------------------------------------
     *----------------- funtion when car type change ---------------------------
     --------------------------------------------------------------------------*/
    $scope.set = function () {

        $.post(MY_CONSTANT.url + '/list_all_cars', {access_token: $cookieStore.get('obj').accesstoken},

            function (data) {
                data = JSON.parse(data);

                if (data.status == responseCode.SUCCESS) {
                    var length = data.data.car_list.length;
                    console.log("data");
                    console.log(data);
                    console.log(length);

                    for (var i = 0; i < length; i++) {

                        if ($scope.select_car == data.data.car_list[i].car_type) {
                            console.log("here");
                            $scope.$apply(function () {
                                $scope.cars = {
                                    fare_per_min: data.data.car_list[i].fare_per_min,
                                    fare_per_km: data.data.car_list[i].fare_per_km,
                                    fare_fixed: data.data.car_list[i].fare_fixed
                                };
                            });
                        }

                    }
                }
                else {
                    $state.go('page.login');
                }

            });
    }

    $scope.car = {};

    /*--------------------------------------------------------------------------
     * ---------------- funtion for update car fare ----------------------------
     --------------------------------------------------------------------------*/
    $scope.addCarFare = function (cars) {

        $scope.successMsg = '';
        $scope.errorMsg = '';
        $scope.car.access_token = $cookieStore.get('obj').accesstoken;
        $scope.car.car_id = $scope.select_car;
        $scope.car.fare_fixed = cars.fare_fixed;
        $scope.car.fare_per_km = cars.fare_per_km;
        $scope.car.fare_per_min = cars.fare_per_min;
        $scope.car.wait_time_fare_per_min = 0;
        $scope.car.fare_threshold_time = 0;
        $scope.car.fare_threshold_distance = 0;

        console.log("$scope.car.car_id2 "+$scope.car.car_id);

        if($scope.car.car_id == '' || $scope.car.car_id == undefined) {
            $scope.errorMsg = "Please select car type";
            setTimeout(function () {
                $scope.errorMsg  = "";
                $scope.$apply();
            }, 3000);
        }
        else{
            $.post(MY_CONSTANT.url + '/update_car_fare', $scope.car
            ).then(
                function (data) {
                    data = JSON.parse(data);

                    if (data.status == responseCode.SUCCESS) {
                        $scope.successMsg = data.message;
                        $scope.promo = {};
                        $scope.$apply();
                        setTimeout(function () {
                            $scope.successMsg = "";
                            $scope.$apply();
                        }, 3000);
                    } else {
                        $scope.errorMsg = data.message;
                        $scope.$apply();
                        setTimeout(function () {
                            $scope.errorMsg = "";
                            $scope.$apply();
                        }, 3000);
                    }
                    scrollTo(0, 0);
                });
        }


    };

});
