App.controller('AddBookingController', ['$scope', '$interval', '$timeout', '$http', 'uiGmapLogger', '$cookies', '$cookieStore', 'MY_CONSTANT', '$state', 'ngDialog', 'uiGmapGoogleMapApi', 'responseCode', 'countryName'
    , function ($scope, $interval, $timeout, $http, $log, $cookies, $cookieStore, MY_CONSTANT, $state, ngDialog, GoogleMapApi, responseCode, countryName) {

        $log.currentLevel = $log.LEVELS.debug;
        $scope.newReg = {};
        $scope.newReg.successMsg = "";
        $scope.book_ride = false;
        $scope.booking = {};
        $scope.driver_data = {};
        $scope.info = {};
        $scope.ride = {};
        $scope.rideText = "Book";
        $scope.time_msg = false;
        $scope.book_ride_later_data = {};
        $scope.customer_access_token = "";
        $scope.show_driver_flag = false;
        $scope.manual_drivers = false;
        $scope.time_value = false;
        $scope.book_type = true; //automatic booking
        $scope.approx = {};
        $scope.eta_flag = 0;   //flag for showing ETA
        $scope.approx_value_show = 0;//flag for showing hr tag
        $scope.approx_price = 0; //flag for estimated price
        $scope.count = 0;
        $scope.promodivshow = 0;
        $scope.promosuccessMsg = "";
        $scope.promoerrorMsg = "";
        $scope.total_rqst_send = 0;
        $scope.rideStatusShow = 0; //showing assigned to nd status of requested Ride
        $scope.showloader = false;
        $scope.interval_for_particular_driver = {};
        $scope.cancel_interval_time_driver = {};
        $scope.info.isShowPhoneNoToDriver = false;
        $scope.editUsername = true;
        $scope.editUserFlag = 0;

        var driverMarkerArr = new Array();
        var infoWindow = new google.maps.InfoWindow();
        var bounds = new google.maps.LatLngBounds();
        //socket connection

        var socket = io.connect(MY_CONSTANT.url_booking);
        socket.on("admin:rideStatus",function(data){
            if($scope.rideId == parseInt(data.rideId)) {
                $scope.showloader = false;
                $scope.rideId = data.rideId;
                $scope.rideStatusShow = 1;
                $scope.rideStatus = data.status;
                switch (data.flag) {
                    case 0:   //WHEN DRIVER IS BEING ASSIGNED
                        var response = data.data.driverData;
                        $scope.assignedTo = response.firstName + " " + response.lastName;
                        $scope.vehicleNumber = response.carNo;
                        $scope.statusColor = '#FF9900';
                        break;
                    case 1:   //WHEN DRIVER HAS ACCEPTED
                        var response = data.data.rideData.userData;
                        $scope.vehicleNumber = response.carNo;
                        $scope.assignedTo = response.firstName + " " + response.lastName;
                        $scope.statusColor = 'BLUE';

                        break;
                    case 2:   //WHEN DRIVER HAS ARRIVED
                        $scope.rideStatusShow = 1;
                        break;
                    case 3:   //WHEN RIDE IS STARTED
                        $scope.rideStatusShow = 1;
                        $scope.statusColor = 'green';

                        break;
                    case 4:   //WHEN RIDE IS ENDED
                        $scope.rideStatusShow = 1;
                        $scope.statusColor = '#006666';
                        break;
                    case 9:   //WHEN  DRIVER REJECTED
                        $scope.rideStatusShow = 0;
                        $scope.rideText = "Book";
                        $scope.ride_function = false;
                        break;
                    case 10:   //WHEN NO DRIVER ACCEPTED OR AVAILABLE
                        $scope.rideStatusShow = 0;
                        $scope.rideText = "Book";
                        $scope.ride_function = false;
                        break;
                }
                $scope.displaymsg = data.status;
                ngDialog.open({
                    template: 'display_msg_modalDialog',
                    className: 'ngdialog-theme-default',
                    showClose: false,
                    scope: $scope
                });
                $scope.$apply();
            }
        });

        //============================================================================
        //                            getting car types
        //============================================================================

        $.post(MY_CONSTANT.url + '/api/vehicles/viewType', {accessToken: $cookieStore.get('obj').accesstoken},
            function (data) {
                var carArray = [];
                if (typeof(data) == 'string') {
                    data = JSON.parse(data);
                }
                if (data.flag == responseCode.SUCCESS) {
                    var carList = data.data.vehicles;
                    carList.forEach(function (column) {
                        var d = {};
                        d.name = column.name;
                        d.id = column.id;
                        d.type = column.type;
                        carArray.push(d);
                    });
                    $scope.$apply(function () {
                        $scope.carlist = carArray;
                    });
                }
                else if (data.flag == responseCode.INVALID_ACCESS_TOKEN) {
                    $state.go('page.login');
                }
            });

        //remove api hit when moving from one controller to another
        $scope.$on('$destroy', function () {
             socket.close();
        });

        var d = new Date((new Date()).getTime());
        var offset = ((d.getTimezoneOffset()) * 60 * (-1));
        /*--------------------------------------------------------------------------
         * --------- showing drivers marker ----------------------------------
         --------------------------------------------------------------------------*/
        var createDriverMarker = function (info) {
            var icon = 'app/img/freeDriver.png';

            var marker1 = new google.maps.Marker({
                position: new google.maps.LatLng(info.latitude, info.longitude),
                map: $scope.mapContainer,
                icon: {
                    url: 'app/img/freeDriver.png',
                    scaledSize: new google.maps.Size(30, 30)
                }
            });

            marker1.content = '<div class="infoWindowContent">' +
                '<center>Driver Info</center>' +
                '<span> Name - ' + info.user_name + '</span><br>' +
                '<span> Phone - ' + info.phone_no + '</span><br>' +
                    //'<span> Car Type - ' + info.car_name + '</span><br>' +
                    //'<span> Vehicle Number - ' + info.driver_car_no + '</span>' +
                '</div>';

            google.maps.event.addListener(marker1, 'click', function () {
                infoWindow.setContent(marker1.content);
                infoWindow.open($scope.mapContainer, marker1);
            });

            driverMarkerArr.push(marker1);

        }

//==========================================================================================================================
//========================================================== calculating distance ===========================================
//==========================================================================================================================
        $scope.getDistance = function () {


            // show route between the points
            directionsService = new google.maps.DirectionsService();
            directionsDisplay = new google.maps.DirectionsRenderer(
                {
                    suppressMarkers: true,
                    suppressInfoWindows: true
                });
            // directionsDisplay.setMap(map);
            var request = {
                origin: $scope.location1,
                destination: $scope.location2,
                travelMode: google.maps.DirectionsTravelMode.DRIVING
            };
            directionsService.route(request, function (response, status) {
                if (status == google.maps.DirectionsStatus.OK) {
                    var string;

                    directionsDisplay.setDirections(response);

                    var driving_time = parseFloat(response.routes[0].legs[0].duration.value / 60);
                    var distance = parseFloat(response.routes[0].legs[0].distance.value / 1000);
                    string = "The distance between the two points on the chosen route is: " + response.routes[0].legs[0].distance.text;
                    string += "<br/>The aproximative driving time is: " + response.routes[0].legs[0].duration.text;
                    $scope.fare_factor = parseFloat($scope.fare_factor);
                    $scope.approx.fare_fixed = parseFloat($scope.approx.fare_fixed);
                    $scope.approx.fare_per_min = parseFloat($scope.approx.fare_per_min);
                    $scope.estimated_price = ($scope.fare_factor * ($scope.approx.fare_fixed + (driving_time * $scope.approx.fare_per_min) + (distance * $scope.approx.fare_per_km))).toFixed(2);
                    $scope.approx_price = 1;


                }
                $scope.$apply();
            });
        }
//==========================================================================================================================
//===============================================end distance calculation ================================================
//==========================================================================================================================


        //get car type details for expected fare calculation
        $scope.getfare = function (cartype_val) {

            //$.post(MY_CONSTANT.url + '/list_all_cars', {access_token: $cookieStore.get('obj').accesstoken},
            //    function (data) {
            //        data = JSON.parse(data);
            //        var length = data.data.car_list.length;
            //        if (data.status == responseCode.SUCCESS) {
            //            var carList = data.data.car_list;
            //            for (i = 0; i < length; i++) {
            //                if (i == cartype_val) {
            //                    $scope.$apply(function () {
            //                        $scope.approx = {
            //                            fare_per_min: data.data.car_list[i].fare_per_min,
            //                            fare_per_km: data.data.car_list[i].fare_per_km,
            //                            fare_fixed: data.data.car_list[i].fare_fixed
            //                        };
            //                    });
            //
            //                }
            //            }
            //            $scope.getDistance();
            //        }
            //    });

        };


        /*--------------------------------------------------------------------------
         * --------- funtion to enter only numbers in number field -----------------
         ------------------------------- ------------------------------------------*/
        //$('#main-content').on('keypress', '#search_phone_no', function (e) {
        //    var curval = $(this).val().length;
        //
        //    if ((e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57))) {
        //        return false;
        //    }
        //    else if (e.which == 8 || e.which == 0) {
        //        return true;
        //    }
        //    else if (curval == 14) {
        //        return false;
        //    }
        //});


        var start = new Date();
        start.setHours(start.getHours() + 1);
        start = new Date(start);

        start.setMinutes(start.getMinutes() + 4);
        start = new Date(start);

        //e.setDate(start.getDate() + 3);

        $("#pick_up_time").datetimepicker({
            format: 'yyyy/mm/dd hh:ii',

            autoclose: true,
            startDate: start
            //endDate: e
        });


        $scope.$watch('book_type', function (newValue, oldValue) {
            start.setHours(start.getHours() + 1);
            start = new Date(start);
            if (newValue == false) {
                $scope.time_value = false;

            }
            else {
                $scope.manual_drivers = false;
            }
        });

        $scope.$watch('time_value', function (newValue, oldValue) {
            start.setHours(start.getHours() + 1);
            start = new Date(start);
            $("#pick_up_time").val('');
        });

        /*--------------------------------------------------------------------------
         * --------- funtion to disable button itself ------------------------------
         ------------------------------- ------------------------------------------*/
        $('#driver_table').on('click', '.assignDriver', function (e) {
            $scope.ongoing_ride_id = e.currentTarget.id;
            $(this).attr('disabled', 'disabled');
            $(this).text('Sent');
        });

        /*--------------------------------------------------------------------------
         * --------- funtion to get latitude and longitude of pick up location -----
         ------------------------------- ------------------------------------------*/
        $scope.pickUpMarker = function (book) {
            var address = $scope.info.chosenPlace;
            (new google.maps.Geocoder()).geocode({
                'address': address
            }, function (results, status) {
                if (status == google.maps.GeocoderStatus.OK) {

                    $scope.booking.latitude = results[0].geometry.location.lat();
                    $scope.booking.longitude = results[0].geometry.location.lng();

                } else {
                }
            });
        };

        /*--------------------------------------------------------------------------
         * --------- funtion to get latitude and longitude of pick up location -----
         -------------------- and place on map -----------------------------------*/

        $scope.map = {
            zoom: 14,
            center: new google.maps.LatLng(57.1910499, -2.0834466),
            pan: true
        };
        var markerArr = new Array();
        var markerArr1 = new Array();
        $scope.mapContainer = new google.maps.Map(document.getElementById('map-container'), $scope.map);

        //event for adding marker on click oof body of map
        google.maps.event.addListener($scope.mapContainer, 'click', function (event) {
            $scope.booking.latitude = event.latLng.lat();
            $scope.booking.longitude = event.latLng.lng();
            $scope.reverseGeocode(event.latLng, 0);
            $scope.placeMarker(event.latLng.lat(), event.latLng.lng(), 0);


        });


//*===========================================================================================================================*
//*=============================================REVERSE GEOCODING TO GET ADDRESS==============================================
//*===========================================================================================================================*
        $scope.reverseGeocode = function (latlong, val) {
            (new google.maps.Geocoder()).geocode({'latLng': latlong}, function (results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    if (results[0]) {
                        if (val == 0) {
                            $('#address').val(results[0].formatted_address);
                            $scope.info.chosenPlace = results[0].formatted_address;
                        }
                        else {
                            $('#drop_off_address').val(results[0].formatted_address);
                            $scope.info.dropOffPlace = results[0].formatted_address;
                        }
                        //$('#latitude').val(marker.getPosition().lat());
                        //$('#longitude').val(marker.getPosition().lng());

                    }
                }
            });

        }
//*===========================================================================================================================*
//*=============================================PLACE MARKER ON GIVEN LATLONG==================================================
//*===========================================================================================================================*
        $scope.placeMarker = function (lat, long, flag) {

            if (flag == 0)
                var icon = 'app/img/redMarker.png';
            else
                var icon = 'app/img/greenMarker.png';
            var marker = new google.maps.Marker({
                map: $scope.mapContainer,
                icon: icon,
                position: new google.maps.LatLng(lat, long),
                draggable: true
            });
            if (markerArr.length) {
                for (var i = 0; i < markerArr.length; i++)
                    markerArr[i].setMap(null);
                markerArr.pop();
            }
            markerArr.push(marker);
            if ($scope.poly) {
                poly = $scope.poly
                poly.setMap(null);   //destrying the already created path;
            }

            if (lat != '' && (!(angular.isUndefined($scope.book_ride_later_data.manual_destination_latitude)))) {
                $scope.drawPAth(lat, long, $scope.book_ride_later_data.manual_destination_latitude, $scope.book_ride_later_data.manual_destination_longitude);

            }
            //else{
            //    var srclatlong = new google.maps.LatLng(lat, long);
            //    bounds.extend(srclatlong);
            //    console.log("elsepart");
            //    $scope.mapContainer.fitBounds(bounds);
            //}


            google.maps.event.addListener(marker, 'drag', function () {
                $scope.reverseGeocode(marker.getPosition(), 0);
                $scope.booking.latitude = marker.getPosition().lat();
                $scope.booking.longitude = marker.getPosition().lng();
                if ($scope.poly) {
                    poly = $scope.poly
                    poly.setMap(null);   //destrying the already created path;
                }

            });

            google.maps.event.addListener(marker, 'dragend', function () {
                $scope.reverseGeocode(marker.getPosition(), 0);
                $scope.booking.latitude = marker.getPosition().lat();
                $scope.booking.longitude = marker.getPosition().lng();
                if ($scope.booking.latitude != '' && (!(angular.isUndefined($scope.book_ride_later_data.manual_destination_latitude)))) {
                    $scope.drawPAth($scope.booking.latitude, $scope.booking.longitude, $scope.book_ride_later_data.manual_destination_latitude, $scope.book_ride_later_data.manual_destination_longitude);
                }
                else {
                    var srclatlong = new google.maps.LatLng(lat, long);
                    bounds.extend(srclatlong);
                    $scope.mapContainer.fitBounds(bounds);
                }

            });

        }


        $scope.pickUpLocationOnMarker = function (book) {
            var address = $scope.info.chosenPlace;

            (new google.maps.Geocoder()).geocode({
                'address': address
            }, function (results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    $scope.location1 = results[0].geometry.location;
                    $scope.booking.latitude = results[0].geometry.location.lat();
                    $scope.booking.longitude = results[0].geometry.location.lng();
                    $scope.map = {
                        zoom: 14,
                        center: new google.maps.LatLng(results[0].geometry.location.lat(), results[0].geometry.location.lng()),
                        pan: true
                    }

                    var panPoint = new google.maps.LatLng(results[0].geometry.location.lat(), results[0].geometry.location.lng());
                    $scope.mapContainer.panTo(panPoint);

                    var icon = 'app/img/redMarker.png';

                    if (markerArr.length) {
                        for (i = 0; i < markerArr.length; i++)
                            markerArr[i].setMap(null);
                        markerArr.pop();
                    }

                    var marker = new google.maps.Marker({
                        map: $scope.mapContainer,
                        icon: icon,
                        position: new google.maps.LatLng(results[0].geometry.location.lat(), results[0].geometry.location.lng()),
                        draggable: true
                    });

                    google.maps.event.addListener(marker, 'drag', function () {
                        $scope.reverseGeocode(marker.getPosition(), 0);
                        $scope.booking.latitude = marker.getPosition().lat();
                        $scope.booking.longitude = marker.getPosition().lng();
                        if ($scope.poly) {
                            poly = $scope.poly
                            poly.setMap(null);   //destrying the already created path;
                        }

                    });

                    google.maps.event.addListener(marker, 'dragend', function () {
                        $scope.reverseGeocode(marker.getPosition(), 0);
                        $scope.booking.latitude = marker.getPosition().lat();
                        $scope.booking.longitude = marker.getPosition().lng();
                        if ($scope.booking.latitude != '' && (!(angular.isUndefined($scope.book_ride_later_data.manual_destination_latitude)))) {
                            $scope.drawPAth($scope.booking.latitude, $scope.booking.longitude, $scope.book_ride_later_data.manual_destination_latitude, $scope.book_ride_later_data.manual_destination_longitude);
                        }
                        else {   //incase of not going to drwa path ...so setting latlong bounds here
                            var srclatlong = new google.maps.LatLng($scope.booking.latitude, $scope.booking.longitude);
                            bounds.extend(srclatlong);
                            $scope.mapContainer.fitBounds(bounds);
                        }

                    });

                    if ($scope.booking.latitude != '' && (!(angular.isUndefined($scope.book_ride_later_data.manual_destination_latitude)))) {
                        $scope.drawPAth($scope.booking.latitude, $scope.booking.longitude, $scope.book_ride_later_data.manual_destination_latitude, $scope.book_ride_later_data.manual_destination_longitude);
                    }
                    else {   //incase of not going to drwa path ...so setting latlong bounds here
                        var srclatlong = new google.maps.LatLng($scope.booking.latitude, $scope.booking.longitude);
                        bounds.extend(srclatlong);
                        $scope.mapContainer.fitBounds(bounds);
                    }

                    markerArr.push(marker);

                } else {
                    $scope.displaymsg = "Pick up location is not valid";

                    ngDialog.open({
                        template: 'display_msg_modalDialog',
                        className: 'ngdialog-theme-default',
                        showClose: false,
                        scope: $scope
                    });
                }
            });
        };

        /*--------------------------------------------------------------------------
         * -------- funtion to get latitude and longitude of drop-off location -----
         -------------------- and place on map -----------------------------------*/


        $scope.dropOffLocationOnMarker = function (book, flag) {


            var address = book.dropOffPlace;
            (new google.maps.Geocoder()).geocode({
                'address': address
            }, function (results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    $scope.location2 = results[0].geometry.location;

                    $scope.book_ride_later_data.manual_destination_latitude = results[0].geometry.location.lat();
                    $scope.book_ride_later_data.manual_destination_longitude = results[0].geometry.location.lng();

                    var icon = 'app/img/greenMarker.png';
                    if (markerArr1.length) {
                        for (i = 0; i < markerArr1.length; i++)
                            markerArr1[i].setMap(null);
                        markerArr1.pop();
                    }

                    var marker = new google.maps.Marker({
                        map: $scope.mapContainer,
                        icon: icon,
                        position: new google.maps.LatLng(results[0].geometry.location.lat(), results[0].geometry.location.lng()),
                        draggable: true
                    });
                    google.maps.event.addListener(marker, 'drag', function () {
                        $scope.reverseGeocode(marker.getPosition(), 1);
                        $scope.book_ride_later_data.manual_destination_latitude = marker.getPosition().lat();
                        $scope.book_ride_later_data.manual_destination_longitude = marker.getPosition().lng();
                        if ($scope.poly) {
                            poly = $scope.poly
                            poly.setMap(null);   //destrying the already created path;
                        }

                    });

                    google.maps.event.addListener(marker, 'dragend', function () {
                        $scope.reverseGeocode(marker.getPosition(), 1);
                        $scope.book_ride_later_data.manual_destination_latitude = marker.getPosition().lat();
                        $scope.book_ride_later_data.manual_destination_longitude = marker.getPosition().lng();
                        if ((!(angular.isUndefined($scope.booking.latitude))) && ($scope.book_ride_later_data.manual_destination_latitude != '' )) {

                            $scope.drawPAth($scope.booking.latitude, $scope.booking.longitude, $scope.book_ride_later_data.manual_destination_latitude, $scope.book_ride_later_data.manual_destination_longitude);
                        }
                        else {
                            $scope.mapContainer.fitBounds(bounds);
                        }
                    });
                    markerArr1.push(marker);

                    if ((!(angular.isUndefined($scope.booking.latitude))) && ($scope.book_ride_later_data.manual_destination_latitude != '' )) {
                        $scope.drawPAth($scope.booking.latitude, $scope.booking.longitude, $scope.book_ride_later_data.manual_destination_latitude, $scope.book_ride_later_data.manual_destination_longitude);
                        if (flag != 0) {
                            $scope.getfare($scope.info.car_type);
                        }

                    }
                    else {
                        $scope.mapContainer.fitBounds(bounds);
                    }

                    // $scope.getDistance();


                } else {

                    $scope.displaymsg = "Drop-off location is not valid";

                    ngDialog.open({
                        template: 'display_msg_modalDialog',
                        className: 'ngdialog-theme-default',
                        showClose: false,
                        scope: $scope
                    });
                }
            });
        };


        /*--------------------------------------------------------------------------
         * --------- funtion to draw path between pick-up location and drop-off ----
         ------------------------------- location ----------------------------------*/
        $scope.drawPAth = function (lat1, lng1, lat2, lng2) {
            if ($scope.poly) {
                poly = $scope.poly
                poly.setMap(null);   //destrying the already created path;
            }


            var lat_lng = new Array();
            var myLatlng = new google.maps.LatLng(lat1, lng1);
            lat_lng.push(myLatlng);
            var myLatlng1 = new google.maps.LatLng(lat2, lng2);
            lat_lng.push(myLatlng1);

            var path = new google.maps.MVCArray();

            //Initialize the Direction Service
            var service = new google.maps.DirectionsService();

            //Set the Path Stroke Color
            var poly = new google.maps.Polyline({map: $scope.mapContainer, strokeColor: '#4986E7'});


            //Loop and Draw Path Route between the Points on MAP
            for (var i = 0; i < lat_lng.length; i++) {
                if ((i + 1) < lat_lng.length) {
                    var src = lat_lng[i];
                    var des = lat_lng[i + 1];
                    path.push(src);
                    poly.setPath(path);
                    service.route({
                        origin: src,
                        destination: des,
                        travelMode: google.maps.DirectionsTravelMode.DRIVING
                    }, function (result, status) {
                        if (status == google.maps.DirectionsStatus.OK) {
                            for (var i = 0, len = result.routes[0].overview_path.length; i < len; i++) {
                                path.push(result.routes[0].overview_path[i]);
                            }


                        }
                    });
                }
            }

            $scope.poly = poly;

            // lat long bounds within given locations
            bounds.extend(myLatlng);
            bounds.extend(myLatlng1);
            $scope.mapContainer.fitBounds(bounds);

        };

        /*--------------------------------------------------------------------------
         * --------- funtion to reset form data ------------------------------------
         ---------------------------------------------------------------------------*/

        $scope.clearData = function () {
            $state.reload();
            $interval.cancel($scope.interval_for_particular_driver);
            $interval.cancel($scope.cancel_interval_time_driver);
            clearInterval($scope.interval_time);
            clearInterval($scope.cancel_interval_time);

        };


        /*--------------------------------------------------------------------------
         * --------- funtion to check whether customer completed his previous  -----
         ------------------------------ ride or not --------------------------------*/

        $scope.checkBookingStatus = function (book) {
            console.log(book);
            $scope.successMsg = '';
            $scope.errorMsg = '';
            $scope.booking.access_token = book.access_token;
            $scope.booking.car_type = book.car_type;
            $scope.booking.device_type = 3;
            var address = book.chosenPlace;
            if (book == undefined) {
                $scope.errorMsg = "All Fields are required.";
                $scope.TimeOutError($scope.errorMsg);
                return false;
            }
            if (book.user_name == '' || book.user_name == undefined || book.user_email == '' || book.user_email == undefined) {
                $scope.errorMsg = "Username and Email id is required.";
                $scope.TimeOutError($scope.errorMsg);
                return false;
            }

            if ($scope.asyncSelected == undefined || $scope.asyncSelected == "") {
                $scope.errorMsg = "Enter Phone Number To Search.";
                $scope.TimeOutError($scope.errorMsg);
                return false;
            }
            if (book.user_name == '' || book.user_name == undefined || book.user_email == '' || book.user_email == undefined) {
                $scope.errorMsg = "Username and Email id is required.";
                $scope.TimeOutError($scope.errorMsg);
                return false;
            }
            if (book.car_type == undefined || book.car_type === '') {
                $scope.errorMsg = "Please select car type..";
                $scope.TimeOutError($scope.errorMsg);
                return false;
            }
            else {
                $scope.successMsg = '';
                $scope.errorMsg = '';
                $scope.booking.access_token = book.access_token;
                $scope.booking.car_type = book.car_type;
                $scope.booking.device_type = 3;
                var address = book.chosenPlace;

                if ($scope.book_type == undefined) {
                    $scope.errorMsg = "Please select booking type..";
                    $scope.TimeOutError($scope.errorMsg);
                    return false;

                }
                if ($scope.info.chosenPlace == '' || $scope.info.chosenPlace == undefined) {
                    $scope.errorMsg = "Enter Pick Up Location.";
                    $scope.TimeOutError($scope.errorMsg);
                    return false;
                }
                else if ($scope.book_type == false) {//manual booking

                    //lat long of drop_off location
                    if (book.dropOffPlace) {
                        (new google.maps.Geocoder()).geocode({
                            'address': book.dropOffPlace
                        }, function (results, status) {
                            if (status == google.maps.GeocoderStatus.OK) {
                                $scope.ride.destinationLocationAddress = book.dropOffPlace;
                                $scope.ride.destinationLatitude = results[0].geometry.location.lat();
                                $scope.ride.destinationLongitude = results[0].geometry.location.lng();

                            }
                        })
                    }


                    (new google.maps.Geocoder()).geocode({
                        'address': address
                    }, function (results, status) {
                        if (status == google.maps.GeocoderStatus.OK) {

                            $scope.booking.admin_panel_request_flag = 1;

                            $.post(MY_CONSTANT.url_booking + '/find_a_driver', $scope.booking
                            ).then(
                                function (data) {
                                    data = JSON.parse(data);
                                    var dataArray = [];
                                    var driverList = data.data;
                                    var length = data.data.length;

                                    if (length) {
                                        driverMarkerArr = [];
                                        driverList.forEach(function (column) {
                                            createDriverMarker(column)

                                            $scope.openInfoWindow = function (e, selectedMarker) {
                                                e.preventDefault();
                                                google.maps.event.trigger(selectedMarker, 'click');
                                            }


                                        });
                                        for (var i = 0; i < driverMarkerArr.length; i++) {
                                            bounds.extend(driverMarkerArr[i].getPosition());
                                        }

                                        $scope.mcOptions = {gridSize: 50, maxZoom: 20, zoomOnClick: false};

                                        if ($scope.markerClusterer) {
                                            $scope.markerClusterer.clearMarkers();   //clearing the markercluster to add new
                                        }

                                        $scope.markerClusterer = new MarkerClusterer($scope.mapContainer, driverMarkerArr, $scope.mcOptions);

                                        $scope.getfare(book.car_type);

                                        $scope.manual_drivers = true;
                                        $scope.eta_flag = 1;   //flag for showing ETA
                                        $scope.approx_value_show = 1;
                                        $scope.approx_price = 0;
                                        $scope.eta = parseFloat((data.nearest_time / 60).toFixed(2));
                                        $scope.fare_factor = data.fare_factor;
                                        if ($scope.info.chosenPlace != "" || $scope.info.chosenPlace != undefined) {

                                            $scope.pickUpLocationOnMarker($scope.info); //getting values of lat long from function

                                        }

                                        if (!(angular.isUndefined($scope.info.dropOffPlace)) && $scope.info.dropOffPlace != "") {

                                            $scope.dropOffLocationOnMarker($scope.info, 1); //getting values of lat long from function

                                        }


                                        driverList.forEach(function (column) {
                                            var d = {};

                                            d.user_id = column.user_id;
                                            d.user_name = column.user_name;
                                            d.distance = parseFloat((column.distance * 0.000621371).toFixed(4));
                                            d.phone_no = column.phone_no;

                                            dataArray.push(d);
                                        });

                                        $scope.$apply(function () {
                                            $scope.list = dataArray;

                                            // Define global instance we'll use to destroy later
                                            var dtInstance;

                                            $timeout(function () {
                                                if (!$.fn.dataTable)
                                                    return;
                                                dtInstance = $('#datatable2').dataTable({
                                                    'paging': true, // Table pagination
                                                    'ordering': true, // Column ordering
                                                    'info': true, // Bottom left status text
                                                    "bDestroy": true,
                                                    oLanguage: {
                                                        sSearch: 'Search all columns:',
                                                        sLengthMenu: '_MENU_ records per page',
                                                        info: 'Showing page _PAGE_ of _PAGES_',
                                                        zeroRecords: 'Nothing found - sorry',
                                                        infoEmpty: 'No records available',
                                                        infoFiltered: '(filtered from _MAX_ total records)'
                                                    }
                                                });
                                                var inputSearchClass = 'datatable_input_col_search';
                                                var columnInputs = $('tfoot .' + inputSearchClass);

                                                // On input keyup trigger filtering
                                                columnInputs
                                                    .keyup(function () {
                                                        dtInstance.fnFilter(this.value, columnInputs.index(this));
                                                    });
                                            });
                                            $scope.$on('$destroy', function () {
                                                dtInstance.fnDestroy();
                                                $('[class*=ColVis]').remove();
                                            });
                                        });


                                    }
                                    else {
                                        $scope.displaymsg = "Sorry, All our drivers are currently busy. We are unable to offer you services right now. Please try again sometime later.";

                                        ngDialog.open({
                                            template: 'display_msg_modalDialog',
                                            className: 'ngdialog-theme-default',
                                            showClose: false,
                                            scope: $scope
                                        });
                                    }


                                });
                        }
                        else {
                            $scope.displaymsg = "Pick up location is not valid";

                            ngDialog.open({
                                template: 'display_msg_modalDialog',
                                className: 'ngdialog-theme-default',
                                showClose: false,
                                scope: $scope
                            });
                        }
                    });
                }
                else if ($scope.book_type == true) {//automatic booking

                    if (!$scope.time_value) {//present booking

                        $scope.getfare(book.car_type);

                        //get lat and long from address
                        //google map api hit
                        (new google.maps.Geocoder()).geocode({
                            'address': address
                        }, function (results, status) {
                            if (status == google.maps.GeocoderStatus.OK) {
                                if ($scope.info.chosenPlace != "" || $scope.info.chosenPlace != undefined) {

                                    $scope.pickUpLocationOnMarker($scope.info); //getting values of lat long from function

                                }

                                if (!(angular.isUndefined($scope.info.dropOffPlace)) && $scope.info.dropOffPlace != "") {
                                    //lat long of drop_off location
                                    if (book.dropOffPlace) {
                                        (new google.maps.Geocoder()).geocode({
                                            'address': book.dropOffPlace
                                        }, function (results, status) {
                                            if (status == google.maps.GeocoderStatus.OK) {
                                                $scope.ride.destinationLocationAddress = book.dropOffPlace;
                                                $scope.ride.destinationLatitude = results[0].geometry.location.lat();
                                                $scope.ride.destinationLongitude = results[0].geometry.location.lng();
                                            }
                                        })
                                    }

                                    $scope.dropOffLocationOnMarker($scope.info, 1); //getting values of lat long from function

                                }
                                $scope.bookRide(book);
                                //
                                //$.post(MY_CONSTANT.url_booking + '/start_app_using_access_token', $scope.booking
                                //).then(
                                //    function (data) {
                                //        data = JSON.parse(data);
                                //        console.log(data);
                                //
                                //        if ((data.status.flag == 130 || data.status.flag == 131) && (data.last_ride == null || !data.last_ride.engagement_id)) {
                                //            //$scope.book_ride = true;
                                //            $scope.rideText = "Book";
                                //            $scope.ride_function = false;
                                //            //setting drivers markers
                                //            var driverList = data.drivers.data;
                                //
                                //            driverMarkerArr = [];
                                //            driverList.forEach(function (column) {
                                //                createDriverMarker(column)
                                //
                                //                $scope.openInfoWindow = function (e, selectedMarker) {
                                //                    e.preventDefault();
                                //                    google.maps.event.trigger(selectedMarker, 'click');
                                //                }
                                //
                                //
                                //
                                //            });
                                //            $scope.mcOptions = {gridSize: 50, maxZoom: 20,zoomOnClick: false};
                                //
                                //            if ($scope.markerClusterer) {
                                //                $scope.markerClusterer.clearMarkers();   //clearing the markercluster to add new
                                //            }
                                //
                                //            $scope.markerClusterer = new MarkerClusterer( $scope.mapContainer, driverMarkerArr,$scope.mcOptions);
                                //            //end drivers markers
                                //            $scope.eta = parseFloat((data.drivers.nearest_time / 60).toFixed(2));
                                //            $scope.fare_factor = data.drivers.fare_factor;
                                //            $scope.eta_flag = 1;   //flag for showing ETA
                                //            $scope.approx_value_show = 1;
                                //            $scope.approx_price = 0;
                                //

                                //            $scope.bookRide(book);
                                //
                                //
                                //
                                //        }
                                //        else {
                                //            $scope.displaymsg = "Complete your previous ride first";
                                //            ngDialog.open({
                                //                template: 'display_msg_modalDialog',
                                //                className: 'ngdialog-theme-default',
                                //                showClose: false,
                                //                scope: $scope
                                //            });
                                //        }
                                //        $scope.$apply();
                                //
                                //    });
                            }
                            else {
                                $scope.displaymsg = "Pick up location is not valid";

                                ngDialog.open({
                                    template: 'display_msg_modalDialog',
                                    className: 'ngdialog-theme-default',
                                    showClose: false,
                                    scope: $scope
                                });
                            }
                        });
                        //end of google map api hit

                    }
                    //else {//for later booking
                    //
                    //    $scope.bookRideLater(book);
                    //}
                }
            }
        };

        /*--------------------------------------------------------------------------
         * --------- funtion to get contact no -------------------------------------
         --------------------------------------------------------------------------*/
        $scope.getContactNumber = function (val) {
            return $.post(MY_CONSTANT.url + '/api/customers/searchCustomer', {
                accessToken: $cookieStore.get('obj').accesstoken,
                id: val
            }).then(function (res) {
                if (typeof(res) == "string") {
                    res = JSON.parse(res);
                }
                var addresses = [];
                angular.forEach(res.data.customer, function (item) {
                    //addresses.push(item.phone_no);
                    var image = item.userImage;
                    addresses.push(item.firstName + " " + item.lastName + "(" + item.phoneNo + "," + item.email + ")" + '<img src=' + image + ' style="width:30px;float:left;margin-right: 2px;"/><div style="clear:both"></div>');
                });
                return addresses;
            });
        };

        /*--------------------------------------------------------------------------
         * --------- funtion to cal setData Fn on enter key ------------------------
         --------------------------------------------------------------------------*/
        $scope.checkEnterKey = function (event, phone_no) {
            $scope.info.car_type = "";
            $scope.info.user_name = "";
            $scope.info.user_email = "";
            if (event.charCode == 13)
                $scope.setData(phone_no);
        };

        /*--------------------------------------------------------------------------
         * --------- funtion on select 'contact no' to get details------------------
         --------------------------------------------------------------------------*/
        $scope.setData = function (val) {
            var newVal = "";

            if (val.split("(").length > 1) {
                newVal = val.split("(")[1].split(")")[0].split(",")[1];
            } else {
                newVal = val.split("(")[0];
            }

            $("#pick_up_time").val("");
            $scope.info.car_type = "";

            $scope.show_form = false;
            $scope.show_driver_flag = false;
            $scope.book_ride = false;
            $scope.show_map = false;

            if (val == "" || val == undefined) {
                $scope.displaymsg = "Please enter customer details.";
                ngDialog.open({
                    template: 'display_msg_modalDialog',
                    className: 'ngdialog-theme-default',
                    showClose: false,
                    scope: $scope
                });
            }
            else {
                return $.post(MY_CONSTANT.url + '/api/customers/searchCustomer', {
                    accessToken: $cookieStore.get('obj').accesstoken,
                    id: newVal
                }, function (response) {
                    if (typeof(response) == "string") {
                        response = JSON.parse(response);
                    }
                    if (response.flag == responseCode.SUCCESS) {

                        var length = response.data.customer.length;
                        if (length) {
                            var addresses = [];
                            angular.forEach(response.data.customer, function (item) {
                                addresses.push(item);
                            });


                            $scope.show_form = true;

                            $scope.$apply(function () {
                                $scope.asyncSelected = addresses[0].phoneNo,
                                    $scope.info.user_name = addresses[0].firstName + addresses[0].lastName,
                                    $scope.info.user_email = addresses[0].email,
                                    $scope.info.user_id = addresses[0].userId,
                                    $scope.info.access_token = addresses[0].accessToken
                                $scope.user_id = addresses[0].userId;
                                $scope.customer_access_token = addresses[0].accessToken;
                                $scope.editUsername = false;

                            });
                        }
                        else {

                            $scope.displaymsg = "This customer is not registered with us.";
                            $scope.newReg.phoneNo = val;
                            $scope.newReg.countryCode = 1;


                            ngDialog.open({
                                template: 'display_new_reg_msg_modalDialog',
                                className: 'ngdialog-theme-default',
                                showClose: false,
                                scope: $scope
                            });
                            $scope.$apply();
                        }
                    }
                    else if (response.flag == responseCode.INVALID_ACCESS_TOKEN) {
                        $state.go('page.login');
                    }
                    else if (response.flag == 1) {  //in case of blocked user
                        $scope.displaymsg = response['error'];

                        ngDialog.open({
                            template: 'display_msg_modalDialog',
                            className: 'ngdialog-theme-default',
                            showClose: false,
                            scope: $scope
                        });
                    }
                    else if (response.flag == responseCode.SHOW_ERROR_MESSAGE) {
                        $scope.displaymsg = response['message'];

                        ngDialog.open({
                            template: 'display_msg_modalDialog',
                            className: 'ngdialog-theme-default',
                            showClose: false,
                            scope: $scope
                        });
                    } else if (response.flag == responseCode.ERROR_IN_EXECUTION) {
                        $scope.displaymsg = "Something went wrong, please try again later.";

                        ngDialog.open({
                            template: 'display_msg_modalDialog',
                            className: 'ngdialog-theme-default',
                            showClose: false,
                            scope: $scope
                        });
                    }
                }).error(function () {
                    console.log('Internal Server Error');
                })
            }

        };

        /*--------------------------------------------------------------------------
         * --------- funtion to book ride ------------------------------------------
         --------------------------------------------------------------------------*/
        $scope.bookRide = function (val) {
            $scope.bookRideTimerFunction(val, 0);
        };

        /*--------------------------------------------------------------------------
         * --------- funtion to book ride (20 sec) ---------------------------------
         --------------------------------------------------------------------------*/
        $scope.bookRideTimerFunction = function (val, flag) {

            $scope.successMsg = '';
            $scope.errorMsg = '';
            $scope.ride.accessToken = val.access_token;
            $scope.ride.latitude = '';
            $scope.ride.longitude = '';
            $scope.ride.carType = val.car_type;
            $scope.ride.duplicate_flag = flag;
            $scope.ride.admin_panel_request_flag = 1;
            if(val.isShowPhoneNoToDriver == true){
                $scope.ride.isShowPhoneNoToDriver = 1;
            }
            else{
                $scope.ride.isShowPhoneNoToDriver = 0;
            }
            if(val.details)
            $scope.ride.comments=val.details
            else{
                $scope.ride.comments=""  ;
            }
            //var address = val.chosenPlace;
            $scope.ride.pickupLocationAddress = val.chosenPlace;
            $scope.ride.pickupLatitude = $scope.booking.latitude;
            $scope.ride.pickupLongitude = $scope.booking.longitude;
            $scope.ride.adminPanelFlag = 1;
            $scope.ride.timezone_offset = offset;


            $.post(MY_CONSTANT.url_booking + '/api/rides/requestRide', $scope.ride
                ,function (data) {
                    if(typeof(data)=="string"){
                        data = JSON.parse(data);
                    }
                    console.log(data);
                    $scope.rideText = "Processing";
                    $scope.ride_function = true;
                    $scope.$apply();
                    if(data.flag==responseCode.SUCCESS){
                        //$scope.showloader = true;
                        //$scope.displaymsg = data.message;
                        $scope.rideId = data.data.rideId;
                        //$scope.rideStatus = "Pending";
                        //$scope.assignedTo = "Requesting";
                        //$scope.rideStatusShow = 1;
                        //ngDialog.open({
                        //    template: 'display_msg_modalDialog',
                        //    className: 'ngdialog-theme-default',
                        //    showClose: false,
                        //    scope: $scope
                        //});

                    }

                }).error(function(data){
                    if(typeof(data)=="string"){
                        data = JSON.parse(data)
                    }
                    var response = JSON.parse(data.responseText);
                    $scope.displaymsg = response.error;

                    ngDialog.open({
                        template: 'display_msg_modalDialog',
                        className: 'ngdialog-theme-default',
                        showClose: false,
                        scope: $scope
                    });
                });
        };

        /*--------------------------------------------------------------------------
         * --------- funtion for new registraion  ----------------------------------
         --------------------------------------------------------------------------*/
        $scope.NewRegistration = function (newReg) {
            //if ($scope.newReg.email == '' || $scope.newReg.email == undefined) {
            //
            //    $scope.newReg.email = $scope.newReg.phone_no + "@domain.com";
            //}
            if(!(newReg.lastName)){
                newReg.lastName = " ";
            }
            if(!(newReg.email)){
                newReg.email = newReg.phoneNo+"@xyz.com";
            }
            $.post(MY_CONSTANT.url + '/api/customers/addCustomer',{
                    accessToken:$cookieStore.get('obj').accesstoken,
                    firstName:newReg.firstName,
                    lastName:newReg.lastName,
                    countryCode:"+"+newReg.countryCode,
                    phoneNo:newReg.phoneNo,
                    email:newReg.email
                },
                function (response) {
                    if(typeof(response)=="string"){
                        response = JSON.parse(response);
                    }

                    if (response.flag == responseCode.SUCCESS) {
                        if($scope.editUserFlag == 1){
                            console.log("newuserwth multiple names");
                            $scope.info.access_token = response.data.accessToken;
                            $scope.info.user_name = $scope.newReg.firstName + " " +  $scope.newReg.lastName;
                            $scope.info.user_email = $scope.newReg.email;
                            $scope.editUserFlag = 0;
                        }
                        else{
                            $scope.setData($scope.newReg.phoneNo);
                        }

                        $scope.asyncSelected = $scope.newReg.phoneNo;
                        $scope.$apply();

                        ngDialog.close({
                            template: 'reg_modal',
                            className: 'ngdialog-theme-default',
                            scope: $scope
                        });
                    }
                    else if (response.status == responseCode.SHOW_ERROR_MESSAGE) {
                        $scope.displaymsg = response['message'];

                        ngDialog.open({
                            template: 'display_msg_modalDialog',
                            className: 'ngdialog-theme-default',
                            showClose: false,
                            scope: $scope
                        });
                    } else if (response.status == responseCode.ERROR_IN_EXECUTION) {
                        $scope.displaymsg = "Something went wrong, please try again later.";

                        ngDialog.open({
                            template: 'display_msg_modalDialog',
                            className: 'ngdialog-theme-default',
                            showClose: false,
                            scope: $scope
                        });
                    }
                }).error(function(data){
                if(typeof(data)=="string"){
                    data = JSON.parse(data)
                }
                var response = JSON.parse(data.responseText);
                    $scope.displaymsg = response.error;
                    ngDialog.close({
                        template: 'reg_modal',
                        className: 'ngdialog-theme-default',
                        scope: $scope
                    });
                    ngDialog.open({
                        template: 'display_msg_modalDialog',
                        className: 'ngdialog-theme-default',
                        showClose: false,
                        scope: $scope
                    });
                return false;
            });

        };

        /*--------------------------------------------------------------------------
         * --------- funtion to display common msg  --------------------------------
         --------------------------------------------------------------------------*/
        $scope.refreshPage = function () {
            ngDialog.close({
                template: 'display_msg_modalDialog',
                className: 'ngdialog-theme-default',
                scope: $scope
            });
        };

        /*--------------------------------------------------------------------------
         * --------- funtion to open new reg modal  --------------------------------
         --------------------------------------------------------------------------*/
        $scope.openNext = function () {

            ngDialog.close({
                template: 'display_new_reg_msg_modalDialog',
                className: 'ngdialog-theme-default',
                scope: $scope
            });

            ngDialog.open({
                template: 'reg_modal',
                className: 'ngdialog-theme-default',
                showClose: false,
                scope: $scope,
                closeByEscape:false,
                closeByDocument:false,
                preCloseCallback: function () {
                    $scope.newReg ={}
                    return true;
                }
            });
        };
        /*---------------------------------------------------------------------------
        -----------------function to open dialog box of edit user name --------------

         */
        $scope.editUserInfo = function(phonenum){
            console.log(phonenum);
            $scope.newReg={
                countryCode:'1',
                phoneNo:phonenum
            }
            $scope.editUserFlag = 1;
            ngDialog.open({
                template: 'reg_modal',
                className: 'ngdialog-theme-default',
                scope: $scope,
                closeByEscape:false,
                closeByDocument:false,
                preCloseCallback: function () {
                    $scope.newReg ={};
                    $scope.editUserFlag = 0
                    return true;
                }
            });
        }

        //time out error function
        $scope.TimeOutError = function (msg) {
            setTimeout(function () {
                $scope.errorMsg = "";
                $scope.$apply();
            }, 3000);

        };
        //timeout error  function for promo success message
        $scope.TimeOutSuccessError = function () {
            setTimeout(function () {
                $scope.promosuccessMsg = "";
                $scope.promoerrorMsg = "";
                $scope.$apply();
            }, 3000);

        }


    }]);



