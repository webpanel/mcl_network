App.controller('ApproveDriverController', function ($scope, $http, $cookies, $cookieStore, MY_CONSTANT, $state,ngDialog,responseCode,promoService,$stateParams) {

    $scope.editDriver={};
    $scope.approveDriver={}
    console.log($stateParams.driver_id);
    $scope.minDate = new Date();
    $scope.vat_flag=true;
    //$scope.checkbox={
    //    value1:'0',
    //    value2:'0',
    //    value3:'0',
    //    value4:'0'
    //
    //}
    $scope.driver_license_img_sent = 0;
    $scope.taxi_license_img_sent = 0;
    $scope.license_authority_img_sent = 0;
    $scope.license_authority_certificate_img_sent = 0;
    $scope.vehicle_insurance_certi_img_sent = 0;
    $scope.vehicle_image_sent = 0;

    //type of company values
    $scope.gender_values=[{
        id:0,
        name: 'Male'
    },
        {
            id:1,
            name: 'Female'
        }
    ]
    $scope.type_of_company_values = [{
        id:0,
        name: 'Self Employed'
    },
        {
            id:1,
            name: 'Limited Company'
        }

    ];
    //flag value for vat registered
    $scope.vat_registered = [{
        id:0,
        name: 'No'
    },
        {
            id:1,
            name: 'Yes'
        }

    ];
    //area of operation
    $scope.aop_values = [{
        id:0,
        name: 'Aberdeen'
    },
        {
            id:1,
            name: 'Aberdeenshire'
        },
        {
            id:2,
            name: 'Edinburgh'
        },
        {
            id:3,
            name: 'Glasgow'
        },
        {
            id:4,
            name: 'Inverness'
        }

    ];
    //usual hours of operation
    $scope.usual_hour_values = [{
        id:0,
        name: 'Any Shift'
    },
        {
            id:1,
            name: ' Early Shift'
        },
        {
            id:2,
            name: ' Day Shift '
        },
        {
            id:3,
            name: 'Late Shift '
        },
        {
            id:4,
            name: 'Night Shift'
        }

    ];

    /*===========================================================================
     *================== disable vat reg field according to flag=================
     *===========================================================================*/
    $scope.vat_reg_flag = function(id){

        if(id==1){
            $scope.vat_flag=false;
        }
        else
            $scope.vat_flag=true;
    }




    /*--------------------------------------------------------------------------
     * ---------------- get promo code data by using promo code id -------------
     --------------------------------------------------------------------------*/
    $.post(MY_CONSTANT.url + '/get_driver_details', {
            access_token: $cookieStore.get('obj').accesstoken,
            driver_id: $stateParams.driver_id
        },
        function (data) {
            data = JSON.parse(data);
            console.log(data);
            var unapprovedlist = data.data.driver_list[0];
            $scope.uploaded_vehicle_img = unapprovedlist.driver_car_image;
            $scope.uploaded_driver_license_img = unapprovedlist.license_image;
            $scope.uploaded_taxi_license_img = unapprovedlist.taxi_license_image;
            $scope.uploaded_license_authority_img = unapprovedlist.licensing_auth_driver_image;
            $scope.uploaded_license_authority_certificate_img = unapprovedlist.vehicle_test_crtificate_image;
            $scope.uploaded_vehicle_insurance_certi_img = unapprovedlist.vehicle_insurer_policy_certificate;
            var str = unapprovedlist.driver_name.split(" ");
            $scope.approveDriver.phn_no=unapprovedlist.phone_no;
            var phone_num = unapprovedlist.phone_no
            var res = phone_num.substring(0, 1);
            if (res == '+')
                phone_num=phone_num.substring(1);
            $scope.approveDriver.phn_no=phone_num;
            $scope.vat_flag = parseInt(unapprovedlist.registered_vat);
            if($scope.vat_flag==1){
                $scope.vat_flag=false;
            }
            else
                $scope.vat_flag=true;


            $scope.approveDriver={
                first_name : str[0],
                last_name  : str[1],
                driver_email:unapprovedlist.user_email,
                driver_name:unapprovedlist.driver_name,
                phn_no:phone_num,
                gender: parseInt(unapprovedlist.gender),
                dateofbirth:unapprovedlist.date_of_birth,
                taxi_license_type:unapprovedlist.taxi_license_type,
                soc_sec_num:unapprovedlist.driver_social_security_number,
                address:unapprovedlist.address,
                city:unapprovedlist.town_city,
                post_code:unapprovedlist.post_code,
                bank_name:unapprovedlist.bank_name,
                bank_account_name:unapprovedlist.bank_account_name,
                account_num:unapprovedlist.bank_account_no,
                sort_code:unapprovedlist.sort_code,
                type_of_company:parseInt(unapprovedlist.company_type),
                vat_registered:parseInt(unapprovedlist.registered_vat),
                vat_reg_no:unapprovedlist.registered_vat_no,
                driver_license_num:unapprovedlist.license_number,
                lic_start_date:unapprovedlist.license_start_date,
                lic_expiry_date:unapprovedlist.license_expiry_date,
                aop:unapprovedlist.area_of_operation,
                usual_hours_of_op:parseInt(unapprovedlist.operations_hours),
                taxi_license_num:unapprovedlist.taxi_license_number,
                taxi_lic_start_date:unapprovedlist.taxi_license_start_date,
                taxi_lic_expiry_date:unapprovedlist.taxi_license_expriy_date,
                vehicle_make:unapprovedlist.vehicle_make,
                vehicle_model:unapprovedlist.vehicle_model,
                vehicle_emissions:unapprovedlist.vehicle_emissions,
                co2_produced:unapprovedlist.co2_produced_per_km,
                vehicle_reg:unapprovedlist.vehicle_registeration,
                engine_size:unapprovedlist.engine_size,
                fuel_type:unapprovedlist.fuel_type,
                no_of_seats:unapprovedlist.number_of_seats,
                vehicle_num:unapprovedlist.driver_car_no,
                car_type:unapprovedlist.car_type,
                vehicle_insurer_name:unapprovedlist.vehicle_insurer_name,
                vehicle_insurance_policy_number:unapprovedlist.vehicle_insurer_policy_number,
                vehicle_insurance_start_date:unapprovedlist.vehicle_insurer_start_date,
                vehicle_insurance_expiry_date:unapprovedlist.vehicle_insurer_expiry_date,
                driver_commission:unapprovedlist.driver_commission

            }
            var res2 = unapprovedlist.taxi_license_type;
            $scope.checkbox={
                value1:res2.substring(0,1),
                value2:res2.substring(1,2),
                value3:res2.substring(2,3),
                value4:res2.substring(3,4)

            }

            $scope.checkboxshow={
                value1:res2.substring(0,1),
                value2:res2.substring(1,2),
                value3:res2.substring(2,3),
                value4:res2.substring(3,4)

            }
            console.log($scope.checkboxshow);
            if($scope.approveDriver.dateofbirth=='0000-00-00'|| $scope.approveDriver.dateofbirth=='Invalid date'){
                $scope.approveDriver.dateofbirth='';
            }
            if($scope.approveDriver.lic_start_date=='0000-00-00'|| $scope.approveDriver.lic_start_date=='Invalid date'){
                $scope.approveDriver.lic_start_date='';
            }
            if($scope.approveDriver.lic_expiry_date=='0000-00-00'|| $scope.approveDriver.lic_expiry_date=='Invalid date'){
                $scope.approveDriver.lic_expiry_date='';
            }
            if($scope.approveDriver.taxi_lic_start_date=='0000-00-00'|| $scope.approveDriver.taxi_lic_start_date=='Invalid date'){
                $scope.approveDriver.taxi_lic_start_date='';
            }
            if($scope.approveDriver.taxi_lic_expiry_date=='0000-00-00'|| $scope.approveDriver.taxi_lic_expiry_date=='Invalid date'){
                $scope.approveDriver.taxi_lic_expiry_date='';
            }
            if($scope.approveDriver.vehicle_insurance_start_date=='0000-00-00'|| $scope.approveDriver.vehicle_insurance_start_date=='Invalid date'){
                $scope.approveDriver.vehicle_insurance_start_date='';
            }
            if($scope.approveDriver.vehicle_insurance_expiry_date=='0000-00-00'|| $scope.approveDriver.vehicle_insurance_expiry_date=='Invalid date'){
                $scope.approveDriver.vehicle_insurance_expiry_date='';
            }

        });
    /*===========================================================================
     *============================getting img file================================
     *===========================================================================*/

    $scope.file_to_upload = function (files,id) {
        console.log(files);
        if(id==0){
            $scope.driver_license_img = files[0];
            $scope.driver_license_img_sent = 1;

            $scope.driver_license_name = files[0].name;
            var file = files[0];
            var imageType = /image.*/;
            if (!file.type.match(imageType)) {

            }
            var img = document.getElementById("driver_license_image");
            img.file = file;
            var reader = new FileReader();
            reader.onload = (function (aImg) {
                return function (e) {
                    aImg.src = e.target.result;
                };
            })(img);
            reader.readAsDataURL(file);

        }
        if(id==1){
            $scope.taxi_license_img = files[0];
            $scope.taxi_license_img_sent = 1;
            $scope.taxi_license_name = files[0].name;
            var file = files[0];
            var imageType = /image.*/;
            if (!file.type.match(imageType)) {

            }
            var img = document.getElementById("taxi_license_image");
            img.file = file;
            var reader = new FileReader();
            reader.onload = (function (aImg) {
                return function (e) {
                    aImg.src = e.target.result;
                };
            })(img);
            reader.readAsDataURL(file);

        }
        if(id==2){
            $scope.license_authority_img = files[0];
            $scope.license_authority_img_sent = 1;
            $scope.license_authority_img_name = files[0].name;
            var file = files[0];
            var imageType = /image.*/;
            if (!file.type.match(imageType)) {

            }
            var img = document.getElementById("license_authority_image");
            img.file = file;
            var reader = new FileReader();
            reader.onload = (function (aImg) {
                return function (e) {
                    aImg.src = e.target.result;
                };
            })(img);
            reader.readAsDataURL(file);
        }
        if(id==3){
            $scope.license_authority_certificate_img = files[0];
            $scope.license_authority_certificate_img_sent = 1;
            $scope.license_authority_certificate_img_name = files[0].name;
            var file = files[0];
            var imageType = /image.*/;
            if (!file.type.match(imageType)) {

            }
            var img = document.getElementById("license_authority_certificate_image");
            img.file = file;
            var reader = new FileReader();
            reader.onload = (function (aImg) {
                return function (e) {
                    aImg.src = e.target.result;
                };
            })(img);
            reader.readAsDataURL(file);

        }
        if(id==4){
            $scope.vehicle_insurance_certi_img = files[0];
            $scope.vehicle_insurance_certi_img_sent = 1;
            $scope.vehicle_insurance_certi_img_name = files[0].name;

            var file = files[0];
            var imageType = /image.*/;
            if (!file.type.match(imageType)) {

            }
            var img = document.getElementById("vehicle_insurance_certi_img");
            img.file = file;
            var reader = new FileReader();
            reader.onload = (function (aImg) {
                return function (e) {
                    aImg.src = e.target.result;
                };
            })(img);
            reader.readAsDataURL(file);

        } if(id==5){
            $scope.vehicle_image = files[0];
            $scope.vehicle_image_sent = 1;
            $scope.vehicle_image_name = files[0].name;
            var file = files[0];
            var imageType = /image.*/;
            if (!file.type.match(imageType)) {

            }
            var img = document.getElementById("car_image");
            img.file = file;
            var reader = new FileReader();
            reader.onload = (function (aImg) {
                return function (e) {
                    aImg.src = e.target.result;
                };
            })(img);
            reader.readAsDataURL(file);
        }


        $scope.$apply();


    }

    /*--------------------------------------------------------------------------
     * ---------------- function to approve driver -----------------------------
     --------------------------------------------------------------------------*/
    $scope.approveDriverFunction = function () {

        //console.log($scope.approveDriver.car_type);
        //if($scope.checkbox.value1==true){
        //    $scope.checkbox.value1=1;
        //}
        //if($scope.checkbox.value2==true){
        //    $scope.checkbox.value2=1;
        //}
        //if($scope.checkbox.value3==true){
        //    $scope.checkbox.value3=1;
        //}
        //if($scope.checkbox.value4==true){
        //    $scope.checkbox.value4=1;
        //}
        //
        //
        //
        //var checkbox_string = ""+$scope.checkbox.value1+$scope.checkbox.value2+$scope.checkbox.value3+$scope.checkbox.value4;
        //console.log(checkbox_string);

        if($scope.approveDriver.dateofbirth == '' || $scope.approveDriver.dateofbirth == undefined || $scope.approveDriver.dateofbirth == null)
        {
            $scope.errorMsg = "Enter User's Date Of Birth";
            $scope.TimeOutError();
            return false;

        }
        if($scope.driver_license_img_sent==0 && $scope.uploaded_driver_license_img =='' ){
            $scope.errorMsg = "Upload Driver License image.";
            $scope.TimeOutError();
            return false;
        }

        if(
            $scope.approveDriver.lic_start_date == '' || $scope.approveDriver.lic_start_date == undefined || $scope.approveDriver.lic_start_date == null ||
            $scope.approveDriver.lic_expiry_date == '' || $scope.approveDriver.lic_expiry_date == undefined || $scope.approveDriver.lic_expiry_date == null){
            $scope.errorMsg = "Enter Driver's License Start and Expiry Date";
            $scope.TimeOutError();
            return false;

        }
        if(
            $scope.approveDriver.taxi_lic_start_date == '' || $scope.approveDriver.taxi_lic_start_date == undefined || $scope.approveDriver.taxi_lic_start_date == null ||
            $scope.approveDriver.taxi_lic_expiry_date == '' || $scope.approveDriver.taxi_lic_expiry_date == undefined || $scope.approveDriver.taxi_lic_expiry_date == null){
            $scope.errorMsg = "Enter Taxi's License Start and Expiry Date";
            $scope.TimeOutError();
            return false;

        }
        if($scope.taxi_license_img_sent==0 && $scope.uploaded_taxi_license_img =='' ){
            $scope.errorMsg = "Upload Taxi License Image.";
            $scope.TimeOutError();
            return false;
        }
        if($scope.license_authority_img_sent==0 && $scope.uploaded_license_authority_img =='' ){
            $scope.errorMsg = "Upload Licensing Authority Driver ID.";
            $scope.TimeOutError();
            return false;
        }
        if($scope.license_authority_certificate_img_sent==0 && $scope.uploaded_license_authority_certificate_img =='' ){
            $scope.errorMsg = "Upload Licensing Authority Vehicle Test Certificate.";
            $scope.TimeOutError();
            return false;
        }
        if($scope.vehicle_insurance_certi_img_sent==0 && $scope.vehicle_insurance_certi_img =='' ){
            $scope.errorMsg = "Upload Vehicle Insurance Certificate.";
            $scope.TimeOutError();
            return false;
        }



        if((typeof $scope.approveDriver.car_type == 'undefined') || $scope.approveDriver.car_type == undefined){
            $scope.errorMsg = "Select car type";
            $scope.TimeOutError();
            return false;
        }
        if($scope.vehicle_image_sent==0 && $scope.uploaded_vehicle_img =='' ){
            $scope.errorMsg = "Upload vehicle image.";
            $scope.TimeOutError();
            return false;
        }

        if(
            $scope.approveDriver.vehicle_insurance_expiry_date == '' || $scope.approveDriver.vehicle_insurance_expiry_date == undefined || $scope.approveDriver.vehicle_insurance_expiry_date == null ||
            $scope.approveDriver.vehicle_insurance_start_date == '' || $scope.approveDriver.vehicle_insurance_start_date == undefined || $scope.approveDriver.vehicle_insurance_start_date == null){
            $scope.errorMsg = "Enter Vehicle's Insurance Start and Expiry Date";
            $scope.TimeOutError();
            return false;

        }
        if($scope.approveDriver.vehicle_emissions >$scope.approveDriver.co2_produced)
        {
            $scope.errorMsg = "Vehicle Emission value should be lower than CO2 produced.";
            $scope.TimeOutError();
            return false;
        }
        if($scope.checkbox.value1==true ||$scope.checkbox.value1==1 ){
            $scope.value1=1;
        }
        else{
            $scope.value1=0
        }

        if($scope.checkbox.value2==true ||$scope.checkbox.value2==1){
            $scope.value2=1;
        }
        else{
            $scope.value2=0
        }
        if($scope.checkbox.value3==true ||$scope.checkbox.value3==1){
            $scope.value3=1;
        }
        else{
            $scope.value3=0
        }


        if($scope.checkbox.value4==true ||$scope.checkbox.value4==1){
            $scope.value4=1;
        }
        else{
            $scope.value4=0
        }



        var checkbox_string = ""+$scope.value1+$scope.value2+$scope.value3+$scope.value4;
        console.log(checkbox_string);

        //$scope.approveDriver.dateofbirth = moment($scope.approveDriver.dateofbirth).format("YYYY/MM/DD") ;
        //$scope.approveDriver.lic_start_date = moment($scope.approveDriver.lic_start_date).format("YYYY/MM/DD") ;
        //$scope.approveDriver.lic_expiry_date = moment($scope.approveDriver.lic_expiry_date).format("YYYY/MM/DD") ;
        //$scope.approveDriver.taxi_license_start_date = moment($scope.approveDriver.taxi_license_start_date).format("YYYY/MM/DD") ;
        //$scope.approveDriver.taxi_license_expiry_date = moment($scope.approveDriver.taxi_license_expiry_date).format("YYYY/MM/DD") ;
        //$scope.approveDriver.vehicle_insurance_start_date = moment($scope.approveDriver.vehicle_insurance_start_date).format("YYYY/MM/DD") ;
        //$scope.approveDriver.vehicle_insurance_expiry_date = moment($scope.approveDriver.vehicle_insurance_expiry_date).format("YYYY/MM/DD") ;



        //==================================================================================================================================
        var formData = new FormData();
        formData.append('access_token', $cookieStore.get('obj').accesstoken);
        formData.append('driver_id', $stateParams.driver_id);
        formData.append('driver_email', $scope.approveDriver.driver_email);
        formData.append('driver_vehicle_number', $scope.approveDriver.vehicle_num);
        formData.append('driver_license_number', $scope.approveDriver.driver_license_num);
        formData.append('social_security_number', $scope.approveDriver.soc_sec_num);
        formData.append('driver_car_type', $scope.approveDriver.car_type);
        formData.append('phone_no', "+"+$scope.approveDriver.phn_no);
        formData.append('driver_paypal_id', 'xyz@paypal.com');
        formData.append('driver_vehicle_image', $scope.vehicle_image);
        formData.append('driver_vehicle_image_flag', $scope.vehicle_image_sent); //flag for img sent
        formData.append('vehicle_emissions', $scope.approveDriver.vehicle_emissions);
        formData.append('co2_produced_per_km', $scope.approveDriver.co2_produced); //previous fields
        formData.append('first_name', $scope.approveDriver.first_name);
        formData.append('last_name', $scope.approveDriver.last_name);
        formData.append('gender', $scope.approveDriver.gender);
        formData.append('date_of_birth',$scope.approveDriver.dateofbirth );
        formData.append('address', $scope.approveDriver.address);
        formData.append('town_city', $scope.approveDriver.city);
        formData.append('post_code', $scope.approveDriver.post_code);
        formData.append('bank_name', $scope.approveDriver.bank_name);
        formData.append('bank_account_name', $scope.approveDriver.bank_account_name);
        formData.append('bank_account_no', $scope.approveDriver.account_num);
        formData.append('sort_code', $scope.approveDriver.sort_code);
        formData.append('company_type', $scope.approveDriver.type_of_company);
        formData.append('registered_vat', $scope.approveDriver.vat_registered);
        formData.append('registered_vat_no',$scope.approveDriver.vat_reg_no);
        formData.append('license_image', $scope.driver_license_img);
        formData.append('license_image_flag', $scope.driver_license_img_sent);//flag for img sent
        formData.append('license_number', $scope.approveDriver.driver_license_num);
        formData.append('license_start_date', $scope.approveDriver.lic_start_date);
        formData.append('license_expiry_date', $scope.approveDriver.lic_expiry_date);
        formData.append('area_of_operation', $scope.approveDriver.aop);
        formData.append('operations_hours', $scope.approveDriver.usual_hours_of_op);
        formData.append('taxi_license_type', checkbox_string);
        formData.append('taxi_license_image',  $scope.taxi_license_img);
        formData.append('taxi_license_image_flag',  $scope.taxi_license_img_sent);
        formData.append('taxi_license_number', $scope.approveDriver.taxi_license_num);
        formData.append('taxi_license_start_date', $scope.approveDriver.taxi_lic_start_date);
        formData.append('taxi_license_expriy_date', $scope.approveDriver.taxi_lic_start_date);
        formData.append('licensing_auth_driver_image', $scope.license_authority_img);
        formData.append('licensing_auth_driver_image_flag', $scope.license_authority_img_sent);

        formData.append('vehicle_test_crtificate_image', $scope.license_authority_certificate_img);
        formData.append('vehicle_test_crtificate_image_flag',$scope.license_authority_certificate_img_sent);
        formData.append('vehicle_make', $scope.approveDriver.vehicle_make);
        formData.append('vehicle_model', $scope.approveDriver.vehicle_model);
        formData.append('vehicle_registeration', $scope.approveDriver.vehicle_reg);
        formData.append('engine_size', $scope.approveDriver.engine_size);
        formData.append('fuel_type', $scope.approveDriver.fuel_type);
        formData.append('number_of_seats', $scope.approveDriver.no_of_seats);
        formData.append('vehicle_insurer_name', $scope.approveDriver.vehicle_insurer_name);
        formData.append('vehicle_insurer_policy_number', $scope.approveDriver.vehicle_insurance_policy_number);
        formData.append('vehicle_insurer_policy_certificate', $scope.vehicle_insurance_certi_img);
        formData.append('vehicle_insurer_policy_certificate_flag',$scope.vehicle_insurance_certi_img_sent);
        formData.append('vehicle_insurer_start_date', $scope.approveDriver.vehicle_insurance_start_date);
        formData.append('vehicle_insurer_expiry_date', $scope.approveDriver.vehicle_insurance_expiry_date);
        formData.append('driver_commission', $scope.approveDriver.driver_commission);





        $.ajax({
            type: 'POST',
            url: MY_CONSTANT.url + '/approve_driver',
            dataType: "json",
            data: formData,
            async: false,
            processData: false,
            contentType: false,
            success: function (data) {
                console.log(data);
                if (data.status == responseCode.SUCCESS) {
                    $scope.displaymsg = "Driver Approved Successfully.";
                }
                else if (data.status == responseCode.INVALID_ACCESS_TOKEN) {
                    $state.go('page.login');
                }
                else {
                    $scope.displaymsg = data.message;
                }
                ngDialog.open({
                    template: 'display_msg_modalDialog',
                    className: 'ngdialog-theme-default',
                    showClose: false,
                    scope: $scope
                });

            }
        });

    };


    /*--------------------------------------------------------------------------
     * --------- funtion to refresh page ---------------------------------------
     --------------------------------------------------------------------------*/
    $scope.refreshPage = function () {
        $state.go('app.pendingdrivers');
        ngDialog.close({
            template: 'display_msg_modalDialog',
            className: 'ngdialog-theme-default',
            scope: $scope
        });

    };

    /*--------------------------------------------------------------------------
     * ---------------- function to get date -----------------------------------
     --------------------------------------------------------------------------*/
    function dateConversion(dte) {
        var dteSplit1 = dte.split("T");
        var date = dteSplit1[0];
        return date;
    }
    /*--------------------------------------------------------------------------
     * ---------------- Timeout function -----------------------------------
     --------------------------------------------------------------------------*/
    $scope.TimeOutError = function () {
        setTimeout(function () {
            $scope.errorMsg = "";
            $scope.$apply();
        }, 3000);

    }


    $scope.getVal1 = function(val){
        $scope.checkbox.value1 = val;
        console.log($scope.checkbox.value1);
    }

    $scope.getVal2 = function(val){
        $scope.checkbox.value2 = val;
        console.log($scope.checkbox.value2);

    }
    $scope.getVal3 = function(val){
        $scope.checkbox.value3 = val;
        console.log($scope.checkbox.value3);

    }
    $scope.getVal4 = function(val){
        $scope.checkbox.value4 = val;
        console.log($scope.checkbox.value4);

    }


});

