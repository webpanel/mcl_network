App.controller('DashboardController', ['$scope', '$timeout', '$http', 'uiGmapLogger', '$cookies', '$cookieStore', 'MY_CONSTANT', '$state','responseCode', 'uiGmapGoogleMapApi','MapLatLong'
    , function ($scope, $timeout, $http, $log, $cookies, $cookieStore, MY_CONSTANT, $state, responseCode,GoogleMapApi,MapLatLong,socket) {


        var socket = io.connect(MY_CONSTANT.url_booking);

        socket.on('send:time', function (data) {
            console.log(data);
            $scope.time = data.time;
        });

        $log.currentLevel = $log.LEVELS.debug;

        $scope.total_no_of_drivers = "";
        $scope.MapTitle = "Driver Name";

        markerArr = new Array();
        markerCount = 0;
        var searchMarkers = [];
        var bound_val =0;
        $scope.showdataflag = 1;
        //if($cookieStore.get('type')==0 ||$cookieStore.get('type')==1){
        //    $scope.showdataflag = 1;
        //}
        //else if($cookieStore.get('type')==2){
        //    $scope.showdataflag = 0;
        //}

        //$cookieStore.put('zoom', 10);


        socket.on('admin:LocationUpdated', function (data) {

//            console.log("driver location updated : ")
//            console.log(data);

            markerArr.forEach(function(marker){
               if(marker.userId == data.userId)
               {
                   //console.log("user found ....");
                   marker.setPosition(new google.maps.LatLng(data.latitude, data.longitude));
//                   move(marker , data.latitude , data.longitude);

               }
            });

            //$scope.markerClusterer = new MarkerClusterer( $scope.mapContainer, markerArr,$scope.mcOptions);
            //bound_val =  $scope.setBounds();
        });
        
        var rides = {
            
        };
        
        function createCircle(lat , lng , radious){
                      var circle = new google.maps.Circle({
            strokeColor: '#0000ff',
            strokeOpacity: 0.8,
            strokeWeight: 2,
            fillColor: '#0000cc',
            fillOpacity: 0.35,
            map: $scope.mapContainer,
            center:new google.maps.LatLng(lat, lng) ,
            radius: radious
          });
            
            return circle;
        }
        
        
         socket.on('admin:rideStatus', function (data) {
             console.log(data);
             
             if(!rides[data.rideId]){
                 rides[data.rideId] = {
                     status : data.status
                 }
             }
             
             switch (data.flag) {
                    case 0:   //WHEN DRIVER IS BEING ASSIGNED
                     
                     if(data.status == "In Progress" )
                         {
                             
                           rides[data.rideId].circle =  createCircle(data.data.rideData.pickupLatitude, data.data.rideData.pickupLongitude,50)
                           rides[data.rideId].userName = data.data.rideData.userName;
                           rides[data.rideId].email = data.data.rideData.userData.email;
                            rides[data.rideId].userId = data.data.rideData.userData.userId;
                             rides[data.rideId].pickupLatitude =data.data.rideData.pickupLatitude;
                             rides[data.rideId].pickupLongitude =data.data.rideData.pickupLongitude;
                             
                             
                             
                             
                         }else {
                             
                             console.log(rides[data.rideId].circle)
                             console.log(rides[data.rideId].circle.toString())
                             rides[data.rideId].circle.fillColor = "#00dd00"
                             rides[data.rideId].circle.strokeColor = "#00dd00"
                             if(rides[data.rideId].driverCircle)
                                 {
                                     rides[data.rideId].driverCircle.setMap(null);
                                     delete rides[data.rideId].driverCircle
                                 }
                             
                             rides[data.rideId].driverCircle = createCircle(data.data.driverData.latitude, data.data.driverData.longitude,20)
                             
                             
                             
                         }
                     

                        break;
                    case 1:   //WHEN DRIVER HAS ACCEPTED
                      

                        break;
                    case 2:   //WHEN DRIVER HAS ARRIVED
                       
                        break;
                    case 3:   //WHEN RIDE IS STARTED
                      

                        break;
                    case 4:   //WHEN RIDE IS ENDED
                      
                        break;
                    case 9:   //WHEN  DRIVER REJECTED
                      
                        break;
                    case 10:   //WHEN NO DRIVER ACCEPTED OR AVAILABLE
                     
                      if(rides[data.rideId].driverCircle){
                          rides[data.rideId].driverCircle.setMap(null);
                          
                      }
                     if(rides[data.rideId].circle){
                          rides[data.rideId].circle.setMap(null);
                          
                      }
                      
                        break;
                }
         })



function move(marker , latitude , longitude){


    var numDeltas = 5;
    var delay = 10; //milliseconds
    var i = 0;
    var lat;
    var lng;
    var deltaLat,deltaLng;

    transition();
    function transition(){
        i = 0;

        lat = marker.getPosition().lat();
        lng = marker.getPosition().lng();
        deltaLat = (lat - latitude)/numDeltas;
        deltaLng = (lng - longitude)/numDeltas;

        console.log(deltaLat,deltaLng)
        moveMarker();
    }

    function moveMarker() {
        lat += deltaLat;
        lng += deltaLng;

        marker.setPosition(new google.maps.LatLng(lat, lng));
        if (i != numDeltas) {
            i++;
            setTimeout(moveMarker, delay);
        }
    }


}




        $scope.map = {
            zoom: $cookieStore.get('zoom') || 10,
            center: new google.maps.LatLng(MapLatLong.lat, MapLatLong.lng),
            pan : true
        }
        // Create the search box and link it to the UI element.
        var input = document.getElementById('pac-input');
        var searchBox = new google.maps.places.SearchBox(input);







        $scope.mapContainer = new google.maps.Map(document.getElementById('map-container'), $scope.map);
        $scope.mapContainer.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

        // Bias the SearchBox results towards current map's viewport.
        $scope.mapContainer.addListener('bounds_changed', function() {
            searchBox.setBounds($scope.mapContainer.getBounds());
        });
        

      

        var infoWindow = new google.maps.InfoWindow();
//============================================================================================================================
//=================Listen for the event fired when the user selects a prediction and retrieve from search box=================
//============================================================================================================================

        // [START region_getplaces]
        // Listen for the event fired when the user selects a prediction and retrieve
        // more details for that place.
        searchBox.addListener('places_changed', function() {
            var places = searchBox.getPlaces();

            if (places.length == 0) {
                return;
            }

            // Clear out the old markers.
            searchMarkers.forEach(function(marker) {
                marker.setMap(null);
            });
            searchMarkers = [];

            // For each place, get the icon, name and location.
            var bounds = new google.maps.LatLngBounds();
            places.forEach(function(place) {
                var icon = {
                    url: place.icon,
                    size: new google.maps.Size(71, 71),
                    origin: new google.maps.Point(0, 0),
                    anchor: new google.maps.Point(17, 34),
                    scaledSize: new google.maps.Size(25, 25)
                };

                // Create a marker for each place.
                searchMarkers.push(new google.maps.Marker({
                    map: $scope.mapContainer,
                    icon: icon,
                    title: place.name,
                    position: place.geometry.location
                }));

                if (place.geometry.viewport) {
                    // Only geocodes have viewport.
                    bounds.union(place.geometry.viewport);
                } else {
                    bounds.extend(place.geometry.location);
                }
            });
            $scope.mapContainer.fitBounds(bounds);
        });
        // [END region_getplaces]


        //============================================================================================================================
        //==================================================== Function to create markers=============================================
//============================================================================================================================

        var createMarker = function (info) {
          //  console.log(info);
            switch(info.status){
                case 0 :
                    var icon = 'app/img/offmodeDriver.png';
                    break;
                case 1 :
                    var icon = 'app/img/freeDriver.png';
                    break;
                case 2 :
                    var icon = 'app/img/busyDriver.png';
                    break;
                case 3 :
                    var icon = 'app/img/offmodeDriver.png';
                    break;
            }


            var marker = new MarkerWithLabel({
                position: new google.maps.LatLng(info.latitude, info.longitude),
                map: $scope.mapContainer,
                //labelContent: "" + (info.car_type + 1),
                icon: icon,
                animation: google.maps.Animation.DROP
                //labelAnchor: new google.maps.Point(9, 37),
                //labelClass: "labels", // the CSS class for the label
                //labelStyle: {opacity: 0.9}
            });
            marker.userId = info.userId;
          //  console.log("marker Id :" + marker.userId)
        //    console.log("marker position :" + marker.position)

            marker.content = '<div class="infoWindowContent">' +
                '<center>Driver Info</center>' +
                '<span> Name - ' + info.firstName + ' ' + info.lastName +'</span><br>' +
                '<span> Phone - ' + info.phoneNo + '</span><br>' +
                '<span> Zone - ' + info.regionName + '</span><br>' +
                '<span> Car Type - ' + info.carType + '</span><br>' +
                '<span> Vehicle Number - ' + info.carNo + '</span><br>' +
                '<span> waiting time - ' + info.waitingTime + '</span>' +
                '</div>';

            google.maps.event.addListener(marker, 'click', function () {
                infoWindow.setContent(marker.content);
                infoWindow.open($scope.mapContainer, marker);
            });


            markerArr.push(marker);


            markerCount = markerCount + 1;



        }

        $scope.drawMap = function () {

            $.post(MY_CONSTANT.url + '/api/dashboard/dashboard_map', {
                    accessToken: $cookieStore.get('obj').accesstoken
                },
                function (response) {
                    if(typeof data == "string")
                    {
                        response = JSON.parse(response);
                    }
                    console.log(response);
                    if (response.flag == responseCode.SUCCESS) {


                        var data = response.data;
                        $scope.driverStats = {
                          'offline': data.offline,
                          'free': data.free,
                          'busy': data.busy
                        };

                        var markerAddresses = [];
                        var driverData = data.drivers;

                        var length = data.drivers.length;

                        $scope.total_no_of_drivers = length;
                        $scope.$apply();

                        if (length) {


                            // var panPoint = new google.maps.LatLng(data.driver_data[0].current_location_latitude, data.driver_data[0].current_location_longitude);
                            // $scope.mapContainer.panTo(panPoint);

                            driverData.forEach(function (column) {
                                createMarker(column);
                                $scope.openInfoWindow = function (e, selectedMarker) {
                                    e.preventDefault();
                                    google.maps.event.trigger(selectedMarker, 'click');
                                }
                            });
                            $scope.mcOptions = {gridSize: 50, maxZoom: 20};

                            if ($scope.markerClusterer) {
                                $scope.markerClusterer.clearMarkers();   //clearing the markercluster to add new
                            }



                            $scope.markerClusterer = new MarkerClusterer( $scope.mapContainer, markerArr,$scope.mcOptions);

                            //function to get lat long bounds according to marker position
                            bound_val =  $scope.setBounds();

                        }
                        else {

                            //new google.maps.Marker({
                            //    map: $scope.mapContainer,
                            //    position: new google.maps.LatLng(30.718789, 76.810155)
                            //});


                        }




                        google.maps.event.addListener($scope.mapContainer, 'zoom_changed', function() {
                            $cookieStore.put('zoom', $scope.mapContainer.getZoom());
                        });


                    }
                })
        }

        $scope.setBounds = function(){

            if(bound_val==0){

                var bounds = new google.maps.LatLngBounds();
                for(var i=0;i<markerCount;i++) {
                    bounds.extend(markerArr[i].getPosition());
                }
                $scope.mapContainer.fitBounds(bounds);

            }



            return 1;
        }

        $scope.drawMap();

        $scope.setinterval= setInterval(function(){

            //for(var i=0; i< markerArr.length; i++){
            //    console.log("length"+markerArr.length);
            //    markerArr[i].setMap(null);
            //}

            markerArr = [];    //empty the markerArray to refresh the map

            $scope.drawMap();
        }, 100000);

        $scope.$on('$destroy',function(){
            socket.close();
        });

        $scope.stats = {};
        $scope.stats.rides = 0;
        $scope.stats.users = 0;
        $scope.stats.revenue = 0;
        $scope.stats.userstoday = 0;
        $scope.stats.ridestoday = 0;
        $scope.stats.revenuetoday = 0;

        var now = new Date();
        var yesterday = new Date();

        var yesterday = moment(yesterday).format("YYYY-MM-DD");
        var now = moment(now).format("YYYY-MM-DD");
        yesterday = startDateToUTC(yesterday);
        now = endDateToUTC(now);

        $.post(MY_CONSTANT.url + '/api/regions/list', {
            access_token: $cookieStore.get('obj').accesstoken,
        }, function (response) {
//            response = JSON.parse(response);
          //  console.log(response);
            
            response.data.regions.forEach(function(region){
                
            //    console.log(region.polygon)
                
        var triangleCoords = [

        ];
                
                region.polygon[0].forEach(function(latLon){
                    triangleCoords.push({
                        lat : latLon.x,
                        lng : latLon.y
                    })
                    
                        var mapLabel = new MapLabel({
        text: latLon.x+","+ latLon.y,
        position: new google.maps.LatLng(latLon.x, latLon.y),
        map: $scope.mapContainer,
        fontSize: 10,
        align: 'right'
    });
                })
                
                


        // Construct the polygon.
        var bermudaTriangle = new google.maps.Polygon({
          paths: triangleCoords,
          strokeColor: '#FF0000',
          strokeOpacity: 0.8,
          strokeWeight: 2,
          fillColor: '#FF0000',
          fillOpacity: 0.35
        });
        bermudaTriangle.setMap($scope.mapContainer);
                
if (!google.maps.Polygon.prototype.getBounds) {
 
google.maps.Polygon.prototype.getBounds=function(){
    var bounds = new google.maps.LatLngBounds()
    this.getPath().forEach(function(element,index){bounds.extend(element)})
    return bounds
}
 
}
                
             //   console.log(bermudaTriangle.getBounds().getCenter().lat(), bermudaTriangle.getBounds().getCenter().lng())
                
    var mapLabel = new MapLabel({
        text: region.regionName,
        position: new google.maps.LatLng(bermudaTriangle.getBounds().getCenter().lat(), bermudaTriangle.getBounds().getCenter().lng()),
        map: $scope.mapContainer,
        fontSize: 12,
        align: 'right'
    });
                
            })
//            if (response.status == responseCode.SUCCESS) {
//                var data = response.data;
//                $scope.stats.revenue = data.total_data.earnings;
//                $scope.stats.rides = data.total_data.rides;
//                $scope.stats.users = data.total_users;
//                $scope.stats.userstoday = data.total_users_registered_today;
//                $scope.stats.ridestoday = data.specified_dates_data.rides;
//                $scope.stats.revenuetoday = data.specified_dates_data.earnings;
//                $scope.$apply();
//            }
        })


    }]);