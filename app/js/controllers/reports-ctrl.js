/**
 * Created by sanjay on 3/28/15.
 */
myApp.controller('ChartController', function ($scope,  $timeout, $cookies, $cookieStore, MY_CONSTANT, responseCode,ngDialog,$location) {
    'use strict';

    $scope.reports = {};
    //var searchObject = $location.search();
    //if(!angular.isUndefined(searchObject.start_date) ){
    //    console.log("searchObject");
    //    console.log(searchObject.start_date);
    //    console.log("searchObject");
    //}
    //else{
    //    console.log("kuchbhi");
    //
    //}



    /*----------------- draw charts for last week -------------------------------*/
    var now = new Date();
    var startTime = now.setDate(now.getDate() - 7);
    $scope.offset = now.getTimezoneOffset();
    drawCharts(now.toISOString(),new Date().toISOString());


    /*--------------------------------------------------------------------------
     * ---------------- funtion to draw charts ---------------------------------
     --------------------------------------------------------------------------*/
    $scope.findCharts = function () {

        var start_date = $scope.reports.start_date1;
        var end_date = $scope.reports.end_date1;

        if(end_date)
            end_date.setDate(end_date.getDate() + 1);

        var days = end_date - start_date;

        if($scope.reports.start_date1 == '' || $scope.reports.start_date1 == undefined || $scope.reports.start_date1 == null){

            $scope.displaymsg = "Please select start date.";

            ngDialog.open({
                template: 'display_msg_modalDialog',
                className: 'ngdialog-theme-default',
                showClose: false,
                scope: $scope
            });
        }
        else if($scope.reports.end_date1 == '' || $scope.reports.end_date1 == undefined || $scope.reports.end_date1 == null){
            $scope.displaymsg = "Please select end date.";

            ngDialog.open({
                template: 'display_msg_modalDialog',
                className: 'ngdialog-theme-default',
                showClose: false,
                scope: $scope
            });
        }
        else if (days <= 0) {
            $scope.displaymsg = "Start date must be less than end date";
            ngDialog.open({
                template: 'display_msg_modalDialog',
                className: 'ngdialog-theme-default',
                showClose: false,
                scope: $scope
            });
        }
        else{
            start_date = $("#start_date").val();
            end_date = $("#end_date").val();

            start_date = startDateToUTC(start_date);
            end_date = endDateToUTC(end_date);

            drawCharts(start_date,end_date);

        }

    }

    /*--------------------------------------------------------------------------
     * ---------------- code for datepicker ------------------------------------
     --------------------------------------------------------------------------*/

    $scope.maxDate = new Date();

    $scope.today = function() {
        $scope.promo.start_date = new Date();
    };
    //$scope.today();

    $scope.clear = function () {
        $scope.promo.start_date = null;
    };

    $scope.toggleMin = function() {
        $scope.maxDate = $scope.maxDate ? null : new Date();
    };
    $scope.toggleMin();

    $scope.open = function($event) {
        $event.preventDefault();
        $event.stopPropagation();

        $scope.opened = true;
    };

    $scope.dateOptions = {
        formatYear: 'yy',
        startingDay: 1
    };

    $scope.initDate = new Date();
    $scope.format = 'yyyy-MM-dd';



    $scope.today = function() {
        $scope.promo.end_date = new Date();
    };
    //$scope.today();

    $scope.clear = function () {
        $scope.promo.end_date = null;
    };

    $scope.toggleMin = function() {
        $scope.maxDate = $scope.maxDate ? null : new Date();
    };
    $scope.toggleMin();

    $scope.open1 = function($event) {
        $event.preventDefault();
        $event.stopPropagation();

        $scope.opened1 = true;
    };

    $scope.dateOptions = {
        formatYear: 'yy',
        startingDay: 1
    };

    $scope.initDate = new Date();
    $scope.format = 'yyyy-MM-dd';

    /*--------------------------------------------------------------------------
     * ---------------- function to draw charts --------------------------------
     --------------------------------------------------------------------------*/
    function drawCharts(start_date,end_date){

        /*----------------- API Heat for earning chart ---------------------------*/
        $.post(MY_CONSTANT.url + '/heat_map_earnings', {
            access_token: $cookieStore.get('obj').accesstoken,
            start_time: start_date,
            end_time: end_date,
            timezone: $scope.offset
        }, function (response) {
            var data = JSON.parse(response);
            var chart = new Highcharts.Chart(data);
            $scope.$apply();
        });


        /*----------------- API Heat for ride chart ------------------------------*/
        $.post(MY_CONSTANT.url + '/heat_map_rides_data', {
            access_token: $cookieStore.get('obj').accesstoken,
            start_time: start_date,
            end_time: end_date,
            timezone: $scope.offset
        }, function (response) {
            var data = JSON.parse(response);
            var chart = new Highcharts.Chart(data);
            $scope.$apply();
        });
    }


});

