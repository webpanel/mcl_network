/**
 * Created by sanjay on 3/25/15.
 */
App.controller('RestPasswordController', function ($scope, $http,$state, $cookies,$location , $cookieStore, MY_CONSTANT, $state,responseCode) {
    //initially set those objects to null to avoid undefined error
    // place the message if something goes wrong
    $scope.resetPass = {};

    var token = $location.search().token;
    var email = $location.search().email;
    email = email.replace(' ', '+');
    console.log(email);

    $scope.resetPassword = function () {
        $scope.errorMsg = '';
        if($scope.resetPass.password != $scope.resetPass.confirmpassword){
            $scope.errorMsg = "Both Passwords do not match.";

            setTimeout(function () {
                $scope.errorMsg = "";
                $scope.$apply();
            }, 3000);

        }
        else{
            $.post(MY_CONSTANT.url + '/change_forgot_password',
                {
                    email: email,
                    token: token,
                    password: $scope.resetPass.password
                }).then(
                function (data) {
                    data = JSON.parse(data);

                    if (data.status == responseCode.SHOW_ERROR_MESSAGE) {
                        $scope.errorMsg = "Something went wrong, Please try again later";
                        $scope.$apply();
                        setTimeout(function () {
                            $scope.errorMsg = "";
                            $scope.$apply();
                        }, 3000);
                    }
                    else if(data.status == responseCode.SUCCESS) {
                        $scope.successMsg = "Password reset successfully.";
                        $scope.$apply();
                        setTimeout(function () {
                            $scope.successMsg = "";
                            $state.go('page.login');
                            $scope.$apply();
                        }, 3000);
                    }
                   else {
                        $scope.errorMsg = data.message.toString();
                        $scope.$apply();
                        setTimeout(function () {
                            $scope.errorMsg = "";
                            $scope.$apply();
                        }, 3000);
                    }
                });
        }

    };


});

