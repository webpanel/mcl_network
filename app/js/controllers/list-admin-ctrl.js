/**
 * Created by sanjay on 3/28/15.
 */
App.controller('ListAdminController', function ($scope, $http, $route, $state, $cookies, $cookieStore, MY_CONSTANT, $timeout, ngDialog, responseCode) {

    'use strict';
    $scope.addAdmin = {};
    $scope.addAdmin.email = '';
    $scope.delete_driver_id = '';
    //type of users
    $scope.type_of_admin=[{
        id:0,
        name: 'Admin'
    },
        {
            id:1,
            name: 'Manager'
        }
]
    $scope.exportData = function () {
        alasql('SELECT * INTO CSV("adminlist.csv",{headers:true}) FROM ?',[$scope.excelList]);
    };


    /*--------------------------------------------------------------------------
     * -------------------funtion to open dialog for add driver ----------------
     --------------------------------------------------------------------------*/
    $scope.AddAdminDialog = function () {
        $scope.addAdmin.email = '';
        ngDialog.open({
            template: 'modalToAddAdmin',
            className: 'ngdialog-theme-default',
            showClose: false,
            scope: $scope,
            preCloseCallback: function () {
                $scope.addAdmin.email='';
                $scope.addAdmin.name = '';
                return true;
            }
        });
    };

    /*--------------------------------------------------------------------------
     * -------------------------funtion to add driver --------------------------
     --------------------------------------------------------------------------*/
    $scope.submit = function (addAdmin) {
        console.log(addAdmin.type_of_admin.name);
        $.post(MY_CONSTANT.url + '/add_super_admin', {
            access_token: $cookieStore.get('obj').accesstoken,
            email: addAdmin.email,
            user_name: addAdmin.name,
            type:addAdmin.type_of_admin.id,
            type_name:addAdmin.type_of_admin.name
        }, function (data) {
            console.log(data);
            data = JSON.parse(data);

            if (data.status == responseCode.SUCCESS) {
                $scope.addAdmin.successMsg = "Admin Added Sucessfully.";
                $scope.$apply();
                setTimeout(function () {
                    $scope.addAdmin.successMsg = "";
                    $scope.$apply();
                    ngDialog.close({
                        template: 'modalToAddAdmin',
                        className: 'ngdialog-theme-default',
                        showClose: false,
                        scope: $scope
                    });
                }, 3000);
                $state.reload();
            }
            else if (data.status == responseCode.INVALID_ACCESS_TOKEN){
                $state.go('page.login');
            }
            else {
                $scope.addAdmin.errorMsg = data.message;
                $scope.$apply();
                setTimeout(function () {
                    $scope.addAdmin.errorMsg = "";
                    $scope.$apply();
                    ngDialog.close({
                        template: 'modalToAddAdmin',
                        className: 'ngdialog-theme-default',
                        showClose: false,
                        scope: $scope
                    });
                }, 3000);
            }
        })
    };

    //var driver_details = function () {
    $.post(MY_CONSTANT.url + '/api/admin/adminUserList', {
        accessToken: $cookieStore.get('obj').accesstoken
    }, function (data) {

        var dataArray = [];
        var excelArray =[];
        if(typeof data == "string")
        {
            data = JSON.parse(data);
        };

        if (data.flag == responseCode.SUCCESS) {
            console.log(data);
            var adminList = data.data.admins;
            adminList.forEach(function (column) {
//==========================================================================================================================
//============================================================ data for excel =============================================
//==========================================================================================================================
                var e ={};
                e.NAME = column.firstName + " " + column.lastName;
                e.USERNAME = column.userName;
                e.EMAIL = column.email;
                e.CONTACT = column.phoneNo;
                e.TYPE_OF_ADMIN = (column.adminType = '0')?'ADMIN':'MANAGER';
                excelArray.push(e);


//==========================================================================================================================
//============================================================  end data for excel =============================================
//==========================================================================================================================

                var d = {};
                d.name = column.firstName + " " + column.lastName;
                d.user_name = column.userName;
                d.user_email = column.email;
                d.admin_id = column.userId;
                d.contact = column.phoneNo;
                d.admin_type = (column.adminType = '0')?'ADMIN':'MANAGER';
                d.edit_admin_url = "/app/edit-admin/" +  column.userId;
                d.view_admin_url = "/app/view-admin/" +  column.userId;
                dataArray.push(d);
            });

            $scope.$apply(function () {
                $scope.list = dataArray;
                $scope.excelList = excelArray;

                // Define global instance we'll use to destroy later
                var dtInstance;

                $timeout(function () {
                    if (!$.fn.dataTable)
                        return;
                    dtInstance = $('#datatable2').dataTable({
                        'paging': true, // Table pagination
                        'ordering': true, // Column ordering
                        'info': true, // Bottom left status text
                        // Text translation options
                        // Note the required keywords between underscores (e.g _MENU_)
                        oLanguage: {
                            sSearch: 'Search all columns:',
                            sLengthMenu: '_MENU_ records per page',
                            info: 'Showing page _PAGE_ of _PAGES_',
                            zeroRecords: 'Nothing found - sorry',
                            infoEmpty: 'No records available',
                            infoFiltered: '(filtered from _MAX_ total records)'
                        },
                        "aoColumnDefs": [
                            { 'bSortable': false, 'aTargets': [5] }
                        ]
                    });
                    var inputSearchClass = 'datatable_input_col_search';
                    var columnInputs = $('tfoot .' + inputSearchClass);

                    // On input keyup trigger filtering
                    columnInputs
                        .keyup(function () {
                            dtInstance.fnFilter(this.value, columnInputs.index(this));
                        });
                });
                // When scope is destroyed we unload all DT instances
                // Also ColVis requires special attention since it attaches
                // elements to body and will not be removed after unload DT
                $scope.$on('$destroy', function () {
                    dtInstance.fnDestroy();
                    $('[class*=ColVis]').remove();
                });
            });
        }
        else if (data.flag == responseCode.INVALID_ACCESS_TOKEN){
            $state.go('page.login');
        }
    });

    /*--------------------------------------------------------------------------
     * --------- funtion to refresh page ---------------------------------------
     --------------------------------------------------------------------------*/
    $scope.refreshPage = function () {
        $state.reload();
        ngDialog.close({
            template: 'display_msg_modalDialog',
            className: 'ngdialog-theme-default',
            scope: $scope
        });

    };


    /*--------------------------------------------------------------------------
     * ---------------- funtion to open modal for delete admin ----------------
     --------------------------------------------------------------------------*/
    $scope.deleteadmin_popup = function (id) {




        $scope.admin_id  = id;

        ngDialog.open({
            template: 'delete_admin_modalDialog',
            className: 'ngdialog-theme-default',
            showClose: false,
            scope: $scope
        });
    };

    /*--------------------------------------------------------------------------
     * -------------------------funtion to delete admin --------------------------
     --------------------------------------------------------------------------*/
    $scope.delete_admin = function () {

        ngDialog.close({
            template: 'delete_admin_modalDialog',
            className: 'ngdialog-theme-default',
            scope: $scope
        });


        $.post(MY_CONSTANT.url + '/api/admin/deleteAnAdmin',
            {
                accessToken: $cookieStore.get('obj').accesstoken,
                "userId":$scope.admin_id
            }, function (data) {
                if(typeof data == "string")
                {
                    data = JSON.parse(data);
                }
                console.log(data);

                if (data.flag == responseCode.SUCCESS) {
                    $scope.displaymsg = "Admin deleted successfully";
                }
                else if (data.flag == responseCode.INVALID_ACCESS_TOKEN){
                    $state.go('page.login');
                }
                else {
                    $scope.displaymsg = data.error;
                }
                $scope.$apply();
                ngDialog.open({
                    template: 'display_msg_modalDialog',
                    className: 'ngdialog-theme-default',
                    showClose: false,
                    scope: $scope
                });
            });


    };

});