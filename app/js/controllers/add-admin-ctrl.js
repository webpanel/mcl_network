/**
 * Created by Salma on 8/18/15.
 */
App.controller('AddAdminController', function ($scope, $http, $cookies, $cookieStore, MY_CONSTANT, $state,ngDialog,responseCode) {
    //type of users
    $scope.add_admin_img_sent= 0;
    $scope.type_of_admin=[{
        id:0,
        name: 'Admin'
    },
        {
            id:1,
            name: 'Manager'
        }
    ]
    /*==========================================================
                   function on upload of image
     ===========================================================*/
    $scope.file_to_upload = function (files) {
        console.log(files);

        $scope.add_admin_image = files[0];
        $scope.add_admin_img_sent = 1;
        var file = files[0];
        var imageType = /image.*/;
        if (!file.type.match(imageType)) {

        }
        var img = document.getElementById("admin_image");
        img.file = file;
        var reader = new FileReader();
        reader.onload = (function (aImg) {
            return function (e) {
                aImg.src = e.target.result;
            };
        })(img);
        reader.readAsDataURL(file);
    }
    /*==========================================================
                        api hit for add admin
     ===========================================================*/

    $scope.addAdmin = function(addadmin){
        if($scope.add_admin_img_sent==0){
            $scope.errorMsg = "Upload Image";
            $scope.TimeOutError();
            return false;
        }
        console.log(addadmin.type_of_admin.id);

        var formData = new FormData();
        formData.append('accessToken', $cookieStore.get('obj').accesstoken);
        formData.append('userName', addadmin.user_name);
        formData.append('email', addadmin.email_id);
        formData.append('password', addadmin.password);
        formData.append('firstName', addadmin.first_name);
        formData.append('lastName', addadmin.last_name);
        formData.append('dob', addadmin.dob);
        formData.append('adminType', addadmin.type_of_admin.id);
        formData.append('countryCode', addadmin.country_code);
        formData.append('phoneNo', addadmin.mobile_num);
        formData.append('address', addadmin.street_address);
        formData.append('locality', addadmin.locality);
        formData.append('region', addadmin.region);
        formData.append('zipcode', addadmin.postal_code);
        formData.append('admin_image',$scope.add_admin_image);

        //api hit here

        $.ajax({
            type: 'POST',
            url: MY_CONSTANT.url + '/api/admin/addNewAdmin',
            dataType: "json",
            data: formData,
            async: false,
            processData: false,
            contentType: false,
            success: function (data) {
                if (data.flag == responseCode.SUCCESS) {
                    $scope.displaymsg = "Admin Added Successfully";
                }
                else if (data.flag == responseCode.INVALID_ACCESS_TOKEN) {
                    $state.go('page.login');
                }
                else {
                    $scope.displaymsg = data.error;
                }
                ngDialog.open({
                    template: 'display_msg_modalDialog',
                    className: 'ngdialog-theme-default',
                    showClose: false,
                    scope: $scope
                });

            }
        });
    }

    /*=================================================
     *              Timeout function
     ================================================*/
    $scope.TimeOutError = function () {
        setTimeout(function () {
            $scope.errorMsg = "";
            $scope.$apply();
        }, 3000);

    }

});


