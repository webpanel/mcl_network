/**
 * Created by salma on 6/9/15.
 */
App.controller('VehicleController', function ($scope, $http, $cookies,$timeout, $cookieStore, MY_CONSTANT, $state,ngDialog,responseCode) {

    $scope.select_car = '';
    $scope.vehicle_status = [
        {
            id:0,
            name: 'INACTIVE'
        },
        {
            id:1,
            name: 'ACTIVE'
        }

    ]
//getting car types in list
    $.post(MY_CONSTANT.url + '/api/vehicles/viewType', {accessToken: $cookieStore.get('obj').accesstoken},
        function (data) {
            var carArray = [];
            if(typeof(data)=='string'){
                data = JSON.parse(data);
            }
            console.log("viewtype");
            console.log(data);

            if (data.flag == responseCode.SUCCESS) {
                var carList = data.data.vehicles;
                carList.forEach(function (column) {
                    var d = {};
                    d.name = column.name;
                    d.id = column.id;
                    d.type = column.type;
                    carArray.push(d);
                });
                $scope.$apply(function () {
                    $scope.carlist = carArray;
                });
            }
        });

    $.post(MY_CONSTANT.url + '/api/vehicles/list', {accessToken: $cookieStore.get('obj').accesstoken},
        function (data) {
            var dataArray = [];
            if(typeof data == "string"){
                data = JSON.parse(data);
            }
            console.log(data);

            if (data.flag== responseCode.SUCCESS) {
                var carList = data.data.vehicle;
                carList.forEach(function (column) {
                    var d = {};
                    d.id = column.id;
                    d.model = column.model;
                    d.registrationNo = column.registrationNo;
                    d.status = column.status;
                    d.type = column.type
                    dataArray.push(d);
                });
                $scope.$apply(function () {
                    $scope.list = dataArray;
                    // Define global instance we'll use to destroy later
                    var dtInstance;

                    $timeout(function () {
                        if (!$.fn.dataTable) return;
                        dtInstance = $('#datatable2').dataTable({
                            'paging': true,  // Table pagination
                            'ordering': true,  // Column ordering
                            'info': true,  // Bottom left status text
                            // Text translation options
                            // Note the required keywords between underscores (e.g _MENU_)
                            oLanguage: {
                                sSearch: 'Search all columns:',
                                sLengthMenu: '_MENU_ records per page',
                                info: 'Showing page _PAGE_ of _PAGES_',
                                zeroRecords: 'Nothing found - sorry',
                                infoEmpty: 'No records available',
                                infoFiltered: '(filtered from _MAX_ total records)'
                            },
                            "aoColumnDefs": [
                                { 'bSortable': false, 'aTargets': [3] }
                            ]
                        });
                        var inputSearchClass = 'datatable_input_col_search';
                        var columnInputs = $('tfoot .' + inputSearchClass);

                        // On input keyup trigger filtering
                        columnInputs
                            .keyup(function () {
                                dtInstance.fnFilter(this.value, columnInputs.index(this));
                            });
                    });

                    // When scope is destroyed we unload all DT instances
                    // Also ColVis requires special attention since it attaches
                    // elements to body and will not be removed after unload DT
                    $scope.$on('$destroy', function () {
                        dtInstance.fnDestroy();
                        $('[class*=ColVis]').remove();
                    });
                });
            }
            else if (data.flag == responseCode.INVALID_ACCESS_TOKEN) {
                $state.go('page.login');
            }
            else{
                alert(data.message);
            }
        });

//@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
//                            OPEN EDIT VEHICLE DIALOG BOX
//@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
    $scope.editVehicleDialog = function(id,type){
        $scope.type = type;
        $scope.id = id;
        console.log("type"+type);
        $.post(MY_CONSTANT.url + '/api/vehicles/view', {accessToken: $cookieStore.get('obj').accesstoken,id:id},
            function (data) {
                var dataArray = [];
                if(typeof data == "string"){
                    data = JSON.parse(data);
                }
                console.log(data);

                if (data.flag== responseCode.SUCCESS) {
                    var vehicleView = data.data.vehicle[0];
                    console.log(vehicleView);
                    $scope.updateVehicle = {
                        vehicle_model:vehicleView.model,
                        reg_no:vehicleView.registrationNo,
                        status:parseInt(vehicleView.status)

                    }
                    ngDialog.open({
                        template: 'edit_vehicle_dialog',
                        className: 'ngdialog-theme-default',
                        showClose: false,
                        closeByDocument:false,
                        scope: $scope
                    });

                }
                else {
                    $scope.displaymsg = data.error;
                    ngDialog.open({
                        template: 'display_msg_modalDialog',
                        className: 'ngdialog-theme-default',
                        showClose: false,
                        scope: $scope
                    });
                }
                $scope.$apply();

            });

}
//@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
//                           SUBMIT UPDATE VEHICLE DETAILS
//@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
    $scope.submitUpdateVehicle = function(updateVehicle){
        console.log(updateVehicle);
        console.log($scope.type);

        $.post(MY_CONSTANT.url + '/api/vehicles/update', {accessToken: $cookieStore.get('obj').accesstoken,
                model:updateVehicle.vehicle_model,
                registrationNo:updateVehicle.reg_no,
                status:updateVehicle.status,
                type:$scope.type,
                id:$scope.id


            },
            function (data) {
                ngDialog.close({
                    template: 'edit_vehicle_dialog',
                    className: 'ngdialog-theme-default',
                    showClose: false,
                    closeByDocument:false,
                    scope: $scope
                });

                if(typeof data == "string"){
                    data = JSON.parse(data);
                }
                console.log(data);

                if (data.flag== responseCode.SUCCESS) {
                    $scope.displaymsg = "Vehicle Updated Successfully.";
                }
                else {
                    $scope.displaymsg = data.error;

                }
                $scope.$apply();
                ngDialog.open({
                    template: 'display_msg_modalDialog',
                    className: 'ngdialog-theme-default',
                    showClose: false,
                    scope: $scope
                });

            });


    }
//@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
//                            OPEN ADD VEHICLE DIALOG BOX
//@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
    $scope.openAddVehicleDialog = function(){

        ngDialog.open({
            template: 'add_vehicle_dialog',
            className: 'ngdialog-theme-default',
            showClose: false,
            scope: $scope
        });
    }
    //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
//                           SUBMIT ADD VEHICLE DETAILS
//@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
    $scope.submitAddVehicle = function(addVehicle){
        console.log(addVehicle);

        $.post(MY_CONSTANT.url + '/api/vehicles/add', {accessToken: $cookieStore.get('obj').accesstoken,
                model:addVehicle.vehicle_model,
                registrationNo:addVehicle.reg_no,
                status:1,
                type:addVehicle.type
            },
            function (data) {
                ngDialog.close({
                    template: 'add_vehicle_dialog',
                    className: 'ngdialog-theme-default',
                    showClose: false,
                    closeByDocument:false,
                    scope: $scope
                });

                if(typeof data == "string"){
                    data = JSON.parse(data);
                }
                console.log(data);

                if (data.flag== responseCode.SUCCESS) {
                    $scope.displaymsg = "Vehicle Added Successfully.";
                }
                else {
                    $scope.displaymsg = data.error;

                }
                $scope.$apply();
                ngDialog.open({
                    template: 'display_msg_modalDialog',
                    className: 'ngdialog-theme-default',
                    showClose: false,
                    scope: $scope
                });

            });


    }

});

