var myApp = angular.module('myAppName', ['angle','uiGmapgoogle-maps']);

myApp.config(['uiGmapGoogleMapApiProvider', function (GoogleMapApi) {
    GoogleMapApi.configure({
//    key: 'your api key',
        v: '3.17',
        libraries: 'weather,geometry,visualization'
    });
}]);

myApp.run(["$log", function ($log) {

    $log.log('I\'m a line from custom.js');

}]);

App.constant("MY_CONSTANT", {
    "url":"http://www.vclnetwork.com:9100",
    "url_booking": "http://www.vclnetwork.com:9000"
});

App.constant("promoCode", {
    "APP": "In App",
    "SIGNUP": "Sign Up"
});

App.constant("countryName", {
    "NAME":"INDIA"
});
App.constant("MapLatLong", {
    "lat": 57.1910499,
    "lng": -2.0834466
});

App.constant("responseCode", {
    "SUCCESS": 200,
    "PARAMETER_MISSING": 100,
"SHOW_ERROR_MESSAGE":201,
"INVALID_ACCESS_TOKEN":401,
"ERROR_IN_EXECUTION":404,
"IMAGE_FILE_MISSING":102,
"INVALID_CAR_TYPE":103,
"INVALID_BLOCK_STATUS":104,
"INVALID_CAR_ID":105
});
myApp.config(['$stateProvider', '$locationProvider', '$urlRouterProvider', 'RouteHelpersProvider','$httpProvider',
    function ($stateProvider, $locationProvider, $urlRouterProvider, helper,$httpProvider) {
        'use strict';
        $httpProvider.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded;charset=utf-8';
        // Set the following to true to enable the HTML5 Mode
        // You may have to set <base> tag in index and a routing configuration in your server
        $locationProvider.html5Mode(false);

        // default route
        $urlRouterProvider.otherwise('/page/login');

        //
        // Application Routes
        // -----------------------------------
        $stateProvider
            //
            // Single Page Routes
            // -----------------------------------
            .state('page', {
                url: '/page',
                templateUrl: 'app/pages/page.html',
                resolve: helper.resolveFor('modernizr', 'icons', 'parsley'),
                controller: ["$rootScope", function ($rootScope) {
                    $rootScope.app.layout.isBoxed = false;
                }]
            })
            .state('page.login', {
                url: '/login',
                title: "Login",
                templateUrl: 'app/pages/login.html',
                resolve: helper.resolveFor('ngDialog')
            })
            .state('page.register', {
                url: '/register',
                title: "Register",
                templateUrl: 'app/pages/register.html',
                resolve: helper.resolveFor('ngDialog')
            })
            .state('page.recover', {
                url: '/recover',
                title: "Recover",
                templateUrl: 'app/pages/recover.html',
                resolve: helper.resolveFor('ngDialog')
            })
            .state('page.terms', {
                url: '/terms',
                title: "Terms & Conditions",
                templateUrl: 'app/pages/terms.html'
            })
            .state('page.404', {
                url: '/404',
                title: "Not Found",
                templateUrl: 'app/pages/404.html'
            })
            .state('page.resetpassword', {
                url: '/reset-password',
                title: "Reset Password",
                templateUrl: 'app/pages/resetpassword.html'
            })

            //App routes
            .state('app', {
                url: '/app',
                abstract: true,
                templateUrl: helper.basepath('app.html'),
                controller: 'AppController',
                resolve: helper.resolveFor('ngDialog','modernizr', 'icons', 'screenfull')
            })
            .state('app.changepassword', {
                url: '/change-password',
                title: 'Change Password',
                templateUrl: helper.basepath('changePassword.html'),
                resolve: helper.resolveFor( 'parsley')
            })
            .state('app.dashboard', {
                url: '/dashboard',
                title: 'Dashboard',
                templateUrl: helper.basepath('dashboard.html')
            })
            .state('app.dashboardReport', {
                url: '/dashboard-report',
                title: 'Dashboard Report',
                templateUrl: helper.basepath('dashboard-report.html')
            })
            .state('app.listadmin', {
                url: '/list-admin',
                title: 'Add Admin',
                templateUrl: helper.basepath('list_admin.html'),
                resolve: helper.resolveFor('datatables', 'datatables-pugins','parsley','ngDialog')
            })
            .state('app.addadmin', {
                url: '/add-admin',
                title: 'Add Admin',
                templateUrl: helper.basepath('add_admin.html'),
                resolve: helper.resolveFor('parsley','ngDialog','datepicker')
            })
            .state('app.edit_admin', {
                url: '/edit-admin/:admin_id',
                title: 'Edit Admin',
                templateUrl: helper.basepath('editadmin.html'),
                resolve: helper.resolveFor('parsley','ngDialog','datepicker')
            })
            .state('app.view_admin', {
                url: '/view-admin/:admin_id',
                title: 'View Admin',
                templateUrl: helper.basepath('view-admin.html'),
                resolve: helper.resolveFor('parsley','ngDialog','datepicker')
            })
            .state('app.listview', {
                url: '/mydrivers',
                title: 'All Drivers',
                templateUrl: helper.basepath('mydrivers.html'),
                resolve: helper.resolveFor('datatables', 'datatables-pugins','ngDialog','parsley','ngTable','ngTableExport','whirl')
            })
            .state('app.activedrivers', {
                url: '/activedrivers',
                title: 'Active Drivers',
                templateUrl: helper.basepath('activedriver.html'),
                resolve: helper.resolveFor('datatables', 'datatables-pugins','ngDialog','parsley','ngTable','ngTableExport','whirl')
            })
             .state('app.add_driver', {
                url: '/add-driver',
                title: 'Add Driver',
                templateUrl: helper.basepath('add_driver.html'),
                resolve: helper.resolveFor('parsley','ngDialog','datepicker')
            })
            .state('app.view_driver', {
                url: '/view-driver/:driver_id',
                title: 'Add Driver',
                templateUrl: helper.basepath('viewdriver.html'),
                resolve: helper.resolveFor('parsley','ngDialog','datepicker')
            })

            .state('app.pendingdrivers', {
                url: '/pendingdrivers',
                title: 'Pending Drivers',
                templateUrl: helper.basepath('pendingdrivers.html'),
                resolve: helper.resolveFor('datatables', 'datatables-pugins', 'ngDialog', 'inputmask' ,'parsley','whirl')
            })
            .state('app.editdriver', {
                url: '/edit-driver/:driver_id',
                title: 'Edit Driver',
                templateUrl: helper.basepath('editdriver.html'),
                resolve: helper.resolveFor('datatables', 'datatables-pugins', 'ngDialog', 'inputmask','parsley','datepicker')
            })
            .state('app.approvedriver', {
                url: '/approve-driver/:driver_id',
                title: 'Approve Driver',
                templateUrl: helper.basepath('approvedriver.html'),
                resolve: helper.resolveFor('datatables', 'datatables-pugins', 'ngDialog', 'inputmask','parsley','datepicker')
            })
            .state('app.interestedDrivers', {
                url: '/interestedDriver',
                title: 'Intersted Driver',
                templateUrl: helper.basepath('interestedDrivers.html'),
                resolve: helper.resolveFor('datatables', 'datatables-pugins', 'ngDialog', 'inputmask','parsley','datepicker')
            })
            .state('app.mypassengers', {
                url: '/all_passengers',
                title: 'My Passengers',
                templateUrl: helper.basepath('mypassengers.html'),
                resolve: helper.resolveFor('datatables', 'datatables-pugins', 'ngDialog','whirl','parsley')
            })
            .state('app.contactUs', {
                url: '/contact_us',
                title: 'Contact Us',
                templateUrl: helper.basepath('contactUs.html'),
                resolve: helper.resolveFor('datatables', 'datatables-pugins', 'ngDialog','whirl','parsley')
            })
            .state('app.view_customer', {
                url: '/view-customer/:customer_id',
                title: 'View Customer',
                templateUrl: helper.basepath('view_customer.html'),
                resolve: helper.resolveFor('parsley','ngDialog','datepicker')
            })
            .state('app.editcustomer', {
                url: '/edit-customer/:customer_id',
                title: 'Edit Driver',
                templateUrl: helper.basepath('edit_customer.html'),
                resolve: helper.resolveFor('datatables', 'datatables-pugins', 'ngDialog', 'inputmask','parsley','datepicker')
            })
            .state('app.myrides', {
                url: '/my_rides',
                title: 'My Rides',
                templateUrl: helper.basepath('myrides.html'),
                resolve: helper.resolveFor('datatables', 'datatables-pugins','whirl')
            })
            .state('app.ongoingrides', {
                url: '/ongoing-rides',
                title: 'Ongoing rides',
                templateUrl: helper.basepath('ongoingrides.html'),
                resolve: helper.resolveFor( 'ngDialog','datatables', 'datatables-pugins','whirl')
            })
            .state('app.completedrides', {
                url: '/completed-rides',
                title: 'Completed rides',
                templateUrl: helper.basepath('completedrides.html'),
                resolve: helper.resolveFor( 'ngDialog','datatables', 'datatables-pugins','whirl')
            })
            .state('app.missedrides', {
                url: '/missed-rides',
                title: 'Missed rides',
                templateUrl: helper.basepath('missed-rides.html'),
                resolve: helper.resolveFor( 'ngDialog','datatables', 'datatables-pugins','whirl')
            })
            .state('app.cancelledrides', {
                url: '/cancelled-rides',
                title: 'Cancelled rides',
                templateUrl: helper.basepath('cancelled-rides.html'),
                resolve: helper.resolveFor( 'ngDialog','datatables', 'datatables-pugins','whirl')
            })
            .state('app.scheduledrides', {
                url: '/scheduled-rides',
                title: 'Scheduled rides',
                templateUrl: helper.basepath('scheduled_rides.html'),
                resolve: helper.resolveFor( 'ngDialog','datatables', 'datatables-pugins','whirl','ngDialog')
            })
            .state('app.listvehicles', {
                url: '/vehicles',
                title: 'Vehicles',
                templateUrl: helper.basepath('listvehicles.html'),
                resolve: helper.resolveFor('ngDialog','datatables', 'datatables-pugins','parsley','whirl')
            })
            .state('app.addbooking', {
                url: '/add-booking',
                title: 'Add Booking',
                templateUrl: helper.basepath('addbooking.html'),
                resolve: helper.resolveFor('parsley','ngDialog','loadGoogleMapsJS','whirl', function () {
                    return loadGoogleMaps();
                }, 'google-map')
            })
            .state('app.fare', {
                url: '/fare',
                title: 'Fare',
                templateUrl: helper.basepath('fare.html'),
                resolve: helper.resolveFor('datatables', 'datatables-pugins','parsley')
            })
            .state('app.surge_pricing', {
                url: '/surge_pricing',
                title: 'Surge Pricing',
                templateUrl: helper.basepath('surge_pricing.html'),
                resolve: helper.resolveFor('datatables', 'datatables-pugins','parsley')
            })
            .state('app.form', {
                url: '/promo-code',
                title: 'Promo Codes',
                templateUrl: helper.basepath('promocode.html'),
                resolve: helper.resolveFor('datatables', 'datatables-pugins','parsley', 'ngDialog','ngTable','ngTableExport','whirl')
            })
            .state('app.addpromocode', {
                url: '/add-promo-code',
                title: 'Add Promo Codes',
                templateUrl: helper.basepath('addpromo.html'),
                resolve: helper.resolveFor('parsley', 'ngDialog')
            })
            .state('app.editpromocode', {
                url: '/edit-promo-code/:promo_id',
                title: 'Edit Promo Codes',
                templateUrl: helper.basepath('editpromo.html'),
                resolve: helper.resolveFor('parsley', 'ngDialog')
            })
            .state('app.engagements', {
                url: '/engagement-list/:promo_code',
                title: 'Engagement List',
                templateUrl: helper.basepath('engagements.html'),
                resolve: helper.resolveFor('datatables', 'datatables-pugins','parsley', 'ngDialog','ngTable','ngTableExport','whirl')
            })
            .state('app.reports', {
                url: '/reports',
                title: 'Reports',
                templateUrl: helper.basepath('reports.html'),
                resolve: helper.resolveFor('high-chart','ngDialog')
            })
            .state('app.corporate', {
                url: '/corporate',
                title: 'Corporate',
                templateUrl: helper.basepath('corporate.html'),
                resolve: helper.resolveFor('datatables', 'datatables-pugins', 'ngDialog','parsley','whirl')
            })
            .state('app.invoice', {
                url: '/invoice-list/:corp_id',
                title: 'Invoice List',
                templateUrl: helper.basepath('invoicelist.html'),
                resolve: helper.resolveFor('datatables', 'datatables-pugins','parsley', 'ngDialog','ngTable','ngTableExport','whirl')
            })
            .state('app.payments', {
                url: '/payments',
                title: 'Payments',
                templateUrl: helper.basepath('payments.html'),
                resolve: helper.resolveFor('datatables', 'datatables-pugins', 'ngDialog','whirl')
            })
            .state('app.help_faq', {
                url: '/help_faq',
                title: 'Help & FAQ',
                templateUrl: helper.basepath('help_faq.html'),
                resolve: helper.resolveFor('ngDialog')
            })
            .state('app.email_template', {
                url: '/email-template',
                title: 'Email template',
                templateUrl: helper.basepath('email_template.html'),
                resolve: helper.resolveFor('datatables', 'datatables-pugins','parsley','ngDialog')
           })
            .state('app.addEmailTemplate', {
                url: '/updateEmailTemplate',
                title: 'Email template',
                templateUrl: helper.basepath('updateEmailTemplate.html'),
                resolve: helper.resolveFor('datatables', 'datatables-pugins','parsley')
            })

            .state('app.updateEmailTemplate', {
                url: '/updateEmailTemplate/:templateId',
                title: 'Email template',
                templateUrl: helper.basepath('updateEmailTemplate.html'),
                resolve: helper.resolveFor('datatables', 'datatables-pugins','parsley')
            })
            .state('app.bkRequestedRides', {
                url: '/booked-later-requested-rides',
                title: 'Booked Later Completed rides',
                templateUrl: helper.basepath('bk-requested-rides.html'),
                resolve: helper.resolveFor( 'ngDialog','datatables', 'datatables-pugins','whirl')
            })
            .state('app.bkOngoingRides', {
                url: '/booked-later-ongoing-rides',
                title: 'Booked Later Ongoing rides',
                templateUrl: helper.basepath('bk-ongoing-rides.html'),
                resolve: helper.resolveFor( 'ngDialog','datatables', 'datatables-pugins','whirl')
            })
            .state('app.bkCompletedRides', {
                url: '/booked-later-completed-rides',
                title: 'Booked Later Completed rides',
                templateUrl: helper.basepath('bk-completed-rides.html'),
                resolve: helper.resolveFor( 'ngDialog','datatables', 'datatables-pugins','whirl')
            })
            .state('app.bkCancelledRides', {
                url: '/booked-later-cancelled-rides',
                title: 'Booked Later Cancelled rides',
                templateUrl: helper.basepath('bk-cancelled-rides.html'),
                resolve: helper.resolveFor( 'ngDialog','datatables', 'datatables-pugins','whirl')
            })
            .state('app.blog', {
                url: '/blog',
                title: 'Blog Management',
                templateUrl: helper.basepath('blog.html'),
                resolve: helper.resolveFor( 'ngDialog','datatables', 'datatables-pugins','whirl')
            })
            .state('app.updateBlog', {
                url: '/updateBlog/:blogId',
                title: 'Update Blog',
                templateUrl: helper.basepath('updateBlog.html'),
                resolve: helper.resolveFor('ngDialog','parsley')
            });

        $httpProvider.defaults.transformRequest = function(data){
            if (data === undefined) {
                return data;
            }
            return $.param(data);
        }





        /**=========================================================
         * Module: masked,js
         * Initializes the masked inputs
         =========================================================*/
        App.directive('masked', function () {
            return {
                restrict: 'A',
                controller: ["$scope", "$element", function ($scope, $element) {
                    var $elem = $($element);
                    if ($.fn.inputmask)
                        $elem.inputmask();
                }]
            };
        });

        App.directive('googleplace', function() {
            return {
                require: 'ngModel',
                link: function(scope, element, attrs, model) {
                    var options = {
                        types: [],
                        componentRestrictions: {}
                    };
                    scope.gPlace = new google.maps.places.Autocomplete(element[0], options);

                    google.maps.event.addListener(scope.gPlace, 'place_changed', function() {
                        scope.$apply(function() {
                            model.$setViewValue(element.val());
                        });
                    });
                }
            };
        });


        App.controller('MapCtrl',function($scope) {
            $scope.gPlace;
        });



    }]);

App.controller('DatepickerDemoCtrl', ['$scope', function ($scope) {
    $scope.today = function () {
        $scope.dt = new Date();
    };
    $scope.today();

    $scope.clear = function () {
        $scope.dt = null;
    };

    // Disable weekend selection
    $scope.disabled = function (date, mode) {
        return ( mode === 'day' && ( date.getDay() === 0 || date.getDay() === 6 ) );
    };

    $scope.toggleMin = function () {
        $scope.minDate = $scope.minDate ? null : new Date();
    };
    $scope.toggleMin();

    $scope.open = function ($event) {
        $event.preventDefault();
        $event.stopPropagation();

        $scope.opened = true;
    };

    $scope.dateOptions = {
        formatYear: 'yy',
        startingDay: 1
    };

    $scope.initDate = new Date('2016-15-20');
    $scope.formats = ['yyyy-MM-dd', 'dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
    $scope.format = $scope.formats[0];

}]);

/**=========================================================
 * Module: demo-dialog.js
 * Demo for multiple ngDialog Usage
 * - ngDialogProvider for default values not supported
 *   using lazy loader. Include plugin in base.js instead.
 =========================================================*/

// Called from the route state. 'tpl' is resolved before
App.controller('DialogIntroCtrl', ['$scope', 'ngDialog', 'tpl', function($scope, ngDialog, tpl) {
    'user strict';

    // share with other controllers
    $scope.tpl = tpl;
    // open dialog window
    ngDialog.open({
        template: tpl.path,
        // plain: true,
        className: 'ngdialog-theme-default'
    });

}]);

// Loads from view
App.controller('DialogMainCtrl', ["$scope", "$rootScope", "ngDialog", function ($scope, $rootScope, ngDialog) {
    'user strict';

    $rootScope.jsonData = '{"foo": "bar"}';
    $rootScope.theme = 'ngdialog-theme-default';

    $scope.directivePreCloseCallback = function (value) {
        if(confirm('Close it? MainCtrl.Directive. (Value = ' + value + ')')) {
            return true;
        }
        return false;
    };

    $scope.preCloseCallbackOnScope = function (value) {
        if(confirm('Close it? MainCtrl.OnScope (Value = ' + value + ')')) {
            return true;
        }
        return false;
    };

    $scope.open = function () {
        ngDialog.open({ template: 'firstDialogId', controller: 'InsideCtrl', data: {foo: 'some data'} });
    };

    $scope.openDefault = function () {
        ngDialog.open({
            template: 'firstDialogId',
            controller: 'InsideCtrl',
            className: 'ngdialog-theme-default'
        });
    };

    $scope.openDefaultWithPreCloseCallbackInlined = function () {
        ngDialog.open({
            template: 'firstDialogId',
            controller: 'InsideCtrl',
            className: 'ngdialog-theme-default',
            preCloseCallback: function(value) {
                if (confirm('Close it?  (Value = ' + value + ')')) {
                    return true;
                }
                return false;
            }
        });
    };

    $scope.openConfirm = function () {
        ngDialog.openConfirm({
            template: 'modalDialogId',
            className: 'ngdialog-theme-default'
        }).then(function (value) {
        }, function (reason) {
        });
    };

    $scope.openConfirmWithPreCloseCallbackOnScope = function () {
        ngDialog.openConfirm({
            template: 'modalDialogId',
            className: 'ngdialog-theme-default',
            preCloseCallback: 'preCloseCallbackOnScope',
            scope: $scope
        }).then(function (value) {
        }, function (reason) {
        });
    };

    $scope.openConfirmWithPreCloseCallbackInlinedWithNestedConfirm = function () {
        ngDialog.openConfirm({
            template: 'dialogWithNestedConfirmDialogId',
            className: 'ngdialog-theme-default',
            preCloseCallback: function(value) {

                var nestedConfirmDialog = ngDialog.openConfirm({
                    template:
                    '<p>Are you sure you want to close the parent dialog?</p>' +
                    '<div>' +
                    '<button type="button" class="btn btn-default" ng-click="closeThisDialog(0)">No' +
                    '<button type="button" class="btn btn-primary" ng-click="confirm(1)">Yes' +
                    '</button></div>',
                    plain: true,
                    className: 'ngdialog-theme-default'
                });

                return nestedConfirmDialog;
            },
            scope: $scope
        })
            .then(function(value){
                // Perform the save here
            }, function(value){

            });
    };

    $scope.openInlineController = function () {
        $rootScope.theme = 'ngdialog-theme-default';

        ngDialog.open({
            template: 'withInlineController',
            controller: ['$scope', '$timeout', function ($scope, $timeout) {
                var counter = 0;
                var timeout;
                function count() {
                    $scope.exampleExternalData = 'Counter ' + (counter++);
                    timeout = $timeout(count, 450);
                }
                count();
                $scope.$on('$destroy', function () {
                    $timeout.cancel(timeout);
                });
            }],
            className: 'ngdialog-theme-default'
        });
    };

    $scope.openTemplate = function () {
        $scope.value = true;

        ngDialog.open({
            template: $scope.tpl.path,
            className: 'ngdialog-theme-default',
            scope: $scope
        });
    };

    $scope.openTemplateNoCache = function () {
        $scope.value = true;

        ngDialog.open({
            template: $scope.tpl.path,
            className: 'ngdialog-theme-default',
            scope: $scope,
            cache: false
        });
    };

    $scope.openTimed = function () {
        var dialog = ngDialog.open({
            template: '<p>Just passing through!</p>',
            plain: true,
            closeByDocument: false,
            closeByEscape: false
        });
        setTimeout(function () {
            dialog.close();
        }, 2000);
    };

    $scope.openNotify = function () {
        var dialog = ngDialog.open({
            template:
            '<p>You can do whatever you want when I close, however that happens.</p>' +
            '<div><button type="button" class="btn btn-primary" ng-click="closeThisDialog(1)">Close Me</button></div>',
            plain: true
        });
        dialog.closePromise.then(function (data) {
            console.log('ngDialog closed' + (data.value === 1 ? ' using the button' : '') + ' and notified by promise: ' + data.id);
        });
    };

    $scope.openWithoutOverlay = function () {
        ngDialog.open({
            template: '<h2>Notice that there is no overlay!</h2>',
            className: 'ngdialog-theme-default',
            plain: true,
            overlay: false
        });
    };

    $rootScope.$on('ngDialog.opened', function (e, $dialog) {
    });

    $rootScope.$on('ngDialog.closed', function (e, $dialog) {
    });

    $rootScope.$on('ngDialog.closing', function (e, $dialog) {
    });
}]);

App.controller('InsideCtrl', ["$scope", "ngDialog", function ($scope, ngDialog) {
    'user strict';
    $scope.dialogModel = {
        message : 'message from passed scope'
    };
    $scope.openSecond = function () {
        ngDialog.open({
            template: '<p class="lead m0"><a href="" ng-click="closeSecond()">Close all by click here!</a></h3>',
            plain: true,
            closeByEscape: false,
            controller: 'SecondModalCtrl'
        });
    };
}]);

App.controller('SecondModalCtrl', ["$scope", "ngDialog", function ($scope, ngDialog) {
    'user strict';
    $scope.closeSecond = function () {
        ngDialog.close();
    };
}]);







