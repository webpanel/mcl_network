'use strict';

/* Services */


// Demonstrate how to register services
// In this case it is a simple value service.
angular.module('myApp.services', []).
    factory('socket', function (socketFactory) {


        var myIoSocket = io.connect('http://localhost:3001');

        return socketFactory({
            ioSocket: myIoSocket
        });
        // return mySocket;
        //   //return socketFactory();
    }).
    value('version', '0.1');